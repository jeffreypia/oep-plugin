<?php
/**
 * Created by PhpStorm.
 * User: ryansmith
 * Date: 2019-03-26
 * Time: 09:29
 */

namespace OEP\Common;


class Search {

	/**
	 * Asset type search value
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $type = '';
	/**
	 * Asset type "meta" details
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private static $type_meta;
	/**
	 * Queried category slug
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $category;
	/**
	 * Queried asset type format
	 *
	 * Applicable on taxonomy archive
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $format;
	/**
	 * Order key (native)
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $orderby_key = 'orderby';
	/**
	 * Order options
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private static $orderby_options;
	/**
	 * Selected order value
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $orderby = '';
	/**
	 * Searching "all media" flag
	 *
	 * @var   boolean
	 * @since 1.0.0
	 */
	private static $searching_all = null;
	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $oep The ID of this plugin.
	 */
	protected $oep;
	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	protected $version;

	/**
	 * Get things going
	 *
	 * @param      string $oep     The name of this plugin.
	 * @param      string $version The version of this plugin.
	 *
	 * @since 1.0.0
	 */
	function __construct( $oep, $version ) {
		$this->oep     = $oep;
		$this->version = $version;
	}

	/**
	 * Grab the search form template
	 *
	 * @param bool $full if full search form
	 *
	 * @since 1.0.0
	 */
	public static function do_form( $full = false ) {
		// @TODO: need a better way to do this
		global $search_present;
		if ( get_search_query() ) {
			$full = true;
		}

		$args = [];

		$args = $full ? array_merge( $args, [
			'orderby_key'     => self::$orderby_key,
			'orderby_options' => self::$orderby_options,
			'orderby'         => self::$orderby,
		] ) : $args;
		ob_start();
		oep_fn_template_part( 'search', 'form' . ( $search_present ? '-full' : '' ), $args );
		echo ob_get_clean();
		$search_present = true;
	}


	/**
	 * Adjust query to filter by post post_type whenever a blog type term is passed
	 *
	 * @param $search_query
	 */
	public static function set_search_query( $search_query ) {
		// @TODO returns Notice: is_search was called incorrectly
		// if ( is_search() && $search_query->is_main_query() ) {

		if ( $search_query->is_main_query() ) {
			if ( isset($_GET['post_type']) && in_array( $_GET['post_type'], get_terms( [
				'taxonomy' => OEP_BLOG_TYPES_KEY,
				'fields'   => 'id=>slug',
			] ) ) ) {
				$search_query->set("post_type", "post");
				$search_query->set(OEP_BLOG_TYPES_KEY, $_GET['post_type']);
			}
		}
	}


	public function db_filter_authors_search( $posts_search ) {
		// Don't modify the query at all if we're not on the search template
		// or if the LIKE is empty
		// @TODO returns Notice: is_search was called incorrectly
		// if ( !is_search() || empty( $posts_search ) )
		if ( empty( $posts_search ) )
			return $posts_search;
		global $wpdb;
		// Get all of the users of the blog and see if the search query matches either
		// the display name or the user login
		add_filter( 'pre_user_query', [$this, 'db_filter_user_query'] );
		$search = sanitize_text_field( get_query_var( 's' ) );
		$args = array(
			'count_total' => false,
			'search' => sprintf( '*%s*', $search ),
			'search_fields' => array(
				'display_name',
				'user_login',
			),
			'fields' => 'ID',
		);
		$matching_users = get_users( $args );
		remove_filter( 'pre_user_query', 'db_filter_user_query' );
		// Don't modify the query if there aren't any matching users
		if ( empty( $matching_users ) )
			return $posts_search;
		// Take a slightly different approach than core where we want all of the posts from these authors
		$posts_search = str_replace( ')))', ")) OR ( {$wpdb->posts}.post_author IN (" . implode( ',', array_map( 'absint', $matching_users ) ) . ")))", $posts_search );
		return $posts_search;
	}

	/**
	 * Modify get_users() to search display_name instead of user_nicename
	 */
	public function db_filter_user_query( &$user_query ) {
		if ( is_object( $user_query ) )
			$user_query->query_where = str_replace( "user_nicename LIKE", "display_name LIKE", $user_query->query_where );
		return $user_query;
	}


	/**
	 * Remove pages from search results internally
	 */
	public function remove_pages_from_search() {
	    global $wp_post_types;
	    $wp_post_types['page']->exclude_from_search = true;
	}
}