<?php

/**
 * Fired during plugin activation
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.1.0
 * @package    OEP
 * @subpackage OEP/includes
 * @author     Ryan Smith <ryan@designzillas.com>
 */

namespace OEP\Common;

class OEP_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.0
	 */
	public static function activate() {
		do_action( 'oep/activate' );
		flush_rewrite_rules();
	}

}
