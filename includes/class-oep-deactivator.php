<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.1.0
 * @package    OEP
 * @subpackage OEP/includes
 * @author     Ryan Smith <ryan@designzillas.com>
 */

namespace OEP\Common;

class OEP_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.0
	 */
	public static function deactivate() {
		do_action( 'oep/deactivate' );
		flush_rewrite_rules();
	}

}
