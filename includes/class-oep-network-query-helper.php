<?php
/**
 * Network query helper class
 *
 * This utility class assists with looping Network_Query objects provided by the
 * True Multisite Indexer plugin.
 *
 * @since      1.2.0
 * @package    OEP
 * @subpackage OEP/includes
 * @author     Jordan Pakrosnis <jordan@designzillas.com>
 */

namespace OEP\Common;

class Network_Query_Helper {

	/**
	 * Flag for whether or not we're actually going to do stuff
	 *
	 * @since  1.2.0
	 * @access protected
	 * @var    boolean
	 */
	protected $is_network = true;

	/**
	 * Original post before jumping into multisite loop
	 *
	 * @since  1.2.0
	 * @access protected
	 * @var    WP_Post
	 */
	protected $og_post;

	/**
	 * Spin up the helper
	 *
	 * @param  WP_Query|Network_Query  $query  standard query or network query
	 * @return boolean                         whether or not we're dealing with a Network_Query
	 *
	 * @since  1.2.0
	 */
	public function __construct( $query ) {

		if ( ! is_a( $query, 'Network_Query' ) ) {
			return $this->is_network = false;
		}

		global $post;
		$this->og_post = $post;
	}

	/**
	 * Manually set up post data and switch to blog for network query loop
	 * iteration
	 *
	 * @param WP_Query|Network_Query  $query  query, as of its state in the loop
	 * @since 1.2.0
	 */
	public function setup_postdata( $query ) {

		// sanity check
		if ( ! $this->is_network ) return;

		global $post;

		$post = $query->post;
		switch_to_blog( $query->post->BLOG_ID );
	}

	/**
	 * Restore original request's blog and post global
	 *
	 * @since 1.2.0
	 */
	public function reset_postdata() {

		// sanity check
		if ( ! $this->is_network ) return;

		global $post;

		restore_current_blog();
		$post = $this->og_post;
	}
}
