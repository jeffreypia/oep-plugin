<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      0.1.0
 * @package    OEP
 * @subpackage OEP/includes
 * @author     Ryan Smith <ryan@designzillas.com>
 */

namespace OEP\Common;

use OEP\CPT\PostType;
use OEP\Taxonomies\Taxonomy;

class Core {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      Loader $loader Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      string $oep The string used to uniquely identify this plugin.
	 */
	protected $oep;

	/**
	 * The current version of the plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      string $version The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function __construct() {
		if ( defined( 'OEP_VERSION' ) ) {
			$this->version = OEP_VERSION;
		} else {
			$this->version = '0.1.0';
		}
		$this->oep = 'oep';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->register_cpts();
		$this->register_taxonomies();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Loader. Orchestrates the hooks of the plugin.
	 * - i18n. Defines internationalization functionality.
	 * - Admin. Defines all hooks for the admin area.
	 * - Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The helper class
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-oep-utils.php';


		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-oep-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-oep-i18n.php';


		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-oep-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-oep-front.php';

		/**
		 * Newsletter widget class
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-oep-newsletter-widget.php';

		/**
		 * Cost of Living class
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-oep-cost-of-living.php';

		/**
		 * OEP Search class.
		 */
		require_once OEP_PLUGIN_DIR . 'search/class-oep-search.php';

		/**
		 * OEP Custom Post Types class.
		 */
		require_once OEP_PLUGIN_DIR . 'cpts/class-oep-cpts.php';


		/**
		 * OEP Taxonomies class.
		 */
		require_once OEP_PLUGIN_DIR . 'taxonomies/class-oep-taxonomies.php';


		/**
		 * Network query helper class
		 */
		require_once OEP_PLUGIN_DIR . 'includes/class-oep-network-query-helper.php';

		$this->loader = new Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the OEP_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Admin( $this->get_oep(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts', 99 );
		$this->loader->add_action( 'admin_head', $plugin_admin, 'add_webfonts', 99 );
		$this->loader->add_action( 'admin_notices', $plugin_admin, 'admin_notice__error' );
		$this->loader->add_action( 'admin_body_class', $plugin_admin, 'customize_editor' );
		$this->loader->add_filter( 'block_categories', $plugin_admin, 'oep_block_categories', 99, 2 );

		/**
		 * Register custom gutenberg blocks
		 */
		$this->loader->add_action( 'init', $plugin_admin, 'register_post_meta', 1) ;
		$this->loader->add_action( 'init', $plugin_admin, 'register_blocks', 1 );
		$this->loader->add_action( 'enqueue_block_editor_assets', $plugin_admin, 'enqueue_block_scripts', 1 );

		$this->loader->add_action( 'acf/init'	, $plugin_admin, 'newsletter_block' ); //apparently acf/init is too early, go figure
		$this->loader->add_action( 'acf/init'	, $plugin_admin, 'social_cards_block' );
		$this->loader->add_action( 'acf/init'	, $plugin_admin, 'stats_block' );
		$this->loader->add_action( 'acf/init'	, $plugin_admin, 'success_story_block' );
		$this->loader->add_action( 'acf/init'   , $plugin_admin, 'hero_slider_block' );
		$this->loader->add_action( 'wp_loaded'	, $plugin_admin, 'trending_stories_block');
		$this->loader->add_action( 'wp_loaded'	, $plugin_admin, 'cta_option_block');
		$this->loader->add_action( 'wp_loaded'	, $plugin_admin, 'company_cards_block');
		$this->loader->add_action( 'acf/init'   , $plugin_admin, 'welcome_collage_block' );
		$this->loader->add_action( 'acf/init'   , $plugin_admin, 'industry_card_block' );
		$this->loader->add_action( 'acf/init'   , $plugin_admin, 'industry_cards_block' );
		$this->loader->add_action( 'acf/init'	, $plugin_admin, 'neighborhoods_map_block');

		/**
		 * Dequeue gallery block and replace with custom function for front-end rendering
		 */
		$this->loader->add_action( 'render_block_data', $plugin_admin, 'block_gallery_scripts', 99, 1 );

	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     0.1.0
	 * @return    string    The name of the plugin.
	 */
	public function get_oep() {
		return $this->oep;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     0.1.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function define_public_hooks() {

		// general public-facing stuff
		$plugin_public = new Front( $this->get_oep(), $this->get_version() );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_filter( 'get_the_archive_title', $plugin_public, 'set_archive_title', 15, 1);

		// misc search stuff
		$plugin_search = new Search( $this->get_oep(), $this->get_version() );
		$this->loader->add_action( 'oep_search_form_full', $plugin_search, 'do_form', 10, 1 );
		$this->loader->add_action( 'pre_get_posts', $plugin_search, 'set_search_query', 10, 1 );
		$this->loader->add_filter( 'posts_search', $plugin_search, 'db_filter_authors_search', 10, 1 );
		$this->loader->add_action( 'init', $plugin_search, 'remove_pages_from_search', 10 );

		// cost of living calculator
		$plugin_col = new Cost_Of_Living();
		$compare = $plugin_col::COMPARE_ACTION;
		$this->loader->add_action( 'init', $plugin_col, 'set_form_values' );
		$this->loader->add_action( 'acf/init', $plugin_col, 'register_form_block' );
		$this->loader->add_filter( 'oep_public_js_object', $plugin_col, 'add_js_props' );
		$this->loader->add_action( "wp_ajax_{$compare}", $plugin_col, 'get_compare_results' );
		$this->loader->add_action( "wp_ajax_nopriv_{$compare}", $plugin_col, 'get_compare_results' );
	}

	/**
	 * Register all CPTs
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function register_cpts() {

		$plugin_cpt = new PostType( $this->get_oep(), $this->get_version() );

		$this->loader->add_action( 'init', $plugin_cpt, 'register', 99 );
	}

	/**
	 * Register all taxonomies
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function register_taxonomies() {

		$plugin_tax = new Taxonomy( $this->get_oep(), $this->get_version() );

		/**
		 * Register all taxonomies and set up rewrite rules and queries
		 */
		//share taxonomies across all sites in network
		$this->loader->add_action( 'init', $plugin_tax, 'change_tax_terms_table', 0 );
		//make sure we use the shared terms table when switching blogs
		$this->loader->add_action( 'switch_blog', $plugin_tax, 'change_tax_terms_table' );
		$this->loader->add_action( 'init', $plugin_tax, 'register' );
		$this->loader->add_filter( 'init', $plugin_tax, 'rewrite_rules', 99, 1 );
		$this->loader->add_filter( 'parse_query', $plugin_tax, 'parse_query', 99, 1 );

		/**
		 * Misc taxonomy hooks
		 */
		$this->loader->add_filter( 'body_class', $plugin_tax, 'set_term_body_class');
		// set the title for terms
		$this->loader->add_filter( 'get_the_archive_title', $plugin_tax, 'set_archive_title', 10, 1);

		// get all terms in taxonomy of current term
		$this->loader->add_action( 'taxonomy_get_terms', $plugin_tax, 'get_tax_terms', 10, 1);
		$this->loader->add_action( 'get_submenu', $plugin_tax, 'get_submenu', 10, 2);
		$this->loader->add_filter( 'wp_list_categories', $plugin_tax, 'add_term_classes_to_pills', 10, 2);
		$this->loader->add_action( 'taxonomy_featured_posts', $plugin_tax, 'get_taxonomy_featured_posts', 10, 4 );
		$this->loader->add_filter( 'pre_get_posts', $plugin_tax, 'set_filter_query', 99, 1);

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    0.1.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     0.1.0
	 * @return    Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

}
