<?php

/**
 * Miscellaneous functionality
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/includes
 */

/**
 * Helper functions to support other classes
 *
 * @since      0.1.0
 * @package    OEP
 * @subpackage OEP/includes
 * @author     Ryan Smith <ryan@designzillas.com>
 */

namespace OEP\Common;
use OEP\CPT\PostType;

// If this file is called directly, or if ACF isn't loaded, abort.
if ( ! defined( 'ABSPATH' ) ) {
	wp_die( "Nope, you shouldn't be here" );
}

/**
 * deregister jQuery, like what Soil did before they disabled jquery cdn support
 */

/**
 * Load jQuery from jQuery's CDN with a local fallback
 *
 * You can enable/disable this feature in functions.php (or lib/setup.php if you're using Sage):
 * add_theme_support('soil-jquery-cdn');
 */
function register_jquery() {
	$jquery_version = "1.12.1"; //default, change this to upgrade if neccesary

	wp_deregister_script('jquery');

	wp_register_script(
		'jquery',
		'https://code.jquery.com/jquery-' . $jquery_version . '.min.js',
		[],
		null,
		true
	);

	add_filter('wp_resource_hints', function ($urls, $relation_type) {
		if ($relation_type === 'dns-prefetch') {
			$urls[] = 'code.jquery.com';
		}
		return $urls;
	}, 10, 2);

	add_filter('script_loader_src', __NAMESPACE__ . '\\jquery_local_fallback', 10, 2);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\register_jquery', 100);
//add_action('admin_enqueue_scripts', __NAMESPACE__ . '\\register_jquery', 100);

/**
 * Output the local fallback immediately after jQuery's <script>
 *
 * @link http://wordpress.stackexchange.com/a/12450
 */
function jquery_local_fallback($src, $handle = null) {
	static $add_jquery_fallback = false;

	if ($add_jquery_fallback) {
		echo '<script>(window.jQuery && jQuery.noConflict()) || document.write(\'<script src="' . $add_jquery_fallback .'"><\/script>\')</script>' . "\n";
		$add_jquery_fallback = false;
	}

	if ($handle === 'jquery') {
		$add_jquery_fallback = apply_filters('script_loader_src', \includes_url('/js/jquery/jquery.js'), 'jquery-fallback');
	}

	return $src;
}
add_action('wp_head', __NAMESPACE__ . '\\jquery_local_fallback');

/**
 * we need a way to start a nav menu from a particular page / object id instead of outputting the entire menu for use with our submenu hook.
 */

function oep_wp_nav_menu_objects_start_in( $sorted_menu_items, $args ) {
	if(isset($args->start_in)) {
		$menu_item_parents = array();
		foreach( $sorted_menu_items as $key => $item ) {
			// init menu_item_parents
			if( $item->object_id == (int)$args->start_in ) $menu_item_parents[] = $item->ID;

			if( in_array($item->menu_item_parent, $menu_item_parents) ) {
				// part of sub-tree: keep!
				$menu_item_parents[] = $item->ID;
			} else {
				// not part of sub-tree: away with it!
				if($item->object_id != (int)$args->start_in) {
					unset( $sorted_menu_items[ $key ] );
				}
			}
		}
		return $sorted_menu_items;
	} else {
		return $sorted_menu_items;
	}
}

add_filter("wp_nav_menu_objects",__NAMESPACE__ . '\\oep_wp_nav_menu_objects_start_in', 10, 2);

/**
 * Are any of these things empty()?
 *
 * Checks all arguments to see if any of them are empty(). Alternative to:
 * if ( empty( this ) || empty( that ) ) {}.
 *
 * @param  mixed
 *
 * @return bool
 *
 * @since  1.0.0
 */
function oep_fn_any_empty() {

	return ( count(
		         array_filter(
			         func_get_args(),
			         function ( $v ) {
				         return empty( $v );
			         } // wrapper needed for empty()
		         )
	         ) > 0 );
}

/**
 * Output a theme button
 *
 * @see   themes/oep/includes/class-button for params
 * @since 1.0.0
 */
function oep_fn_button( $button, $query = [], $data = [] ) {
	do_action( 'oep_button', $button, $query, $data );
}

/**
 * Returns a $_GET value
 *
 * @param  string  key for $_GET
 *
 * @return mixed   whatever the value is, null if nothing
 *
 * @since 1.0.0
 */
function oep_get( $key ) {
	return isset( $_GET[ $key ] ) ? sanitize_text_field( $_GET[ $key ] ) : null;
}


/**
 * Break array into string of attributes
 *
 * Has special cases for "class" and "data" keys if their values are arrays, so
 * it's compatible with arrays of data attributes (by using recusion).
 *
 * @param  array  $attrs  array of data attributes (keys) and their values
 * @param  string $prefix a prefix for the data attribute (ex: "data-")
 *
 * @return string          inline string of data attributes
 *
 * @since  1.0.0
 */
function oep_fn_attrs( $attrs, $prefix = '' ) {

	foreach ( $attrs as $attr => &$value ) {

		// remove if empty
		if ( empty( $value ) ) {
			unset( $attrs[ $attr ] );
			continue;
		}

		// if data- attributes
		if ( $attr == 'data' && is_array( $value ) ) {
			$attrs[ $attr ] = oep_fn_attrs( array_filter( $value ), 'data-' );
			continue;
		}

		// if array of classes
		if ( $attr == 'class' && is_array( $value ) ) {
			$value = implode( ' ', array_filter( $value ) );
		}

		// array of classes + all other cases
		$attrs[ $attr ] = $prefix . $attr . '="' . esc_attr( $value ) . '"';
	}

	return apply_filters( 'oep_attrs', implode( ' ', $attrs ), $attrs );
}


/**
 * Wrapper for attrs helper (but just for "class")
 *
 * @param  array $classes classes to send to oep_fn_attrs()
 * @return string
 *
 * @see    oep_attrs()
 * @since  1.0.0
 */
function oep_fn_attrs_class( $classes ) {
	return oep_fn_attrs( [ 'class' => $classes ] );
}


/**
 * Get a template part from the plugin
 *
 * @param string $slug the slug name for the generic template
 * @param string $name the name of the specialized template
 * @param array  $args arguments passed to oep_fn_load_template
 *
 * @since 1.0.0
 */
function oep_fn_template_part( $slug, $name = null, $args = [] ) {

	// allow edge cases to get in here
	do_action( "get_template_part_{$slug}", $slug, $name );

	// build the path
	$path = OEP_PLUGIN_DIR . '/public/partials/' . $slug;

	// add specialized name
	$path .= $name ? "-$name" : '';
	$path .= '.php';

	// check for the file
	if ( file_exists( $path ) ) {
		oep_fn_load_template( $path, false, $args );
	}
}


/**
 * Load template
 *
 * Modified version of WordPress' load_template function with an added argument
 * to pass variables to the required template part to avoid needing
 * memory-hogging global vars.
 *
 * This implementation is similar to what WooCommerce does.
 *
 * @param string $_template_file path  to the template file
 * @param bool   $require_once   whether to require_once or require
 * @param array  $args           variables       extracted to template part
 *
 * @since 1.0.0
 */
function oep_fn_load_template( $_template_file, $require_once = true, $args = [] ) {

	global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

	if ( is_array( $wp_query->query_vars ) ) {
		extract( $wp_query->query_vars, EXTR_SKIP );
	}

	// modification
	if ( is_array( $args ) ) {
		extract( $args, EXTR_SKIP );
	}

	if ( isset( $s ) ) {
		$s = esc_attr( $s );
	}

	if ( $require_once ) {
		require_once( $_template_file );
	} else {
		require( $_template_file );
	}
}

/**
 * Get Font Awesome icon string
 *
 * @param  string $name Font Awesome icon name
 *
 * @return string
 *
 * @since  1.0.0
 */
function oep_fn_icon( $name ) {
	return $name ? '<i class="fas fa-' . $name . '"></i>' : '';
}

/**
 * Get Author Profile info
 *
 * @param  string $id ID of author's profile post
 * @param  string $region region of the page method is being called (banner, tile, etc)
 *
 * @return string
 *
 * @since  1.0.0
 */
function oep_get_author_block( $region = '' ) {
//    $authorID           = get_the_author_meta( 'ID' );
	$author_id = get_post_field ('post_author', get_the_ID());
    if( !$author_id ) { return; }

    // Get associated profile post
    // If profile field was not populated, exit
    $profile = get_the_author_meta( 'profile', $author_id );

    if(!$profile) return;

    $author_profile_ID  = $profile[0];
    $author_profile     = get_post( $author_profile_ID );
    $author_thumbnail   = get_the_post_thumbnail( $author_profile_ID );
    $author_thumbnail   = $author_thumbnail ? $author_thumbnail : OEP_GLOBAL_THUMBNAIL;
    $author_name        = get_the_author_meta( 'display_name' );
    $author_title       = get_field('title', $author_profile_ID);
    $author_bio         = ($author_profile->post_content) ?? "";
    $author_link        = get_author_posts_url( $author_id );
	$author_linkedin    = get_field('url_linkedin', $author_profile_ID);
	$author_twitter     = get_field('url_twitter', $author_profile_ID);
	$author_facebook    = get_field('url_facebook', $author_profile_ID);
	$author_email       = get_field('email', $author_profile_ID);

    $full_name = get_field('name_first', $author_profile_ID);
    $full_name .= $full_name ? ' ' . get_field('name_last', $author_profile_ID) : $author_name;

	$args = [
        'region'        => $region,
		'profile_id'	=> $author_profile_ID,
		'profile'   	=> get_post( $author_profile_ID ),
		'thumbnail' 	=> $author_thumbnail,
		'name' 			=> $full_name,
		'title'     	=> $author_title,
		'bio'      		=> $author_bio,
		'linkedin'     	=> $author_linkedin,
		'twitter'     	=> $author_twitter,
		'facebook'     	=> $author_facebook,
		'email'     	=> $author_email,
		'link'     		=> get_author_posts_url( $author_id ),
	];
	oep_fn_template_part( 'author-block', false, $args );
}

/**
 * Get Author Profile info
 *
 * @param  string $id ID of author's profile post
 * @param  string $region region of the page method is being called (banner, tile, etc)
 *
 * @return string
 *
 * @since  1.0.0
 */
function oep_author_block( $region ) {
    echo oep_get_author_block( $region );
}

/**
 * Get terms for post from selected taxonomies
 *
 * @param  string $id ID of post
 *
 * @return string
 *
 * @since  1.0.0
 */
function oep_get_terms( $id ) {
    $taxonomies = array(
        'oep_taxonomies_qol',
        'oep_taxonomies_industries',
    );
    return wp_get_post_terms( $id, $taxonomies );
}

/**
 * Get terms for post
 *
 * @param  string $id ID of post
 *
 * @return string
 *
 * @since  1.0.0
 */
function oep_get_all_terms( $id ) {
    $taxonomies = array(
        'oep_taxonomies_competitiveness',
        'oep_taxonomies_qol',
        'oep_taxonomies_industries',
        'oep_taxonomies_regions',
        'oep_taxonomies_activity',
        'oep_taxonomies_teams',
        'oep_taxonomies_departments',
        'oep_taxonomies_memberships',
        'oep_taxonomies_ideas',
        'oep_taxonomies_values',
    );
    return wp_get_post_terms( $id, $taxonomies );
}

/**
 * Get posts that share similar taxonomies
 *
 * @param  string $id ID of post
 *
 * @return string
 *
 * @since  1.0.0
 */
function oep_get_related_posts( $id ) {
    $terms = oep_get_all_terms( $id);
    $tax_slugs = [];
	$taxArray = [];

    foreach( $terms as $term ) :
        $term_ids[] = $term->term_id;
        $tax_slugs[] = $term->slug;
        $taxonomies[] = $term->taxonomy . "|" . $term->slug;
        $taxArray['relation'] =  'OR';
        $taxArray[] = array (
            'taxonomy'  => $term->taxonomy,
            'field'     => 'slug',
            'terms'     => $term->slug,
        );
    endforeach;

    $args = Array(
        'posts_per_page'    => 3,
        'post_type'        => array_merge( [ 'post' ], PostType::get_cpt_keys()),
        'post_status'       => 'publish',
        'exclude'           => $id,
        'tax_query'         => Array(
            'relation'      => 'OR',
            $taxArray
        )
    );

    return get_posts($args);

}

function oep_get_post_type( $id ) {
    /**
     * Get posts that share similar taxonomies
     *
     * @param  string $id ID of post
     *
     * @return string
     *
     * @since  1.0.0
     * @TODO: test with other post types once ready; remove test; store icon in CPT registration
     */

    $post_type = get_post_type( $id );
    $icon = "";

    if( OEP_SUCCESS_KEY === $post_type ) :
        return '<span class="post_type"><i class="fas fa-trophy"></i> ' . __("Success Story", "oep") . '</span>';
    elseif( OEP_VIDEO_KEY === $post_type ) :
        return '<span class="post_type"><i class="fas fa-video"></i> ' . __("Video", "oep")   . '</span>';
    elseif( 'post' === $post_type && has_term('press-releases', OEP_BLOG_TYPES_KEY, $id)) :
        return '<span class="post_type"><i class="fas fa-bullhorn"></i> ' . __("Press Release", "oep") . '</span>';
    elseif( OEP_PODCAST_KEY === $post_type ) :
        return '<span class="post_type"><i class="fas fa-podcast"></i> ' . __("Podcast", "oep") . '</span>';
    else :
        return '<span class="post_type"></span>';
    endif;
}

/**
 * Get post type dropdown
 *
 * @since 0.1.0
 */
function oep_post_type_select( $type = NULL ) {

    $type = $type ?: "";

    if ( $type == "post" ) {
        $type = get_query_var( OEP_BLOG_TYPES_KEY );
    }

	if ( ! $types = PostType::get_cpt_keys() ) {
		return;
	}

	$blog_types = get_terms([ 'taxonomy' => OEP_BLOG_TYPES_KEY ]);
    $exclude = [ OEP_PROFILE_KEY, OEP_LANDING_PAGE_KEY, OEP_COMPANY_KEY ];
    ?>

	<select name="post_type">

		<option value="" <?php selected( $type, '' ); ?>><?php _e( 'Everything', 'oep' ); ?></option>

		<?php foreach ( $blog_types as $blog_type ) : // posts, articles, etc. ?>

            <option value="<?php echo esc_attr( $blog_type->slug ); ?>" <?php selected( $type, $blog_type->slug ); ?>><?php echo $blog_type->name; ?></option>

		<?php endforeach; ?>

        <?php foreach ( $types as $key => $value ) : if ( ! in_array( $value, $exclude ) ) : // allowed CPT types ?>

			<option value="<?php echo esc_attr( $value ); ?>" <?php selected( $type, $value ); ?>>
                <?php echo get_post_type_labels( get_post_type_object( $value ) )->menu_name; ?>
            </option>

		<?php endif; endforeach; ?>

	</select>

<?php }

/**
 * Get posts flagged as "trending" by Hubspot
 *
 * @return array
 *
 * @since  1.0.0
 */
function oep_get_trending_posts() {

    $amplifiedID = 1; // ID of Amplified site

    // Create variable to hold results
    $trending = [];

    // @TODO: figure out how to get data from Hubspot
    $args = array(
        'posts_per_page'    => 6,
        'post_status'       => 'publish',
        'post_type'         => [ 'post' ],
    );

    switch_to_blog( $amplifiedID ); // switch to Amplified

    $trending['posts'] = get_posts($args);

    // Loop thru posts and retrieve thumbnail while still on Amplified
    // Assign url as an array item so it can be passed thru to multisite
    foreach( $trending['posts'] as $post ) :
        $post->thumbnail    = oep_get_cover_image( get_post_thumbnail_id( $post->ID ) );
        $post->author_name  = get_the_author_meta( 'display_name', $post->post_author );
        $post->author_link  = get_author_posts_url( $post->post_author );
        $post->posted_on    = oep_get_posted_on();
    endforeach;

    restore_current_blog(); // switch back to main site

    if( !$trending['posts'] ) return false;

    // Return trending and posts object
    return $trending;
}

/**
 * Get posts with "feature" tag
 *
 * @param  string $id ID of post
 *
 * @return array
 *
 * @since  1.0.0
 */
function oep_get_featured_posts( $id ) {
    // Create variable to hold results
    $featured = [];
    $post_terms = [];
    $post_terms['relation'] = 'OR';

    $terms = oep_get_all_terms( $id ); //get all terms from $id

    foreach( $terms as $term ) {
        $post_terms[$term->term_id] = array(
            'taxonomy'  => $term->taxonomy,
            'field'     => 'slug',
            'terms'     => $term->name,
        );
    }

    // Get all posts w/ 'featured' tag
    $args = Array(
        'posts_per_page'    => 1,
        'post_status'       => 'publish',
        'exclude'           => $id,
        'tax_query'         => Array(
            'relation'      => 'AND',
            array (
                'taxonomy'  => 'post_tag',
                'field'     => 'slug',
                'terms'     => 'featured',
            ),
            array (
                $post_terms
            )
        )
    );
    $featured['posts'] = get_posts($args);

    if( !$featured['posts'] ) return false;

    // Get all terms associated w/ 'featured' post
    $terms = oep_get_all_terms( $featured['posts'][0]->ID);
    if( !$terms ) return false;

    // Get name of 1st taxonomy term
    $featured['name'] = $terms[0]->name;

    // Return name and posts object
    return $featured;
}

/**
 * Get 3 upcoming Event posts
 *
 * @return array
 *
 * @since  1.0.0
 */
function oep_get_upcoming_events( $number_of_posts, $id ) {
    // Create variable to hold results
    $event = [];
	$today = date("Ymd"); // Set start date to today
    // Get 3 upcoming event posts
    $args = Array(
        'post_type'         => OEP_EVENT_KEY,
        'posts_per_page'    => $number_of_posts,
        'post_status'       => 'publish',
        'post__not_in'      => [$id],
        'meta_query'        => array(
                array(
	                'key' => 'start_date',
	                'value' => $today,
	                'compare' => '>=',
	                'type' => 'DATE'
                )
        )
        // 'tax_query'         => Array(
        //     array (
        //         'taxonomy'  => 'post_tag',
        //         'field'     => 'slug',
        //         'terms'     => 'featured',
        //     )
        // )
    );
    $event['posts'] = get_posts($args);
    if( !$event['posts'] ) return false;

    foreach( $event['posts'] as $post ) :
        $post->start_date = get_field( 'start_date', $post->ID );
    endforeach;

    // Return name and posts object
    return $event;
}


/**
 * Server rendering for author picks
 * @param $author_picks array of post ids
 * @param $content mixed content
 * @return mixed
 */
function oep_render_author_picks($author_picks, $content) {
//	ALAN("hi", $author_picks);
	if ( empty( $author_picks ) ) {
		return;
	}
	$author_picks_query_args = array(
		'post__in' => $author_picks,
		'post_type' => array_merge( [ 'post' ], PostType::get_cpt_keys())
	);
	$author_picks_query = new \WP_Query($author_picks_query_args);

	oep_fn_template_part( 'author-picks', '', array( 'author_picks_query' => $author_picks_query ) );

}

function hex2rgba( $colour, $alpha ) {
	if ( $colour[0] == '#' ) {
		$colour = substr( $colour, 1 );
	}
	if ( strlen( $colour ) == 6 ) {
		list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
	} elseif ( strlen( $colour ) == 3 ) {
		list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
	} else {
		return false;
	}
	$r = hexdec( $r );
	$g = hexdec( $g );
	$b = hexdec( $b );
	return 'rgba('.$r.', '.$g.', '.$b.', '.$alpha.')';
//	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}


/**
 * Get ACF fields by array of names
 *
 * @since 1.2.0
 * @param  array    $names    field names
 * @param  integer  $post_id  post ID to get field from
 * @return array              get_field() values
 */
function get_fields( $names, $post_id = null ) {

    if ( ! function_exists( 'get_field' ) ) {

        foreach ( $names as $name ) {
            $fields[ $name ] = '';
        }
        return $fields;
    }

    foreach ( $names as $name ) {
        $fields[ $name ] = get_field( $name, $post_id );
    }

    return $fields;
}
