<?php
/**
 * Newsletter Widget Class
 */

namespace OEP\Common;

class Newsletter_Widget extends \WP_Widget {
	// class constructor
	public function __construct() {
		$widget_ops = array(
			'classname' => 'oep-newsletter-widget',
			'description' => 'Newsletter Signup Block',
		);
		parent::__construct( 'oep_newsletter_widget', 'Newsletter Widget', $widget_ops );
	}

	// output the widget content on the front-end
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		ob_start();
		oep_fn_template_part( 'newsletter' );


		echo $args['after_widget'];
		return ob_get_clean();
	}

	// output the option form field in admin Widgets screen
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Title', 'oep' );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_attr_e( 'Title:', 'oep' ); ?>
			</label>

			<input
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				type="text"
				value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
	}

	// save options
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}
}