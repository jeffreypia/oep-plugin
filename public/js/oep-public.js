/**
 * Cost of living calculator
 *
 * @since   1.2.0
 * @package oep
 */
(function( $ ) {
	'use strict';

	// set stuff up
	var $colForm,
		$colCards,
		$colTable,
		$colLoadOverlay,
		colCityFrom,
		colCityTo,
		colCategories,
		colIncomeFrom,
		colIncomeTo;

	// money formatting
	var moneyFormat = Intl.NumberFormat( 'en-US', {
		style: 'currency',
		currency: 'USD',
		minimumFractionDigits: 0,
	});

	// bind
	$(document).ready( init );


	/**
	 * Go!
	 */
	function init() {
		$colForm = $( 'form.cost-of-living' );
		$colLoadOverlay = $( '.loading', $colForm );
		$colCards = $( '.wp-block-oep-col-featured-cards' );
		$colTable = $( '.wp-block-oep-col-table' );
		doColCompare();
	}


	/**
	 * Fire off cost of living functionality
	 *
	 * @since 1.2.0
	 */
	function doColCompare() {

		// start loader
		doColFormLoader();

		// handle API data
		getColComparison( $colForm.serialize() );
	}


	/**
	 * Show/hide the cost of living form loader overlay
	 *
	 * @since 1.2.0
	 */
	function doColFormLoader() {
		$colLoadOverlay.fadeToggle( 400 );
	}


	/**
	 * AJAX the cost of living details
	 *
	 * @param {object}  data  form fields
	 * @since 1.2.0
	 */
	var getColComparison = debounce( function( data ) {

		colIncomeFrom = $( '[name="col_income"]', $colForm ).val();

		$.ajax({
			type: 'POST',
			url: oep.ajaxURL,
			data: {
				action: oep.col.compareAction,
				fields: data,
				do_categories: $colCards.length,
				do_table: $colTable.length,
			},
			success: doColResults,
			complete: doColFormLoader,
		});

	}, 300 );


	/**
	 * Handle cost of living results
	 *
	 * @param {object}  result  AJAX result
	 * @since 1.2.0
	 */
	function doColResults( result ) {

 		// @todo make this show an error or something
		if ( ! result.success ) {
			console.log( 'Cost of Living AJAX failed' );
		}

		colCityFrom = result.data.city_from;
		colCityTo = result.data.city_to;
		colCategories = result.data.categories;
		colIncomeTo = result.data.city_to_salary;

		doTags();
		doColTableResults( result.data.table );
	}


	/**
	 * Populate the cost of living "comparison table" block
	 *
	 * @param {object}  data  table data
	 * @since 1.2.0
	 */
	function doColTableResults( data ) {

		if ( ! data ) {
			return;
		}

		// iterate through table rows
		$( '[data-col-metric]' ).each( function() {

			var $this = $(this);
			var id = $this.data( 'col-metric' );
			var metric = data[ id ];

			if ( ! metric ) {
				return;
			}

			$( '[data-city="from"]', this ).text( metric.cost_from );
			$( '[data-city="to"]' , this ).text( metric.cost_to );
			$( '[data-city="average"]', this ).text( metric.cost_average );
		});
	}


	/**
	 * Populate the cost of living "featured cards" block titles/values
	 *
	 * @see   https://github.com/janl/mustache.js#usage
	 * @since 1.2.0
	 */
	function doTags() {

		// set up mustache "view"
		var tagView = {};

		/**
		 * Configure each of the tags
		 */
		if ( colCityFrom ) {
			tagView.city_from = colCityFrom.Place_Name;
		}

		if ( colCityTo ) {
			tagView.city_to = colCityTo.Place_Name;
		}

		if ( colCategories ) {

			// "%whatever% in %wherever% is %amount% more/less expensive"
			tagView.cost_housing = getCategoryTag( colCategories.Housing );
			tagView.cost_groceries = getCategoryTag( colCategories.Grocery );
			tagView.cost_transportation = getCategoryTag( colCategories.Transportation );
			tagView.cost_healthcare = getCategoryTag( colCategories.Healthcare );

			// "If you earn %income% in %wherever%"
			tagView.income_city_from = moneyFormat.format( colIncomeFrom ) + ' ' + oep.i18n.in + ' ' + colCityFrom.Place_Name;

			// "your comparitive income in %wherever% is %adjusted income%"
			tagView.income_adjusted = oep.i18n.in + ' ' + colCityTo.Place_Name + ' ' + oep.i18n.is + ' ' + colIncomeTo;

			// figure out overall % more/less expensive
			var indexComparison = ( colCityFrom.COST - colCityTo.COST ).toFixed( 2 );
			indexComparison = Math.abs( indexComparison ) + '% ' + ( indexComparison < 0 ? oep.i18n.more : oep.i18n.less );

			// "the cost of living in %wherever% is %amount% more/less expensive
			// than %wherever%"
			tagView.col_city_to = colCityTo.Place_Name + ' ' + oep.i18n.is + ' ' + indexComparison + ' ' + oep.i18n.expensive;
		}


		/**
		 * Conditionally wrap the tags in <em>
		 */
		var noTagEm = [ 'city_from', 'city_to' ];

		$.each( tagView, function( tag, value ) {

			if ( noTagEm.indexOf( tag ) > -1 ) {
				return; // don't wrap if it's excluded
			}

			tagView[ tag ] = '<em class="template-tag tag-' + tag + '">' + value + '</em>';
		});

		// get the stuff we want to do tag replacements on
		var $tagTargets = $( '.wp-block-oep-col-featured-cards .industry-card_title, .wp-block-oep-col-table th' );

		// apply the tags to the contents!
		$tagTargets.each( function() {
			this.innerHTML = Mustache.render( this.innerHTML, tagView );
		});

	} // doTags()


	/**
	 * Get a string for a cost of living category being more/less expensive
	 *
	 * @param {object}  category  category percentage, more/less details
	 * @since 1.2.0
	 */
	function getCategoryTag( category ) {
		return category.percentage + ' ' + category.type + ' ' + oep.i18n.expensive + '.';
	}


	/**
	 * Debounce to reduce overload
	 *
	 * @see   https://davidwalsh.name/javascript-debounce-function
	 * @since 1.2.0
	 */
	function debounce( func, wait, immediate ) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if ( ! immediate ) func.apply( context, args );
		  	};
			var callNow = immediate && ! timeout;
			clearTimeout( timeout );
			timeout = setTimeout( later, wait );
			if ( callNow ) func.apply( context, args );
		};
	}

})( jQuery );

/**
 * Gallery carousel figure height fix?
 *
 * @since   1.0.0
 * @package oep
 */
(function( $ ) {
	'use strict';

	// set stuff up
	var $carousel;

	// bind
	$(document).ready( resetGalleryCarousel );
	$(window).on( 'resize', resetGalleryCarousel );


	/**
	 * Fire off figure height setters for gallery carousel
	 */
	function resetGalleryCarousel() {
		$carousel = $( '.wp-block-blockgallery-carousel > div > div' );
		$carousel.on( 'ready.flickity', setFigureHeight );
		setFigureHeight();
	}


	/**
	 * Dynamically set the height of the figure
	 */
	function setFigureHeight() {

		var $fig = $( '.wp-block-blockgallery-carousel figure' ),
			$maxHeight = 0;

		if ( $fig.length ) {

			$fig.each( function() {

				var $h = $(this).children( 'img' ).outerHeight(),
					$fch = $(this).children( 'figcaption' ).outerHeight(),
					$ft = $(this).position();

				$ft = $ft.top + 25;

				$(this).attr( 'data-height', $h + "px" );

				// set the figcaption to position absolute
				$(this).children( 'figcaption' ).css( 'position', 'absolute' );

				// set the parent height to the image height + the caption height
				$(this).css( 'height', $h + "px" );
				$(this).parent().css( 'height', $h + $fch + "px" );

				// set max height
				$maxHeight = ( $h + $fch ) > $maxHeight ? $h + $fch : $maxHeight;
			});
		}

		// set height of carousel
		$carousel.css( 'height', $maxHeight + "px" );
	}

})( jQuery );

/**
 * Hero slider block
 *
 * @since   1.2.0
 * @package oep
 */
(function($) {

	var $hero,
		$slider,
		$buttonPrev,
		$buttonNext,
		$currentIndex,
		$progressFill,
		flkty,
		numCells;

	$(document).ready( init );

	/**
	 * Instantiate stuff
	 *
	 * @since 1.2.0
	 */
	function init() {

		$hero = $( '.hero-slider' );

		// sanity-check for flickity and slider
		if ( $hero.length < 1 || ! ( 'flickity' in $.fn ) ) {
			return;
		}

		// init and config slider
		$slider = $( '.slides', $hero ).flickity({
			cellAlign: 'left',
			contain: true,
			wrapAround: false,
			pageDots: false,
			prevNextButtons: false,
			fade: true,
		});

		// flickity data
		flkty = $slider.data( 'flickity' );
		numCells = flkty.cells.length;

		// set up some objects
		$buttonPrev = $( '.slide-prev', $hero );
		$buttonNext = $( '.slide-next', $hero );
		$currentIndex = $( '.count .slide-current', $hero );
		$progressBar = $( '.progress-bar', $hero );
		$progressFill = $( '.progress-fill', $hero );

		// update current slide and progress
		$slider.on( 'select.flickity', setCurrentIndex );
		$slider.on( 'select.flickity', setProgress );

		// set and bind buttons
		$buttonPrev.on( 'click', doPrevSlide );
		$buttonNext.on( 'click', doNextSlide );

		setCurrentIndex();
		setProgressFillHeight();
		setProgress();

	} // init()

	/**
	 * Update current slide #/index
	 *
	 * @since 1.2.0
	 */
	function setCurrentIndex() {
		$currentIndex.text( ( flkty.selectedIndex + 1 ).pad() );
	}

	/**
	 * Set the initial progress bar fill height
	 *
	 * @since 1.2.0
	 */
	function setProgressFillHeight() {
		$progressFill.css( 'height', ( ( 1 / numCells ) * 100 ) + "%" );
	}

	/**
	 * Set the progress bar fill height
	 *
	 * @since 1.2.0
	 */
	function setProgress() {
		$progressFill.css( 'transform', "translateY(" + ( flkty.selectedIndex * -100 ) + "%)" );
	}

	/**
	 * Go to the previous slide
	 *
	 * @since 1.2.0
	 */
	function doPrevSlide() {
		$slider.flickity( 'previous' );
	}

	/**
	 * Go to the next slide
	 *
	 * @since 1.2.0
	 */
	function doNextSlide() {
		$slider.flickity( 'next' );
	}

	/**
	 * Add number "padding" function
	 *
	 * @see   https://stackoverflow.com/questions/2998784/how-to-output-integers-with-leading-zeros-in-javascript#answer-11187738
	 * @since 1.2.0
	 */
	Number.prototype.pad = function( size ) {

		var s = String( this );

		while ( s.length < ( size || 2 ) ) {
			s = "0" + s;
		}
		return s;
	};

})( jQuery );

/**
 * Cost of living calculator
 *
 * @since   1.2.0
 * @package oep
 */
(function( $ ) {
	'use strict';

	// bind
	$(document).ready( init );


	/**
	 * Go!
	 */
	function init() {
		var $hsCounter = 0;

		$( '[data-hs-portal-id]' ).each( function() {
			$hsCounter++;

			var $this = $(this);
			var portalID = $this.data( 'hs-portal-id' ),
				formID = $this.data( 'hs-form-id' );

			// Generate unique class to target
			$this.addClass( 'hubspot-form-' + $hsCounter );

			// Render form
			hbspt.forms.create({
				portalId: portalID,
				formId: formID,
				target: '.hubspot-form-' + $hsCounter,
			});
		});
	}

})( jQuery );
