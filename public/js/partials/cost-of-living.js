/**
 * Cost of living calculator
 *
 * @since   1.2.0
 * @package oep
 */
(function( $ ) {
	'use strict';

	// set stuff up
	var $colForm,
		$colCards,
		$colTable,
		$colLoadOverlay,
		colCityFrom,
		colCityTo,
		colCategories,
		colIncomeFrom,
		colIncomeTo;

	// money formatting
	var moneyFormat = Intl.NumberFormat( 'en-US', {
		style: 'currency',
		currency: 'USD',
		minimumFractionDigits: 0,
	});

	// bind
	$(document).ready( init );


	/**
	 * Go!
	 */
	function init() {
		$colForm = $( 'form.cost-of-living' );
		$colLoadOverlay = $( '.loading', $colForm );
		$colCards = $( '.wp-block-oep-col-featured-cards' );
		$colTable = $( '.wp-block-oep-col-table' );
		doColCompare();
	}


	/**
	 * Fire off cost of living functionality
	 *
	 * @since 1.2.0
	 */
	function doColCompare() {

		// start loader
		doColFormLoader();

		// handle API data
		getColComparison( $colForm.serialize() );
	}


	/**
	 * Show/hide the cost of living form loader overlay
	 *
	 * @since 1.2.0
	 */
	function doColFormLoader() {
		$colLoadOverlay.fadeToggle( 400 );
	}


	/**
	 * AJAX the cost of living details
	 *
	 * @param {object}  data  form fields
	 * @since 1.2.0
	 */
	var getColComparison = debounce( function( data ) {

		colIncomeFrom = $( '[name="col_income"]', $colForm ).val();

		$.ajax({
			type: 'POST',
			url: oep.ajaxURL,
			data: {
				action: oep.col.compareAction,
				fields: data,
				do_categories: $colCards.length,
				do_table: $colTable.length,
			},
			success: doColResults,
			complete: doColFormLoader,
		});

	}, 300 );


	/**
	 * Handle cost of living results
	 *
	 * @param {object}  result  AJAX result
	 * @since 1.2.0
	 */
	function doColResults( result ) {

 		// @todo make this show an error or something
		if ( ! result.success ) {
			console.log( 'Cost of Living AJAX failed' );
		}

		colCityFrom = result.data.city_from;
		colCityTo = result.data.city_to;
		colCategories = result.data.categories;
		colIncomeTo = result.data.city_to_salary;

		doTags();
		doColTableResults( result.data.table );
	}


	/**
	 * Populate the cost of living "comparison table" block
	 *
	 * @param {object}  data  table data
	 * @since 1.2.0
	 */
	function doColTableResults( data ) {

		if ( ! data ) {
			return;
		}

		// iterate through table rows
		$( '[data-col-metric]' ).each( function() {

			var $this = $(this);
			var id = $this.data( 'col-metric' );
			var metric = data[ id ];

			if ( ! metric ) {
				return;
			}

			$( '[data-city="from"]', this ).text( metric.cost_from );
			$( '[data-city="to"]' , this ).text( metric.cost_to );
			$( '[data-city="average"]', this ).text( metric.cost_average );
		});
	}


	/**
	 * Populate the cost of living "featured cards" block titles/values
	 *
	 * @see   https://github.com/janl/mustache.js#usage
	 * @since 1.2.0
	 */
	function doTags() {

		// set up mustache "view"
		var tagView = {};

		/**
		 * Configure each of the tags
		 */
		if ( colCityFrom ) {
			tagView.city_from = colCityFrom.Place_Name;
		}

		if ( colCityTo ) {
			tagView.city_to = colCityTo.Place_Name;
		}

		if ( colCategories ) {

			// "%whatever% in %wherever% is %amount% more/less expensive"
			tagView.cost_housing = getCategoryTag( colCategories.Housing );
			tagView.cost_groceries = getCategoryTag( colCategories.Grocery );
			tagView.cost_transportation = getCategoryTag( colCategories.Transportation );
			tagView.cost_healthcare = getCategoryTag( colCategories.Healthcare );

			// "If you earn %income% in %wherever%"
			tagView.income_city_from = moneyFormat.format( colIncomeFrom ) + ' ' + oep.i18n.in + ' ' + colCityFrom.Place_Name;

			// "your comparitive income in %wherever% is %adjusted income%"
			tagView.income_adjusted = oep.i18n.in + ' ' + colCityTo.Place_Name + ' ' + oep.i18n.is + ' ' + colIncomeTo;

			// figure out overall % more/less expensive
			var indexComparison = ( colCityFrom.COST - colCityTo.COST ).toFixed( 2 );
			indexComparison = Math.abs( indexComparison ) + '% ' + ( indexComparison < 0 ? oep.i18n.more : oep.i18n.less );

			// "the cost of living in %wherever% is %amount% more/less expensive
			// than %wherever%"
			tagView.col_city_to = colCityTo.Place_Name + ' ' + oep.i18n.is + ' ' + indexComparison + ' ' + oep.i18n.expensive;
		}


		/**
		 * Conditionally wrap the tags in <em>
		 */
		var noTagEm = [ 'city_from', 'city_to' ];

		$.each( tagView, function( tag, value ) {

			if ( noTagEm.indexOf( tag ) > -1 ) {
				return; // don't wrap if it's excluded
			}

			tagView[ tag ] = '<em class="template-tag tag-' + tag + '">' + value + '</em>';
		});

		// get the stuff we want to do tag replacements on
		var $tagTargets = $( '.wp-block-oep-col-featured-cards .industry-card_title, .wp-block-oep-col-table th' );

		// apply the tags to the contents!
		$tagTargets.each( function() {
			this.innerHTML = Mustache.render( this.innerHTML, tagView );
		});

	} // doTags()


	/**
	 * Get a string for a cost of living category being more/less expensive
	 *
	 * @param {object}  category  category percentage, more/less details
	 * @since 1.2.0
	 */
	function getCategoryTag( category ) {
		return category.percentage + ' ' + category.type + ' ' + oep.i18n.expensive + '.';
	}


	/**
	 * Debounce to reduce overload
	 *
	 * @see   https://davidwalsh.name/javascript-debounce-function
	 * @since 1.2.0
	 */
	function debounce( func, wait, immediate ) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if ( ! immediate ) func.apply( context, args );
		  	};
			var callNow = immediate && ! timeout;
			clearTimeout( timeout );
			timeout = setTimeout( later, wait );
			if ( callNow ) func.apply( context, args );
		};
	}

})( jQuery );
