/**
 * Cost of living calculator
 *
 * @since   1.2.0
 * @package oep
 */
(function( $ ) {
	'use strict';

	// bind
	$(document).ready( init );


	/**
	 * Go!
	 */
	function init() {
		var $hsCounter = 0;

		$( '[data-hs-portal-id]' ).each( function() {
			$hsCounter++;

			var $this = $(this);
			var portalID = $this.data( 'hs-portal-id' ),
				formID = $this.data( 'hs-form-id' );

			// Generate unique class to target
			$this.addClass( 'hubspot-form-' + $hsCounter );

			// Render form
			hbspt.forms.create({
				portalId: portalID,
				formId: formID,
				target: '.hubspot-form-' + $hsCounter,
			});
		});
	}

})( jQuery );
