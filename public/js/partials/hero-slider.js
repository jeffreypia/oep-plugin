/**
 * Hero slider block
 *
 * @since   1.2.0
 * @package oep
 */
(function($) {

	var $hero,
		$slider,
		$buttonPrev,
		$buttonNext,
		$currentIndex,
		$progressFill,
		flkty,
		numCells;

	$(document).ready( init );

	/**
	 * Instantiate stuff
	 *
	 * @since 1.2.0
	 */
	function init() {

		$hero = $( '.hero-slider' );

		// sanity-check for flickity and slider
		if ( $hero.length < 1 || ! ( 'flickity' in $.fn ) ) {
			return;
		}

		// init and config slider
		$slider = $( '.slides', $hero ).flickity({
			cellAlign: 'left',
			contain: true,
			wrapAround: false,
			pageDots: false,
			prevNextButtons: false,
			fade: true,
		});

		// flickity data
		flkty = $slider.data( 'flickity' );
		numCells = flkty.cells.length;

		// set up some objects
		$buttonPrev = $( '.slide-prev', $hero );
		$buttonNext = $( '.slide-next', $hero );
		$currentIndex = $( '.count .slide-current', $hero );
		$progressBar = $( '.progress-bar', $hero );
		$progressFill = $( '.progress-fill', $hero );

		// update current slide and progress
		$slider.on( 'select.flickity', setCurrentIndex );
		$slider.on( 'select.flickity', setProgress );

		// set and bind buttons
		$buttonPrev.on( 'click', doPrevSlide );
		$buttonNext.on( 'click', doNextSlide );

		setCurrentIndex();
		setProgressFillHeight();
		setProgress();

	} // init()

	/**
	 * Update current slide #/index
	 *
	 * @since 1.2.0
	 */
	function setCurrentIndex() {
		$currentIndex.text( ( flkty.selectedIndex + 1 ).pad() );
	}

	/**
	 * Set the initial progress bar fill height
	 *
	 * @since 1.2.0
	 */
	function setProgressFillHeight() {
		$progressFill.css( 'height', ( ( 1 / numCells ) * 100 ) + "%" );
	}

	/**
	 * Set the progress bar fill height
	 *
	 * @since 1.2.0
	 */
	function setProgress() {
		$progressFill.css( 'transform', "translateY(" + ( flkty.selectedIndex * -100 ) + "%)" );
	}

	/**
	 * Go to the previous slide
	 *
	 * @since 1.2.0
	 */
	function doPrevSlide() {
		$slider.flickity( 'previous' );
	}

	/**
	 * Go to the next slide
	 *
	 * @since 1.2.0
	 */
	function doNextSlide() {
		$slider.flickity( 'next' );
	}

	/**
	 * Add number "padding" function
	 *
	 * @see   https://stackoverflow.com/questions/2998784/how-to-output-integers-with-leading-zeros-in-javascript#answer-11187738
	 * @since 1.2.0
	 */
	Number.prototype.pad = function( size ) {

		var s = String( this );

		while ( s.length < ( size || 2 ) ) {
			s = "0" + s;
		}
		return s;
	};

})( jQuery );
