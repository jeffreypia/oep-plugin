/**
 * Gallery carousel figure height fix?
 *
 * @since   1.0.0
 * @package oep
 */
(function( $ ) {
	'use strict';

	// set stuff up
	var $carousel;

	// bind
	$(document).ready( resetGalleryCarousel );
	$(window).on( 'resize', resetGalleryCarousel );


	/**
	 * Fire off figure height setters for gallery carousel
	 */
	function resetGalleryCarousel() {
		$carousel = $( '.wp-block-blockgallery-carousel > div > div' );
		$carousel.on( 'ready.flickity', setFigureHeight );
		setFigureHeight();
	}


	/**
	 * Dynamically set the height of the figure
	 */
	function setFigureHeight() {

		var $fig = $( '.wp-block-blockgallery-carousel figure' ),
			$maxHeight = 0;

		if ( $fig.length ) {

			$fig.each( function() {

				var $h = $(this).children( 'img' ).outerHeight(),
					$fch = $(this).children( 'figcaption' ).outerHeight(),
					$ft = $(this).position();

				$ft = $ft.top + 25;

				$(this).attr( 'data-height', $h + "px" );

				// set the figcaption to position absolute
				$(this).children( 'figcaption' ).css( 'position', 'absolute' );

				// set the parent height to the image height + the caption height
				$(this).css( 'height', $h + "px" );
				$(this).parent().css( 'height', $h + $fch + "px" );

				// set max height
				$maxHeight = ( $h + $fch ) > $maxHeight ? $h + $fch : $maxHeight;
			});
		}

		// set height of carousel
		$carousel.css( 'height', $maxHeight + "px" );
	}

})( jQuery );
