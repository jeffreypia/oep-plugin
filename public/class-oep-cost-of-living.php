<?php
/**
 * Cost of Living Calculator
 *
 * @package OEP
 * @since   1.2.0
 * @author  Jordan Pakrosnis <jordan@designzillas.com>
 */

namespace OEP\Common;

/**
 * Cost of Living
 *
 * @package OEP
 */
class Cost_Of_Living {

	/**
	 * Form keys and values
	 *
	 * @since 1.2.0
	 * @var   string
	 */
	private static $income    = null;
	private static $city_from = null;
	private static $city_to   = null;

	/**
	 * C2ER API key
	 *
	 * @since 1.2.0
	 * @var   string
	 */
	private static $api_key = '{52DA264F-AD48-419C-A04C-2E44263EA846}';

	/**
	 * "To City" place IDs
	 *
	 * @todo create a custom ACF field based on multi-select that pulls the
	 *       places and lets admin choose
	 * @var   integer
	 * @since 1.2.0
	 */
	public static $city_to_ids = [
		66, // Orlando
	];

	/**
	 * Cost of living compoarison AJAX action
	 *
	 * @var   string
	 * @since 1.2.0
	 */
	const COMPARE_ACTION = 'oep_col_compare';

	/**
	 * Get form submission values and set their props
	 *
	 * @since 1.2.0
	 */
	public function set_form_values() {
		self::$income    = oep_get( 'col_income'    );
		self::$city_from = oep_get( 'col_city_from' );
		self::$city_to   = oep_get( 'col_city_to'   );
	}

	/**
	 * Get existing form submission values
	 *
	 * @return array
	 * @since  1.2.0
	 */
	public static function get_form_values() {

		return [
			'income'    => intval( self::$income ),
			'city_from' => self::$city_from,
			'city_to'   => self::$city_to,
		];
	}

	/**
	 * Register to/from place search block
	 *
	 * @since 1.2.0
	 */
	public function register_form_block() {

		$chicken = acf_register_block_type([
			'name'            => 'oep-col-form',
			'title'           => __( 'Cost of Living City Form', 'oep' ),
			'description'     => __( 'Allows visitors to put the city to compare Orlando with.', 'oep' ),
			'render_template' => OEP_PLUGIN_DIR . 'public/partials/col/form.php',
			'category'        => 'oep-col',
			'icon'            => 'analytics',
			'keywords'        => [ 'cost', 'living', 'form', 'city', 'state', 'location' ],
			'mode'            => 'preview',
		]);
	}

	/**
	 * Get form setting fields
	 *
	 * @return array
	 * @since  1.2.0
	 */
	public static function get_form_settings() {

		return get_fields([
			'label_income',
			'label_city_from',
			'label_city_to',
			'button_text',
		]);
	}


	/**
	 * Add stuff to localized JS object
	 *
	 * @param array  $object  existing JS object to be localized
	 * @since 1.2.0
	 */
	public function add_js_props( $object ) {

		$object['col']['compareAction'] = self::COMPARE_ACTION;
		$object['i18n']['expensive']    = __( 'expensive', 'oep' );
		$object['i18n']['in']           = __( 'in', 'oep' );
		$object['i18n']['is']           = __( 'is', 'oep' );
		$object['i18n']['more']         = __( 'more', 'oep' );
		$object['i18n']['less']         = __( 'less', 'oep' );

		return $object;
	}


	/**
	 * Compare two places and send back the results
	 *
	 * Public plugin JS sends AJAX for this
	 *
	 * @since 1.2.0
	 */
	public function get_compare_results() {

		$fields = wp_parse_args( $_POST['fields'], [
			'col_income'    => 80000,
			'col_city_from' => 1,
			'col_city_to'   => 66,
		]);

		// intval() all these
		$fields = array_filter( $fields, 'intval' );

		// get "category" comparison (housing, groceries, etc)
		$categories = intval( $_POST['do_categories'] )
			? self::get_category_comparison( $fields['col_city_from'], $fields['col_city_to'] )
			: false;

		// get "table" comparison (phone, coffee, etc)
		$table = intval( $_POST['do_table'] )
			? self::get_table_comparison( $fields['col_city_from'], $fields['col_city_to'] )
			: false;


		// send it back
		wp_send_json_success([
			'categories'     => $categories,
			'table'          => $table,
			'city_from'      => self::get_place( $fields['col_city_from'] ),
			'city_to'        => self::get_place( $fields['col_city_to'] ),
			'city_to_salary' => self::get_place_to_salary( $fields['col_city_from'], $fields['col_city_to'], $fields['col_income'] ),
		]);
	}


	/**
	 * Get "places" (cities/counties)
	 *
	 * @since  1.2.0
	 * @return array  $places  array of place IDs and names
	 */
	public static function get_places() {

		// if we're up to date, grab the "cached" places
		if ( self::is_places_up_to_date() && $places = get_transient( 'oep_col_places' ) ) {
			return $places;
		}

		// grab table of places from API
		$places = self::get_api( 'GetPlaces', [
			'indexType' => 1,
			'placeType' => 1,
		]);
		$places = @$places->dataSet->Table;

		// reorganize to place ID => place name and cache
		if ( $places ) {
			$places = wp_list_pluck( $places, 'FromPlace', 'Place_Id' );
			set_transient( 'oep_col_places', $places, DAY_IN_SECONDS * 3 );
		}

		return $places;
	}


	/**
	 * Get "category comparison"
	 *
	 * For comparing two places' groceries, housing, etc
	 *
	 * @param  integer  $from  from city ID
	 * @param  integer  $to    to city ID
	 * @return array
	 *
	 * @since 1.2.0
	 */
	private static function get_category_comparison( $from, $to ) {

		// send API call
		$categories = self::get_api( 'GetExpenditureCategoryComparison', [
			'indexType' => 1,
			'fromPlace' => $from,
			'toPlace'   => $to,
		]);

		// response dataSet object will have a "Table1" prop in it; this gets it
		$categories = @(Array)$categories->dataSet;

		// sanity check: there were categories
		if ( empty( $categories ) ) {
			return false;
		}

		// grab the actual nested "table"
		$categories = array_shift( $categories );

		// build reorganized array
		foreach ( $categories as $cat ) {
			$categories_blessed[ $cat->Category ] = [
				'percentage' => $cat->PercentageDifference,
				'type'       => $cat->DifferenceType,
			];
		}

		return $categories_blessed;
	}


	/**
	 * Get "table comparison"
	 *
	 * For comparing two place's phone bill, gasoline, beer, etc.
	 *
	 * @param  integer  $from  from city ID
	 * @param  integer  $to    to city ID
	 * @return array
	 *
	 * @since  1.2.0
	 */
	private static function get_table_comparison( $from, $to ) {

		$table = self::get_api( 'GetAveragePriceComparison', [
			'indexType' => 1,
			'fromPlace' => $from,
			'toPlace'   => $to,
		]);

		// response dataSet object will have a "Table1" prop in it; this gets it
		$table = @(Array)$table->dataSet;

		// sanity check: there's a table
		if ( empty( $table ) ) {
			return false;
		}

		// grab the actual nested "table"
		$table = array_shift( $table );

		// build reorganized array
		foreach ( $table as $row ) {

			$table_blessed[ $row->Category_ID ] = [
				'label'        => $row->Category_Name,
				'cost_from'    => $row->FromPlaceCost,
				'cost_to'      => $row->ToPlaceCost,
				'cost_average' => $row->NationalAverageCost,
			];
		}

		return $table_blessed;
	}


	/**
	 * Get city details
	 *
	 * @param  integer  $city_id  city ID
	 * @return array
	 *
	 * @since  1.2.0
	 */
	private static function get_place( $city_id ) {

		$place = self::get_api( 'GetPlaceDataByID', [
			'indexType' => 1,
			'placeType' => 1,
			'placeID'   => $city_id,
		]);

		return @$place->dataSet->Table[0] ?: false;
	}


	/**
	 * Get place/city salary comparison
	 *
	 * @param  integer  $from    city ID
	 * @param  integer  $to      city ID
	 * @param  integer  $income  yearly income
	 * @return string            what the salary in the city you're from is worth in the city you're going to
	 *
	 * @since  1.2.0
	 */
	private static function get_place_to_salary( $from, $to, $income ) {

		$salary = self::get_api( 'GetSalaryComparison', [
			'indexType'         => 1,
			'fromPlaceId'       => $from,
			'toPlaceId'         => $to,
			'currentBaseSalary' => $income,
		]);

		return $salary ? $salary->comparativeSalary : false;
	}


	/**
	 * Check if our "places" list is up to date
	 *
	 * @return bool  whether or not the "places" need to get updated
	 * @since  1.2.0
	 */
	private static function is_places_up_to_date() {

		// current currently saved/cached index date
		if ( get_transient( 'oep_col_index_date_check' ) ) {
			return true;
		}

		// we need to check again, so grab the latest import info from API
		$index = self::get_api( 'GetIndexImportInformation', [
			'indexType' => 1,
		]);

		// let's remember that we checked
		set_transient( 'oep_col_index_date_check', true, DAY_IN_SECONDS );

		// if we got something, create a date object
		$import_date = $index
			? new \DateTime( $index->dateImported )
			: false;

		// compare the api date with what we have saved
		if ( $import_date && ( $import_date > get_option( 'oep_col_index_date' ) ) ) {
			update_option( 'oep_col_index_date', $import_date );
			return false;
		}

		return true;
	}

	/**
	 * General C2ER API call
	 *
	 * @see   https://api.c2er.org/costofliving/v3.0/api/swagger/ui/index
	 * @since 1.2.0
	 *
	 * @param  string  $endpoint  what we're trying to get
	 * @param  array   $args      extra params for the request
	 * @return mixed              API response
	 */
	private static function get_api( $endpoint, $args ) {

		$request = add_query_arg(
			array_merge([
				'licenseeGuid' => self::$api_key,
			], $args ),
			"https://api.c2er.org:443/costofliving/v3.0/api/api/{$endpoint}"
		);

		$response = wp_remote_get( $request );

		if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) !== 200 ) {
			self::do_api_error_email( $endpoint, print_r( $response, true ) );
			return false;
		}

		$body = wp_remote_retrieve_body( $response );

		return $body ? json_decode( $body ) : false;
	}

	/**
	 * Send API error email to admin
	 *
	 * Sends an error message to admin, limited to one per two hours for any
	 * given endpoint.
	 *
	 * @param  string  $endpoint  the API endpoint involved
	 * @param  string  $message   body of email
	 *
	 * @since 1.2.0
	 */
	private static function do_api_error_email( $endpoint, $message ) {

		if ( get_transient( "oep_col_api_{$endpoint}_error_sent" ) ) {
			return;
		}

		wp_mail(
			get_option( 'admin_email' ),
			sprintf( __( 'OEP could not retreive C2ER %s data' ), $endpoint ),
			sprintf(
				__( '%sError returned from remote call:%s%s%s' ),
				'<b>', '</b>', "<br/><br/>", "<code><pre>$message</pre></code>"
			),
			[ 'Content-Type: text/html; charset=UTF-8' ]
		);

		set_transient( "oep_col_api_{$endpoint}_error_sent", true, HOUR_IN_SECONDS );
	}
}
