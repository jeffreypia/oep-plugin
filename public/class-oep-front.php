<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    OEP
 * @subpackage OEP/public
 * @author     Ryan Smith <ryan@designzillas.com>
 */

namespace OEP\Common;

class Front {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $oep The ID of this plugin.
	 */
	private $oep;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 *
	 * @param      string $oep     The name of the plugin.
	 * @param      string $version The version of this plugin.
	 */
	public function __construct( $oep, $version ) {

		$this->oep     = $oep;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in OEP_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The OEP_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->oep, plugin_dir_url( __FILE__ ) . 'css/oep-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in OEP_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The OEP_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		/**
		 * Hubspot js for rendering forms
		 */
		wp_enqueue_script( 'oep-hubspot', '//js.hsforms.net/forms/v2.js', array(), null, false );

		wp_enqueue_script( $this->oep, plugin_dir_url( __FILE__ ) . 'js/oep-public.min.js', array( 'jquery' ), $this->version, false );

		wp_localize_script( $this->oep, 'oep', apply_filters( 'oep_public_js_object', [
			'ajaxURL' => admin_url( 'admin-ajax.php' ),
		]));

		// stuff with mustache templates
		if ( has_block( 'oep/col-featured-cards' ) || has_block( 'oep/col-table' ) ) {

			wp_enqueue_script(
				$this->oep . '-mustache',
				plugin_dir_url( __FILE__ ) . 'js/mustache.min.js',
				array(),
				'3.0.2'
			);
		}
	}


	/**
	 * Set blog archive title
	 * @param $title
	 *
	 * @return string|void
	 */
	public function set_archive_title( $title ) {
		return is_home() ? __("News", "oep") : $title;
	}

}
