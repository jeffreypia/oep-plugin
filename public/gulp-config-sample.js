/**
 * Local vars for gulpfile.babel.js
 *
 * @package oep
 */
var os = require('os');
var homeDir = os.homedir();

const localGulpConfig = {
    bsURL    : 'https://oep.test/',
    bsOpen   : false,
    keyPath  : homeDir + '/DZ/mamp-certificates/oep.test.key',
    certPath : homeDir + '/DZ/mamp-certificates/oep.test.crt',
}
// const localGulpConfig = {
//     bsURL    : 'https://lwp.test/',
//     bsOpen   : false,
//     keyPath  : homeDir + '/DZ/mamp-certificates/lwp.test.key',
//     certPath : homeDir + '/DZ/mamp-certificates/lwp.test.crt',
// }
export { localGulpConfig };