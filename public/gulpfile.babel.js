/**
 * Plugin public asset gulp tasks
 *
 * Make sure browsersync proxy url is set!
 *
 * @since 1.2.0
 * @package oep
 */

/**
 * Grab gulp packages
 */
var gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync').create()

// grab local vars
import { localGulpConfig } from './gulp-config-local.js'


/**
 * JShint, concat, and minify JS
 */
gulp.task( 'js', function() {

	return gulp.src([
	'./js/partials/*.js'
	])
	.pipe( plumber() )
	.pipe( sourcemaps.init() )
	.pipe( jshint() )
	.pipe( jshint.reporter( 'jshint-stylish' ) )
	.pipe( concat( 'oep-public.js' ) )
	.pipe( gulp.dest( './js' ) )
	.pipe( rename({ suffix: '.min' }) )
	.pipe( uglify() )
	.pipe( sourcemaps.write( '.' ) ) // Creates sourcemap for minified JS
	.pipe( gulp.dest( './js' ) )
})

/**
 * BrowserSync Config
 */
gulp.task( 'browsersync', function () {

	// watch files
	var files = [
		'./js/*.js',
	]

	var bsConfig = {
		proxy: localGulpConfig.bsURL,
		open: localGulpConfig.bsOpen,
		snippetOptions: { whitelist: ['/wp-admin/admin-ajax.php'], blacklist: ['/wp-admin/**'] },
		reloadDelay: 3000,
		browser: 'chrome'
	}

	if ( localGulpConfig.keyPath && localGulpConfig.certPath ) {
		bsConfig.https = {
			key: localGulpConfig.keyPath,
			cert: localGulpConfig.certPath,
		}
	}

	browserSync.init( files, bsConfig );

	gulp.watch( 'js/partials/*.js', ['js'] ).on( 'change', browserSync.reload )
})

/**
 * Non-Browsersync JS/SASS watch
 */
gulp.task( 'watch', function() {
	gulp.watch( 'js/partials/*.js', ['js'] )
})

/**
 * Default: Run style and js tasks
 */
gulp.task( 'default', function() {
	gulp.start( 'js' )
})
