<?php
/**
 * Company Cards Block
 *
 * @since   1.2.0
 * @package oep
 */

extract( OEP\Common\get_fields([
	'companies',
]));

if( !$companies ) return;

$attrs = OEP\Common\oep_fn_attrs_class([
	'company-cards',
	'full-width',
	@$block['className'],
]);
?>

<section <?php echo $attrs; ?>>
	<div class="company-cards_container">

		<?php if ( !empty ( $companies ) ) : ?>

			<?php foreach( $companies as $id ) : ?>

				<?php
					OEP\Common\oep_fn_template_part( 'company-card', '', [ 'company_id' => $id ] );
				?>

			<?php endforeach; ?>

		<?php endif; ?>

	</div>

</section>