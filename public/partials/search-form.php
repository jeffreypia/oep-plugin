<?php
/**
 * Default/header search form
 *
 * @since   1.0.0
 * @package oep
 */
?>
<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ) ; ?>">

	<div class="wrap">

		<!-- keyword -->
		<label for="s">
			<span class="screen-reader-text"><?php _e( 'Search for:', 'label' ); ?></span>
			<input <?php echo OEP\Common\oep_fn_attrs([
				'type'        => 'search',
				'class'       => 'search-field',
				'placeholder' => esc_attr_x( 'Search', 'placeholder' ),
				'value'       => get_search_query(),
				'name'        => 's',
			]); ?> />
			<input type="submit" class="s_submit" value="<?php _e( 'Submit Search', 'oep' ); ?>" />
			<i class="fas fa-search"></i>
		</label>

	</div>

</form>
