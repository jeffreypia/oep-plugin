<?php
/**
 * Render event block
 *
 * @since   1.0.0
 * @package oep
 * @subpackage OEP/public/partials
 * @var $block array of event details
 */

$event_id = array_shift($block["data"]);
$bg_image = get_post_thumbnail_id( $event_id );
$date = get_field( 'start_date', $event_id ) ? new DateTime(get_field( 'start_date', $event_id )) : new DateTime();
?>

<div class="event-block">
    <div class="wrap">
        <div class="event_date">
            <strong><?php echo date_format( $date, 'd' ); ?></strong>
		    <?php echo date_format( $date, 'M' ); ?>
        </div>
        <div class="event_details">
        <!-- title -->
        <h2><?php echo get_the_title( $event_id ); ?></h2>
        <a href="<?php echo get_permalink($event_id); ?>"><span class="event_cta"><?php _e('View Event Details') ?></span></a>
        </div>
    </div>
    <!-- background image -->
	<?php oep_cover_image( oep_get_bg_image( $bg_image ?? null ) ?: OEP_GLOBAL_BANNER, 'banner' ); ?>
</div>