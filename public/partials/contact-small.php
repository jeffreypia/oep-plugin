<?php
/**
 * Render contact block
 *
 * @var $attributes array of settings from the contact block
 * @var $content actual block content
 *
 * @since   1.0.0
 * @package oep
 * @subpackage OEP/public/partials
 */

extract( $args );
?>

<div class="contact-form">
    <?php echo $content ;?>
    
    <div class="hubspot-form" data-hs-portal-id="270308" data-hs-form-id="5ff4eee3-f7c2-45b1-b1e8-4dd0ecd65fa2"></div>
</div>