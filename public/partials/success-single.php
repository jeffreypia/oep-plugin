<?php
/**
 * Single Success Story Partial
 *
 * @since   1.0.0
 * @package oep
 */
extract( OEP\Common\get_fields([
	'quote',
	'name',
	'company',
	'title',
	'video',
], $story_id ) );

$attrs = OEP\Common\oep_fn_attrs_class([
	'single-success',
	@$block['className'],
]);
?>

<div <?php echo $attrs; ?>>
	<?php echo $quote ? "<h2>&quot;$quote&quot;</h2>" : __( 'This is a quote.' ); ?>

	<?php if (!empty($name)){
		echo "<p>– $name";

		if (!empty($title)){
			echo ",<span> $title</span>";
		}

		if (!empty($company)){
			echo "<span> at $company</span>";
		}
		echo "</p>";
	}; ?>

	<?php if (!empty($video)){ ?>

		<a href="#" data-featherlight="#video-light-box" class="play-button">
			<span class="play-icon"><i class="fas fa-play"></i></span>
			<p><?php _e( 'Watch Video' ); ?></p>
		</a>

		<div class="lightbox" id="video-light-box">
			<video controls autoplay>
				<source src="<?php echo $video['url']; ?>" type="video/<?php echo $video['subtype']; ?>">
			</video>
		</div>

	<?php  }; ?>

	<a href="<?php echo get_post_type_archive_link( 'oep_success_story' ); ?>" class="read-more"> <?php _e( 'See More Success Stories' ); ?> <i class="fal fa-arrow-right"></i></a>
</div>

