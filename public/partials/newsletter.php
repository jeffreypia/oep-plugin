<?php
/**
 * Default newsletter block.
 *
 * @since   1.0.0
 * @package oep
 */
?>

<!--Newsletter Form-->
<div class="newsletter">
	<p class="newsletter-header"><?php _e('Sign up for email updates'); ?></p>

	<div class="hubspot-form" data-hs-portal-id="270308" data-hs-form-id="8bb4c8ba-cb02-4ac5-89ad-4bcc781a7694"></div>
</div>
