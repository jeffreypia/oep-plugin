<?php

/**
 * Render title with menu block
 * @var $terms array of terms from block data
 * @var $description
 *
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/public/partials
 */

extract($args);
?>
<div class="wrap">
    <header class="page-header">
        <div class="accent" role="presentation" style="color: <?php \OEP\Taxonomies\Taxonomy::get_color(); ?>"
             data-color="<?php \OEP\Taxonomies\Taxonomy::get_color(); ?>">
			<?php oep_svg( 'heading' ); ?>
        </div>
		<?php
		the_title( '<h1 class="page-title">', '</h1>' );
		?>
        <div class="archive-description"><p class="detail"><?php echo $description ?></p></div>
    </header><!-- .page-header -->
    <ul class="terms">
        <?php
//        do_action( 'taxonomy_get_terms', $terms );
        do_action( 'get_submenu', (int) get_the_ID(), $terms );
        ?>
    </ul>
</div>
<?php

