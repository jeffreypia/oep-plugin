<?php
/**
 * Hero slider block
 *
 * @since   1.2.0
 * @package oep
 */

extract( OEP\Common\get_fields([
	'slides',
	'scroll_label',
	'divider_color',
]));

if ( ! $slides ) {
	echo is_admin() ? '<h2>' . __( 'Click and add some slides!', 'oep' ) . '</h2>' : '';
	return;
}

$attrs = OEP\Common\oep_fn_attrs_class([
	'hero-slider',
	'full-width',
	@$block['className'],
]);
?>

<header <?php echo $attrs; ?>>

	<!-- slides -->
	<div class="slides">
		<?php foreach ( $slides as $slide ) : ?>
			<figure class="slide">

				<?php oep_cover_image( $slide['image'] ?: OEP_GLOBAL_BANNER , 'banner' ); ?>

				<?php if ( $slide['title'] ) : ?>
					<figcaption class="wrap banner-heading">
						<h2><?php echo $slide['title']; ?></h2>
					</figcaption>
				<?php endif; ?>

			</figure>
		<?php endforeach; ?>
	</div>

	<!-- "scroll down" thing -->
	<?php if ( $scroll_label ) : ?>
		<div class="scroll-down">

			<h4><?php echo $scroll_label; ?></h4>

			<div class="scroller">
				<p class="screen-reader-text"><?php __( 'Scroll to see more about Orlando', 'oep' ); ?></p>
				<div class="scroller-triangle"></div>
				<div class="scroller-wheel"></div>
			</div>

		</div>
	<?php endif; ?>

	<!-- slide progress -->
	<div class="hero-slider-navigation">

		<?php oep_svg( 'triangle-dots-upper-left' ); ?>

		<div class="progress-bar">
			<div class="progress-fill"></div>
		</div>

		<div class="count">
			<span class="slide-current">01</span>/<?php printf( '%02d', count( $slides ) ); ?>
		</div>

		<div class="prev-next-buttons">

			<button class="slide-prev text-only">
				<span class="screen-reader-text"><?php _e( 'Previous Slide' ); ?></span>
				<i class="far fa-arrow-left"></i>
			</button>
			<button class="slide-next text-only">
				<span class="screen-reader-text"><?php _e( 'Next Slide' ); ?></span>
				<i class="far fa-arrow-right"></i>
			</button>

		</div>

	</div><!-- .hero-slider-navigation -->

	<?php do_action( 'oep_angled_divider', $divider_color ?: 'white' ); ?>

</header>
