<?php
/**
 * Company Card Partial
 *
 * @since   1.2.0
 * @package oep
 */

extract( OEP\Common\get_fields([
	'industry',
	'company_size',
	'job_titles',
	'website',
	'description',
	'logo',
], $company_id));

$logo = wp_get_attachment_image( $logo, 'thumbnail', false, ['class' => 'company-card_logo']);

?>

<figure class="company-card" data-featherlight="#company-<?php echo $company_id; ?>" data-featherlight-variant="-company-card">

	<figcaption class="company-card_text">
		<h3 class="company-card_title"><?php echo get_the_title( $company_id ); ?> <i class="fal fa-long-arrow-right"></i></h3>

		<div class="company-card_description">
			<?php echo get_the_excerpt( $company_id ); ?>
		</div>
	</figcaption>

	<?php echo $logo; ?>
</figure>

<div class="company-modal" id="company-<?php echo $company_id; ?>">
	<div class="logo-region">
		<?php echo $logo; ?>
	</div>

	<div class="text-area">
		<div class="company-details">
			<p><span><?php _e('Company') ?>:</span> <?php echo get_the_title( $company_id ); ?></p>

			<p><span><?php _e('Industry') ?>: </span>

			<!-- List Industry Taxonomies -->
			<?php
				// Get the taxonomy's terms
				$terms = get_the_terms( $company_id, 'oep_taxonomies_industries');

				// Check if any term exists
				if ( ! empty( $terms ) && is_array( $terms ) ) {

					// Run a loop and print them all
					foreach ( $terms as $term ) { ?>
						<span class="industry"><?php echo $term->name; ?><span class="comma">, </span></span>
					<?php
					}
				}

				else{
					echo "N/A";
				}
			?>

			</p>

			<?php echo $company_size ? "<p><span>" . __('No. of Employees') . ": </span>$company_size</p>" : ''; ?>
			<?php echo $job_titles ? "<p><span>" . __('Top Job Titles') . ": </span>$job_titles</p>" : ''; ?>
			<?php echo $website ? "<p><span>" . __('Website') . ": </span><a href=" . esc_url($website) . " target='_blank'>$website</a></p>" : ''; ?>
		</div>

		<?php echo $description ? "<p class='description'><span>" . __('Description') . ":</span>$description</p>" : ''; ?>
	</div>
</div>
