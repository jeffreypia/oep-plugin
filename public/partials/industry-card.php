<?php
// For each card, extract respective fields and output markup
if( isset( $args ) && $args['extract_card'] ) :
    extract( $card['industry_card'] );
else :
    extract( OEP\Common\get_fields([
        'image',
        'link',
        'title',
        'excerpt',
        'subtitle',
    ]));
endif;

$attrs = OEP\Common\oep_fn_attrs_class([
    'industry-card',
    @$block['className'], // only applies to ACF block
]);
?>

<article <?php echo $attrs; ?>>
    <?php if( $image ) : ?>
    <figure class="industry-card_thumbnail">
        <?php oep_cover_image( $image ) ?>
    </figure>
    <?php endif; ?>

    <?php if( $link ) : ?>
    <a class="industry-card_link" href="<?php echo $link; ?>"><?php $link; ?></a>
    <?php endif; ?>

    <div class="industry-card_content">
        <?php if( $title ) : ?>
        <h1 class="industry-card_title"><?php echo $title; ?></h1>
        <?php endif; ?>

        <?php if( $excerpt ) : ?>
        <p class="industry-card_excerpt"><?php echo $excerpt; ?></p>
        <?php endif; ?>

        <?php if( $subtitle ) : ?>
        <h2 class="industry-card_subtitle"><?php echo $subtitle; ?></h2>
        <?php endif; ?>
    </div>
</article>