<?php

/**
 * Template for featured posts block
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP/CPT
 * @subpackage OEP/CPT/partials
 */

    $featuredPosts = OEP\Common\oep_get_featured_posts($post->ID);

    if( $featuredPosts ) :
?>

<div class="widget posts --featured">
    <h2 class="widget-title"><?php _e('Featured in'); ?> <strong><?php echo $featuredPosts['name'] ?></strong></h2>

    <ul class="posts">
        <?php foreach( $featuredPosts['posts'] as $post ) : ?>
        <li class="post">
            <a class="post_link" href="<?php echo get_the_permalink(); ?>"></a>

            <figure class="post_thumbnail">
                <div class="pills-container"><?php oep_get_pills(); ?></div>
	            <?php
	            if(in_array( get_post_type($post), [ OEP_VIDEO_KEY ])) {
		            do_action('media_thumbnail', $post);
	            } else {
		            ?>
                    <div class="post_thumbnail-wrap">
			            <?php the_post_thumbnail('thumbnail'); ?>
                    </div>
		            <?php
	            }
	            ?>

                <figcaption>
                    <h3 class="post_title"><a href="<?php echo get_the_permalink(); ?>"> <?php echo $post->post_title; ?></a></h3>
                    <footer class="entry-footer">
		                <?php oep_posted_by( 'tile' ); ?> <?php oep_posted_on(); ?>
                    </footer>
                </figcaption>
            </figure>
        
        </li>
        <?php endforeach; ?>
    </ul>
</div><!-- .--featured -->

<?php endif; ?>