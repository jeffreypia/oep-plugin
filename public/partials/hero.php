<?php
/**
 * Render hero block
 *
 * @var $attributes array of stuff from the hero block
 *
 * @since   1.0.0
 * @package oep
 * @subpackage OEP/public/partials
 */

extract( $args );
$attrs = OEP\Common\oep_fn_attrs_class([
    'banner',
    isset( $attributes['mediaID'] ) && $attributes['mediaID'] ? 'has-bg-image' : null,
    'full-width',
]);
?>

<header <?php echo $attrs; ?>>
    <div class="wrap banner-heading">
	    <!-- subtitle -->
	    <h3><?php echo $attributes['subtitle'] ?? ''; ?></h3>
        <!-- title -->
        <h1><?php echo isset( $attributes['title'] ) && ! empty( $attributes['title'] ) ? $attributes['title'] : get_the_title( get_the_ID() ); ?></h1>

        <div>
	        <?php echo isset( $attributes['body'] ) && ! empty( $attributes['body'] ) ? $attributes['body'] : "" ; ?>
        </div>
        <!-- cta -->
        <?php echo isset( $content ) && ! empty( $content ) ? $content : "" ; ?>
    </div>

    <!-- background image -->
	<?php oep_cover_image( oep_get_bg_image( $attributes['mediaID'] ?? null ) ?: null, 'banner' ); ?>

    <?php do_action( 'oep_angled_divider', @$attributes['dividerColor'] ?: 'white', true ); ?>

</header>
