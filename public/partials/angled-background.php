<?php
/**
 * Angled background block template
 *
 * @param   array   $attrs    block attributes
 * @param   string  $content  block contents (inner blocks)
 *
 * @since   1.2.0
 * @package oep
 */
$overflow = @$attrs['hideBackgroundOverflow'];
$block_id = 'block-' . @$attrs['clientId'];

$wrap = \OEP\Common\oep_fn_attrs_class([
	'wp-block-oep-angled-background',
	@$attrs['className'],
	'angled-background-' . ( @$attrs['direction'] ?: 'down' ),
	@$attrs['backgroundGradient'] ? 'angled-background-gradient-' . $attrs['backgroundGradient'] : null,
	@$attrs['backgroundGradient'] === null ? 'angled-background-gradient-default' : null,
	@$attrs['backgroundGradient'] === ''   ? 'angled-background-no-gradient' : null,
	@$attrs['backgroundImageID'] ? 'angled-background-has-image' : null,
	@$attrs['noTopMargin']     ? 'no-top-margin'     : null,
	@$attrs['noBottomMargin']  ? 'no-bottom-margin'  : null,
	@$attrs['noTopPadding']    ? 'no-top-padding'    : null,
	@$attrs['noBottomPadding'] ? 'no-bottom-padding' : null,
	'full-width',
	$block_id,
]);

$background = \OEP\Common\oep_fn_attrs_class([
	'angled-background',
	$overflow === null /*(default)*/ || $overflow === true || @$attrs['backgroundImageID']
		? 'no-overflow'
		: null,
]);
?>

<div <?php echo $wrap; ?>>

	<?php if ( function_exists( 'lwp_do_gradient_accent' ) ) : ?>

		<?php if ( @$attrs['topLeftAccent'] ) : ?>
			<div class="angled-background-accent angled-background-accent-top-left">
				<?php lwp_do_gradient_accent( $attrs['topLeftAccent'] ); ?>
			</div>
		<?php endif; ?>

		<?php if ( @$attrs['topRightAccent' ] ) : ?>
			<div class="angled-background-accent angled-background-accent-top-right">
				<?php lwp_do_gradient_accent( $attrs['topRightAccent'], 'left' ); ?>
			</div>
		<?php endif; ?>

		<?php if ( @$attrs['bottomLeftAccent'] ) : ?>
			<div class="angled-background-accent angled-background-accent-bottom-left">
				<?php lwp_do_gradient_accent( $attrs['bottomLeftAccent'] ); ?>
			</div>
		<?php endif; ?>

		<?php if ( @$attrs['bottomRightAccent' ] ) : ?>
			<div class="angled-background-accent angled-background-accent-bottom-right">
				<?php lwp_do_gradient_accent( $attrs['bottomRightAccent'], 'left' ); ?>
			</div>
		<?php endif; ?>

	<?php endif; ?>

	<div <?php echo $background; ?>>

		<?php oep_cover_image( @$attrs['backgroundImageID'], 'banner' ); ?>

		<?php @$attrs['leftIconAccent'] ? oep_svg( 'icon-accent-reversed', 'icon-accent-left' ) : null ?>
		<?php @$attrs['rightIconAccent'] ? oep_svg( 'icon-accent', 'icon-accent-right' ) : null ?>

	</div>

	<div class="angled-background-content entry-content-dark">
		<?php echo $content; ?>
	</div>

</div>

<?php if ( @$attrs['topOffset'] || @$attrs['bottomOffset'] ) : ?>
<style type="text/css">
<?php
echo ( $offset = @$attrs['topOffset'] ) ?
	".$block_id .angled-background {
		top: {$offset}px;
	}
	.$block_id .angled-background-accent-top-left,
	.$block_id .angled-background-accent-top-right {
		top: {$offset}px;
	}
	"
: '';
echo ( $offset = @$attrs['bottomOffset'] ) ?
	".$block_id .angled-background {
		bottom: {$offset}px;
	}
	.$block_id .angled-background-accent-bottom-left,
	.$block_id .angled-background-accent-bottom-right {
		bottom: {$offset}px;
	}
	"
: '';
?>
</style>
<?php endif;
