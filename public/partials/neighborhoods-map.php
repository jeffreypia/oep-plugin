<?php
/**
 * Stats Block
 *
 * @since   1.2.0
 * @package oep
 */

extract( OEP\Common\get_fields([
	'title',
	'map_embed',
	'default_image',
]));

$attrs = OEP\Common\oep_fn_attrs_class([
	'neighborhood-map',
	@$block['className'],
]);
?>

<section <?php echo $attrs; ?>>

	<div class="wrap">

		<div class="heading">
			<h2><?php echo $title ?: "<b>Explore</b> The Area"; ?></h2>
		</div>

		<?php if ( !empty ( $map_embed ) ) : ?>
			<div class="map">
				<?php echo $map_embed; ?>
			</div>

		<?php else : ?>
			<figure>
				<div>
					<?php oep_cover_image($default_image, 'large');  ?>
				</div>
			</figure>

		<?php endif; ?>

	</div>

</section>
