<?php
/**
 * Stats Block
 *
 * @since   1.0.0
 * @package oep
 */
extract( OEP\Common\get_fields([
	'statistic',
	'attribution',
	'year',
	'icon',
], $stat_id ) );
?>

<li class="stat">
	<div class="icon">
		<?php echo wp_get_attachment_image( $icon ); ?>
	</div>
	<div class="stat-text">
		<?php echo $statistic ? "<h4>$statistic</h4>" : __( 'This is a stat.' ); ?>
		<div class="attribution">
			<?php echo $attribution ? "<p>$attribution, $year</p>" : __('This is an attribution.'); ?>
		</div>
	</div>
</li>
