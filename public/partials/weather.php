<?php

/**
 * Template for weather block
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP/CPT
 * @subpackage OEP/CPT/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="weather">
	<p class="location"><?php _e( 'Orlando, FL', 'oep' ); ?></p>
    <p class="conditions"></p>
	<p class="degrees"></p>
</div>
