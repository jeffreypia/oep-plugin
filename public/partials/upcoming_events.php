<?php

/**
 * Template for upcoming events block
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP/CPT
 * @subpackage OEP/CPT/partials
 */

    // need query to pull 3 upcoming events
    $upcomingEvents = OEP\Common\oep_get_upcoming_events( 3, $post->ID );

    if( $upcomingEvents ) :
?>

<div class="widget posts --upcoming-events">
    <h2 class="widget-title"><?php _e('Upcoming Events'); ?></h2>

    <ul class="posts">
        <?php
            foreach( $upcomingEvents['posts'] as $post ) :
                $date = new DateTime($post->start_date)
        ?>
        <li class="post event">
            <a class="post_link" href="<?php echo get_the_permalink(); ?>"></a>

            <div class="event_date event_date_large">
                <strong><?php echo date_format( $date, 'd' ); ?></strong>
                <?php echo date_format( $date, 'M' ); ?>
            </div>

            <figure class="post_thumbnail">
                <div class="post_thumbnail-wrap">
                    <?php the_post_thumbnail('thumbnail'); ?>
                </div>

                <figcaption>
                    <h3 class="post_title"><?php echo $post->post_title; ?></h3>
                    
                    <span class="event_cta"><?php _e('View Event Details') ?></span>
                </figcaption>
            </figure>
        
        </li>
        <?php endforeach; ?>
    </ul>

    <a class="more-link" href="<?php echo get_post_type_archive_link( OEP_EVENT_KEY ); ?>"><?php _e('View More Upcoming Events'); ?></a>
</div><!-- .--featured -->

<?php endif; ?>