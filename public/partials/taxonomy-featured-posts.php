<?php

/**
 * Render featured posts for a taxonomy if any
 *
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/public/partials
 */
if ( ! $featured_posts_query->have_posts() ) {
	return;
}
?>

<div class="featured">
	<div class="row">
		<?php
		global $count;
		$count = 0;

		// conditional network query loop helper
		$helper = new OEP\Common\Network_Query_Helper( $featured_posts_query );

		while ( $featured_posts_query->have_posts() ) : $featured_posts_query->the_post();

			$count ++;

			// network: manually set $post global and switch to appropriate blog
			$helper->setup_postdata( $featured_posts_query );

			/**
			 * @todo we should probably move this template part to the plugin so
			 * that this doesn't break when the theme is not activated.
			 */
			get_template_part( 'template-parts/content-grid', get_post_type() );

			if ( $count == 4 && $featured_posts_query->post_count > 4 ) {
				echo '</div><div class="row">'; // break up the posts so we can change the layout of the last 3
			}

			// network: restore main request's blog and post global
			$helper->reset_postdata();

		endwhile;
		?>
	</div>
</div>
