<?php
/**
 * Stats Block
 *
 * @since   1.0.0
 * @package oep
 */

extract( OEP\Common\get_fields([
	'subtitle',
	'title',
	'body',
	'stats',
]));

$attrs = OEP\Common\oep_fn_attrs_class([
	'stats',
	'full-width',
	@$block['className'],
]);
?>

<section <?php echo $attrs; ?>>
	<div class="wrap">

		<div class="heading">
			<h3><?php echo $subtitle ?: __( 'Orlando', 'oep' ); ?></h3>
			<h2><?php echo $title ?: __( 'You don\'t know the half of it.', 'oep' ); ?></h2>
			<?php echo $body ? '<p>' . $body . '</p>' : ''; ?>
		</div>

		<ul class="stats-container">

			<?php
			if ( ! empty ( $stats ) ) : foreach( $stats as $stat ) :

				// This gets a template part from public/partials/file named
				// 'stat' that has the full HTML for a single stat, then gives
				// it a custom stat_id, and calls the foreach variable '$stat'
				// that was declared in the line above.
				OEP\Common\oep_fn_template_part( 'stat', '', [ 'stat_id' => $stat ] );

			endforeach; endif;
			?>

		</ul>

	</div>
</section>
