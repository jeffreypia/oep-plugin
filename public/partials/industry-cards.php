<?php
	$cards = get_field('cards');
?>

<div class="industry-cards full-width">
	<div class="wrapper-inner-blocks wrap">
		<?php
			// Iterate thru cards
			foreach( $cards as $card ) :
				OEP\Common\oep_fn_template_part( 'industry-card', '', [
					'card' 		=> $card,
					'extract_card'	=> true,
				] );
			endforeach;
		?>
	</div>
</div>