<?php

/**
 * Render author picks
 *
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/public/partials
 */
?>
<?php

$author_picks_query = $args['author_picks_query'];
if ( $author_picks_query->have_posts() ) : ?>

<div class="author-picks tiles wide in-content">

		<h4><?php _e("Author's Pick", "oep"); ?></h4>

			<?php
			/* Start the Loop */
			global $count;
			$count = 0;
			while ( $author_picks_query->have_posts() ) : $author_picks_query->the_post();
				$count ++;
				/**
				 * @TODO we should probably move this template part to the plugin so that this doesn't break when the theme is not activated.
				 */
				get_template_part( 'template-parts/content-authors-pick', get_post_type() );

			endwhile;
			?>

	
</div>
<?php
endif;
