<?php
/**
 * Cost of living form template
 *
 * @since   1.2.0
 * @package OEP
 * @author  Jordan Pakrosnis <jordan@designzillas.com>
 *
 * @param   string      $content     block inner HTML (empty).
 * @param   array       $block       block settings and attributes.
 * @param   bool        $is_preview  true during AJAX preview.
 * @param   int|string  $post_id     post ID this block is saved to.
 */

use \OEP\Common\Cost_Of_Living;

extract( Cost_Of_Living::get_form_settings() );
extract( Cost_Of_Living::get_form_values()   );
?>

<form class="cost-of-living" action="" method="get">
	<ul class="fields">

		<li class="field">
			<label for="col_income"><?php echo $label_income; ?>:</label>
			<div class="input-wrap">
				<span class="field-prefix"><?php _e( '$' ); ?></span>
				<input type="number" name="col_income" min="1" step="1" value="<?php echo $income ?: 80000; ?>" required>
			</div>
		</li>

		<li class="field">
			<label for="col_city_from"><?php echo $label_city_from; ?>:</label>
			<select name="col_city_from" required>
				<?php foreach ( Cost_Of_Living::get_places() as $place_id => $name ) :
					$name = $name == 'USA USA AVERAGE' ? __( 'USA AVERAGE' ) : $name; ?>

					<option value="<?php echo esc_attr( $place_id ); ?>" <?php selected( $place_id, $city_from ); ?>><?php echo $name; ?></option>

				<?php endforeach; ?>
			</select>
		</li>

		<li class="field">
			<label for="col_city_to"><?php echo $label_city_to; ?>:</label>
			<select name="col_city_to" required>

				<?php if ( ! $city_to && count( Cost_Of_Living::$city_to_ids ) > 1 ) : ?>
					<option value="" selected>&mdash; <?php _e( 'PLEASE SELECT', 'oep' ); ?> &mdash;</option>
				<?php endif; ?>

				<?php foreach ( Cost_Of_Living::get_places() as $place_id => $name ) :

					if ( $place_id === 1 || ! in_array( $place_id, Cost_Of_Living::$city_to_ids ) ) {
						continue;
					} ?>
					<option value="<?php echo esc_attr( $place_id ); ?>" <?php selected( $place_id, $city_to ); ?>><?php echo $name; ?></option>

				<?php endforeach; ?>
			</select>
		</li>

		<li class="field">
			<?php \OEP\Common\oep_fn_button([
				'tag'  => 'button',
				'type' => 'submit',
				'text' => $button_text,
				'icon' => 'check',
			]); ?>
		</li>

	</ul>

	<div class="loading">
		<img class="ajax-spinner" src="<?php echo OEP_DIR_URI .'/assets/images/ajax.svg'; ?>" alt="<?php _e( 'Loading Spinner' ); ?>">
	</div>

</form>
