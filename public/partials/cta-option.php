<?php
/**
 * CTA Option Block
 *
 * @since   1.0.0
 * @package lwp
 */
extract( OEP\Common\get_fields([ 'quantity', 'cta_type', 'cta_1', 'cta_2' ]));
// $quantity 1 == -single, 2 == -double
// $cta_type:
// 		"1" : Regular CTA
// 		"2" : Newsletter CTA
// 		"3" : Downloadable CTA

$attrs = OEP\Common\oep_fn_attrs_class([
	'cta-option',
	'full-width',
	"1" === $quantity ? '-single' : '',
	"2" === $quantity ? '-double' : '',
	@$block['className'],
]);
?>

<section <?php echo $attrs; ?>>
	<?php extract( $cta_1 ); ?>
	<div class="cta cta-1">
		<?php if( $link ) : ?>
		<a class="cta_link" href="<?php echo $link; ?>"></a>
		<?php endif; ?>

		<figure class="cta_thumbnail">
			<?php oep_cover_image( $background, 'large' ); ?>
		</figure>

		<div class="cta_content">
			<h3 class="cta_title"><?php echo $title; ?></h3>
			<div class="cta_description">
				<?php echo $description; ?>
			</div>
		</div><!-- .cta_content -->
	</div>

	<?php if ( '2' === $quantity ) :
		extract( $cta_2 );

		$attrs = \OEP\Common\oep_fn_attrs_class([
			'cta',
			'cta-2',
			"2" === $cta_type ? '-newsletter' : '',
			"3" === $cta_type ? '-asset' : '',
		]); ?>

		<div <?php echo $attrs; ?>>
			<?php if( $link ) : ?>
			<a class="cta_link" href="<?php echo $link; ?>"></a>
			<?php endif; ?>

			<figure class="cta_thumbnail">
				<?php oep_cover_image( $background, 'large' ); ?>
			</figure>

			<div class="cta_content">
				<h3 class="cta_title"><?php echo $title; ?></h3>
				<div class="cta_description">
					<?php echo $description; ?>
				</div>

				<?php
					if( "2" === $cta_type ) :
						OEP\Common\oep_fn_template_part( 'newsletter' );
					endif;

					if( "3" === $cta_type ) :
						oep_button([
							'text' 		=> $button_text ?: __( 'Download File', 'lwp' ),
							'url'  		=> $link,
							'new_tab'	=> 'new-tab',
							'class'		=> 'cta_button'
						]);
					endif;
				?>
			</div><!-- .cta_content -->

		</div>
	<?php endif; ?>
</section>
