<?php
    // Markup divided by region:
    //  "tile" is used by sidebar widgets
    //  "banner" is used by main banner
    //  "default" is used by posts
    switch( $args['region'] ) :
        case 'banner' :
?>
<span class="byline">
    <span class="author vcard --<?php echo $args['region']; ?> ?>">
        <figure class="author_thumbnail"><?php echo $args['thumbnail']; ?></figure>
        <?php _e('By') ?> <a class="author_name url fn n" href="<?php echo $args['link']; ?>"><?php echo $args['name']; ?></a>
        <span class="author_title"><?php echo $args['title']; ?></span>
    </span><!-- .vcard -->
</span><!-- .byline -->

<?php
        break;
        case 'tile' :
?>
<span class="byline">
    <span class="author vcard --<?php echo $args['region']; ?>"><?php _e('by', 'oep'); ?>
        <a class="author_name url fn n" href="<?php echo $args['link']; ?>"><?php echo $args['name']; ?></a>
    </span>
</span>

<?php
        break;
        case 'video' :
?>
<span class="byline">
	<span class="author vcard --video">
		<?php _e('By') ?> <a class="author_name url fn n" href="<?php echo $args['link']; ?>"><?php echo $args['name']; ?></a>
		<span class="author_title"><?php echo $args['title']; ?></span>
	</span><!-- .vcard -->
</span>

<?php
        break;
        case 'archive' :
?>
<div class="archive vcard --<?php echo $args['region']; ?>">
	<div class="author_content">
		<div class="byline">
			<figure class="author_thumbnail">
				<div class="profile-image">
					<?php echo $args['thumbnail']; ?>
				</div>
			</figure>
			<div class="name">
				<span class="author_name"><?php echo $args['name']; ?></span>
				<span class="author_title"><?php echo $args['title']; ?></span>
				<div class="social-links">

					<?php if (!empty($args['twitter'])) : ?>
						<a href="<?php echo $args['twitter']; ?>" class="author_twitter" target="_blank">
							<i class="fab fa-twitter"></i>
						</a>
					<?php endif; ?>

					<?php if (!empty($args['linkedin'])) : ?>
						<a href="<?php echo $args['linkedin']; ?>" class="author_linkedin" target="_blank">
							<i class="fab fa-linkedin-in"></i>
						</a>
					<?php endif; ?>

					<?php if (!empty($args['facebook'])) : ?>
						<a href="<?php echo $args['facebook']; ?>" class="author_facebook" target="_blank">
							<i class="fab fa-facebook-f"></i>
						</a>
					<?php endif; ?>

					<?php if (!empty($args['email'])) : ?>
						<a href="mailto:<?php echo $args['email']; ?>" class="author_email" target="_blank">
							<i class="fas fa-envelope"></i>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="author_bio">
			<?php echo $args['bio']; ?>
		</div>
    </div><!-- .author_content -->
 </div><!-- .author -->

<?php
        break;
        default :
?>
<div class="author vcard --<?php echo $args['region']; ?>">
    <header class="author_heading"><h4><?php _e('About the Author'); ?></h4></header>
    <div class="author_content">
        <span class="byline">
            <figure class="author_thumbnail"><?php echo $args['thumbnail']; ?></figure>
			<div class="author_details">
				<span class="vcard"><?php _e('By', 'oep'); ?>
	                <a class="author_name" href="<?php echo $args['link']; ?>"><?php echo $args['name']; ?></a>
	            </span>
	            <span class="author_title"><?php echo $args['title']; ?></span>
			</div>
        </span>
        <div class="author_bio">
            <?php echo $args['bio']; ?>
        </div>
        <a href="<?php echo $args['link']; ?>" class="author_link"><?php echo __('More articles by ', 'oep') . ' ' . $args['name']; ?> <span class="author_icon"><i class="fal fa-arrow-right"></i></span></a>
    </div><!-- .author_content -->
 </div><!-- .author -->

 <?php endswitch ?>
