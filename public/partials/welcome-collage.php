<?php
/**
 * "Welcome Collage" block with copy and images on sides
 *
 * @since   1.2.0
 * @package oep
 */

extract( OEP\Common\get_fields([
	'title',
	'subtitle',
	'body',
	'link',
	'image_1',
	'image_2',
	'image_3',
	'image_4',
]));

$attrs = OEP\Common\oep_fn_attrs_class([
	'welcome-collage',
	'full-width',
	@$block['className'],
]);
?>

<section <?php echo $attrs; ?>><div>

	<ul class="collage">

		<?php if ( $image_1 ) : // top left ?>
			<li class="collage-image collage-image-1">
				<div class="angled-clip angled-clip-down">
					<?php oep_cover_image( $image_1, 'full' ); ?>
				</div>
			</li>
		<?php endif; ?>

		<?php if ( $image_2 ) : // bottom left ?>
			<li class="collage-image collage-image-2">
				<div class="angled-clip angled-clip-down-small">
					<?php oep_cover_image( $image_2, 'large' ); ?>
				</div>
				<?php oep_svg( 'triangle-dots-TL' ); ?>
			</li>
		<?php endif; ?>

		<?php if ( $image_3 ) : // top right ?>
			<li class="collage-image collage-image-3">
				<div class="angled-clip angled-clip-down">
					<?php oep_cover_image( $image_3, 'full' ); ?>
				</div>
			</li>
		<?php endif; ?>

		<?php if ( $image_4 ) : // bottom right ?>
			<li class="collage-image collage-image-4">
				<div class="angled-clip angled-clip-up-small">
					<?php oep_cover_image( $image_4, 'large' ); ?>
				</div>
			</li>
		<?php endif; ?>

	</ul>

	<?php if ( $title || $subtitle || $body || $link['href'] ) : ?>
		<div class="wp-block-oep-general-text-header -light">

			<?php if ( $title || $subtitle ) : ?>
				<header>
					<?php echo $subtitle ? "<h4>$subtitle</h4>" : ''; ?>
					<?php echo $title ? "<h2><strong>$title</strong></h2>" : ''; ?>
				</header>
			<?php endif; ?>

			<?php echo $body; ?>

			<?php \OEP\Common\oep_fn_button([
				'link'   => $link,
				'icon'   => 'arrow-right',
				'icon_r' => true,
				'class'  => [ 'text-only', 'text-only-black' ],
			]); ?>

		</div>
	<?php endif; ?>

</div></section>
