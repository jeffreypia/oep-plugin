<?php
/**
 * Success Story Block
 *
 * @since   1.0.0
 * @package oep
 */

extract( OEP\Common\get_fields([
	'success_story',
]));
?>

<section class="success-story full-width">
	<div class="wrap">
		<?php if ( !empty ( $success_story ) ) : ?>
			<?php foreach( $success_story as $story ) : ?>
				<?php // This gets a template part from public/partials/file named 'success-story' that has the full HTML for a single story, then gives it a custom story_id, and calls the foreach variable '$story' that was declared in the line above. ?>
				<?php OEP\Common\oep_fn_template_part( 'success-single', '', [ 'story_id' => $story ] ) ; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</section>
