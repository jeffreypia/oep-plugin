<?php

/**
 * Render sliders
 *
 * @var $sliders_query WP_Query from render function
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/public/partials
 */
?>
<?php

$sliders_query = $args['sliders_query'];
if ( $sliders_query->have_posts() ) : ?>

    <header class="banner slider">

		<?php
		/* Start the Loop */
		global $count;
		$count = 0;
		while ( $sliders_query->have_posts() ) : $sliders_query->the_post();
			$count ++;
			/**
			 * @TODO we should probably move this template part to the plugin so that this doesn't break when the theme is not activated.
			 */
			get_template_part( 'template-parts/content-slider', get_post_type() );

		endwhile;
		?>

        <ul class="nav-slider" role="presentation" aria-hidden="true" data-flickity='{ "asNavFor": ".slider", "contain": true, "pageDots": false,"prevNextButtons": false, "cellSelector": ".nav_item", "cellAlign": "center" }'>
			<?php
			$count = 0;
			while ( $sliders_query->have_posts() ) : $sliders_query->the_post();
                $terms = wp_get_object_terms( get_the_ID(), [
                    'oep_taxonomies_industries',
                    'oep_taxonomies_qol',
                    'oep_taxonomies_competitiveness',
                    'oep_taxonomies_teams',
                    'oep_taxonomies_activity'
                ] );
				$count ++;
				?>
                <li class="nav_item">
                    <h1><?php echo $count; ?></h1>
                    <div>
                        <p><?php the_title(); ?></p>
                        
                        <?php if( $terms ) : ?>
                        <span><?php echo $terms[0]->name; ?></span>
                        <?php endif; ?>
                    </div>
                    <a class="post-link" href="<?php echo get_the_permalink(); ?>"></a>
                </li>
			<?php
			endwhile;
			?>
        </ul>

    </header>
<?php
endif;
