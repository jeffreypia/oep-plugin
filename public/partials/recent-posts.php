<?php

/**
 * Render recent posts
 *
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/public/partials
 */
?>
<?php
$post_type = $args['attributes']['post_type'];
$post_type_object = get_post_type_object( $post_type );
$post_type_label = $post_type_object->label;
$style = $args['attributes']['style'];
$title = $args['attributes']['title'];
$link = $post_type == 'post' ? get_permalink( get_option( 'page_for_posts' ) ) : get_post_type_archive_link( $post_type );
$recent_posts_query = $args['recent_posts_query'];
if ( $recent_posts_query->have_posts() ) : ?>

<div class="recent-posts tiles wide with-footer <?php echo $post_type." ".$style; ?>">
		<h2><?php ($style === "vertical") ? oep_svg( 'heading-accent' ) : null; ?><?php echo $title; ?></h2>
        <a class="more-link desktop" href="<?php echo $link; ?>"><?php _e("View More", "oep");?></a>

			<?php
			/* Start the Loop */
			global $count;
			$count = 0;
			if( $recent_posts_query->have_posts() ) :

				echo $post_type === OEP_EVENT_KEY ? '<div class="recent-posts_wrapper">' : '';
				while ( $recent_posts_query->have_posts() ) :
					$recent_posts_query->the_post();
					$count ++;
					/**
					 * @TODO we should probably move this template part to the plugin so that this doesn't break when the theme is not activated.
					 */
					get_template_part( 'template-parts/content-recent-posts', get_post_type() );
				endwhile;

				echo $post_type === OEP_EVENT_KEY ? '</div>' : '';
			endif;
			?>

		<a class="more-link mobile" href="<?php echo $link; ?>"><?php _e("View More Upcoming " . $post_type_label, "oep");?></a>

</div>
<?php
endif;
