<?php

/**
 * Render recent posts
 *
 * @var $stories_query \WP_Query
 *
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/public/partials
 */
?>
<?php
global $style;
$taxonomy       = $args['attributes']['taxonomy'];
$selected_terms = $args['attributes']['terms'];
$style          = "has-" . $args['attributes']['num_posts'] . "-posts";
$title          = $args['attributes']['title'];
$link           = home_url( get_taxonomy( $taxonomy )->rewrite['slug'] );
$stories_query  = $args['stories_query'];
if ( $stories_query->have_posts() ) : ?>

    <div class="recent-posts has-stories tiles wide with-footer <?php echo $taxonomy . " " . $style; ?>">
        <h2>
            <div class="accent" role="presentation" style="color: <?php \OEP\Taxonomies\Taxonomy::get_color($taxonomy); ?>"
                 data-color="<?php \OEP\Taxonomies\Taxonomy::get_color($taxonomy); ?>">
		        <?php is_front_page() ? oep_svg( 'heading-accent' ) : null; ?>
            </div>
            <?php echo $title; ?></h2>
        <a class="more-link" href="<?php echo $link; ?>"><?php _e( "View More", "oep" ); ?></a>
        <div class="stories">

			<?php
			/* Start the Loop */
			global $count;
			$count = 0;
			while ( $stories_query->have_posts() ) : $stories_query->the_post();
				$count ++;
				/**
				 * @TODO we should probably move this template part to the plugin so that this doesn't break when the theme is not activated.
				 */
				get_template_part( 'template-parts/content-stories', get_post_type() );

			endwhile;
			?>
        </div>
		<?php if ( $style === "has-3-posts" ): ?>
            <ul class="term-list">
				<?php foreach ( $selected_terms as $selected_term ):
					$term = get_term( $selected_term );
                    if( !is_null($term) ) :
					?>
                    <li class="term"><?php
	                    echo get_field('icon', $term) ?: "";
                        echo '<a href="'.get_term_link( $term ).'">'.$term->name.'</a>';
                        echo '<span class="arrow-right"><i class="fa fal fa-angle-right"></i></span>';
                        ?>
                    </li>
				<?php endif; endforeach; ?>
            </ul>
		<?php endif ?>

    </div>
<?php
endif;
