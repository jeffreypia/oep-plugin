<?php

/**
 * Render recent events
 *
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/public/partials
 */
?>

<?php
// need query to pull 3 upcoming events
$upcomingEvents = OEP\Common\oep_get_upcoming_events( 3, $post->ID );

if( $upcomingEvents ) :
?>

<div class="widget posts --upcoming-events recent-events">
	<div class="wrap">
		<div class="headline">
			<h2 class="widget-title"><?php _e('Other Upcoming Events'); ?></h2>
			<a class="more-link desktop" href="<?php echo get_post_type_archive_link( OEP_EVENT_KEY ); ?>"><?php _e('View All Upcoming Events'); ?></a>
		</div>

		<ul class="posts">
			<?php
				foreach( $upcomingEvents['posts'] as $post ) :
					$date = new DateTime($post->start_date)
			?>
			<li class="post event">
				<a class="post_link" href="<?php echo get_the_permalink(); ?>"></a>

				<figure class="post_thumbnail">
					<div class="post_thumbnail-wrap">
						<?php the_post_thumbnail('thumbnail'); ?>
					</div>

					<figcaption>

						<div class="event_date">
							<strong><?php echo date_format( $date, 'd' ); ?></strong>
							<?php echo date_format( $date, 'M' ); ?>
						</div>

						<div class="title-cta">
							<h3 class="post_title"><?php echo $post->post_title; ?></h3>
							<span class="event_cta"><?php _e('View Event Details') ?></span>
						</div>
					</figcaption>
				</figure>

			</li>
			<?php endforeach; ?>
		</ul>

		<a class="more-link mobile" href="<?php echo get_post_type_archive_link( OEP_EVENT_KEY ); ?>"><?php _e('View All Upcoming Events'); ?></a>
	</div>

</div><!-- .--featured -->

<?php endif; ?>
