<?php

/**
 * Template for related posts block
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP/CPT
 * @subpackage OEP/CPT/partials
 */

    $relatedPosts = OEP\Common\oep_get_related_posts($post->ID);

    if( $relatedPosts ) :
?>

<div class="widget posts --related">
    <h2 class="widget-title"><?php _e('Related Stories'); ?></h2>

    <ul class="posts">
        <?php
            foreach( $relatedPosts as $post ) :
                $post_type = OEP\Common\oep_get_post_type($post->ID);
        ?>
        <li class="post">
            <a class="post_link" href="<?php echo get_the_permalink(); ?>"></a>

            <figure class="post_thumbnail">
	        <?php
	        if(in_array( get_post_type($post), [ OEP_VIDEO_KEY ])) {
		        do_action('media_thumbnail', $post);
	        } else {
	            ?>
                <div class="post_thumbnail-wrap">
                    <?php the_post_thumbnail('thumbnail'); ?>
                </div>
                <?php
	        }
	        ?>



                <figcaption>
                    <h4 class="post_title">
                        <?php echo $post->post_title; ?>
                        <?php echo $post_type; ?>
                    </h4>

                </figcaption>

                <?php oep_get_pills(); ?>
            </figure>

            <div class="post_meta">
                <?php oep_posted_by('tile'); ?> <strong><?php echo date_format( new DateTime($post->post_date), 'M j.Y' ); ?></strong>
            </div>
        </li>
        <?php endforeach; ?>
    </ul>
</div><!-- .--related -->

<?php endif; ?>