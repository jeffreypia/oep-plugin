<?php

/**
 * Render recommended stories
 *
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/public/partials
 */
?>
<?php

$recommended_posts_query = $args['recommended_posts_query'];
if ( $recommended_posts_query->have_posts() ) : ?>

<div class="recommended-stories tiles wide with-footer">


		<h2><?php _e("Recommended Stories", "oep"); ?></h2>

			<?php
			/* Start the Loop */
			global $count;
			$count = 0;
			while ( $recommended_posts_query->have_posts() ) : $recommended_posts_query->the_post();
				$count ++;
				/**
				 * @TODO we should probably move this template part to the plugin so that this doesn't break when the theme is not activated.
				 */
				get_template_part( 'template-parts/content-recommended-stories', get_post_type() );

			endwhile;
			?>


	
</div>
<?php
endif;
