<?php
/**
 * Render contact block
 *
 * @var $attributes array of settings from the contact block
 * @var $content actual block content
 *
 * @since   1.0.0
 * @package oep
 * @subpackage OEP/public/partials
 */

extract( $args );
?>

<div class="contact-form">
    <?php echo $content ;?>

	<div class="hubspot-form" data-hs-portal-id="270308" data-hs-form-id="584faf31-cda6-46e2-950e-e4d7a0c66fa9"></div>
</div>