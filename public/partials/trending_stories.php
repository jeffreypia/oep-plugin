<?php

/**
 * Template for trending stories block
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP/CPT
 * @subpackage OEP/CPT/partials
 */

    $trendingPosts = OEP\Common\oep_get_trending_posts();

    if( $trendingPosts ) :
        $title = get_field('section_title');
?>

<div class="widget posts full-width --trending">
    <h2 class="widget-title"><?php oep_svg( 'accent' ); ?> <?php echo $title ? $title : __('Trending Stories'); ?></h2>

    <ul class="posts">
        <?php
            foreach( $trendingPosts['posts'] as $post ) :
        ?>
        <li class="post">
            <a class="post_link" href="<?php echo get_the_permalink(); ?>"></a>

            <figure class="post_thumbnail">
                <div class="post_thumbnail-wrap">
                    <?php echo $post->thumbnail; ?>
                </div>

                <figcaption>
                    <h3 class="post_title"><a href="<?php echo get_the_permalink(); ?>"> <?php echo $post->post_title; ?></a></h3>
                    <footer class="entry-footer">
                        <span class="byline">
                            <span class="author vcard --tile"><?php _e('by', 'oep'); ?>
                                <a class="author_name url fn n" href="<?php echo $post->author_link; ?>"><?php echo $post->author_name; ?></a>
                            </span>
                        </span>

                        <?php echo $post->posted_on; ?>
                    </footer>
                </figcaption>
            </figure>
        </li>
        <?php endforeach; ?>
    </ul>
</div><!-- .--featured -->

<?php endif; ?>