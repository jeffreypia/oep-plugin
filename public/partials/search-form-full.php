<?php
/**
 * Full search form
 *
 * @since   1.0.0
 * @package oep
 */
?>

<div class="full-search wrap">

	<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ) ; ?>">

		<ul class="fields">

			<!-- keyword -->
			<li class="field search">
				<label for="s"><?php _e( 'Keyword', 'oep' ); ?></label>
				<input <?php echo \OEP\Common\oep_fn_attrs([
					'type'  => 'search',
					'class' => 'search-field',
					'value' => get_search_query(),
					'name'  => 's',
				]); ?> />
			</li>

            <!-- types -->
            <li class="field types">
                <label for="post_type>"><?php _e( 'Type', 'oep' ); ?></label>
				<?php \OEP\Common\oep_post_type_select( get_query_var('post_type') ); ?>
            </li>


			<!-- categories -->
			<li class="field category">
				<label for="<?php echo OEP_QOL_KEY;?>"><?php _e( 'Filter by', 'oep' ); ?></label>
				<?php wp_dropdown_categories([
				        'show_option_none' 	=> __( 'All', 'all' ) ,
				        'option_none_value'  => '',
				        'name' 				=> OEP_QOL_KEY ,
				        'taxonomy' 			=> OEP_QOL_KEY ,
				        'value_field'       => 'slug',
				        'selected'			=> get_query_var('oep_taxonomies_qol')
                ]); ?>
			</li>


		</ul>

		<div class="buttons">

			<?php \OEP\Common\oep_fn_button([
				'tag'   => 'button',
				'type'  => 'submit',
				'text'  => _x( 'Search', 'submit button' ),
				'icon'  => 'search',
			]); ?>
			<?php \OEP\Common\oep_fn_button([
				'text'  => __( 'Clear Filters', 'oep' ),
				'url'   => add_query_arg([
					's'                 	=> get_search_query(),
					'post_type'				=> '',
					'oep_taxonomies_qol'	=> ''
				], home_url( '/' ) ),
				'icon'  => 'times-circle',
				'class' => [ 'light', 'ghost', 'reset', 'text', 'offset-padding' ],
			]); ?>

		</div>
	</form>
</div>
