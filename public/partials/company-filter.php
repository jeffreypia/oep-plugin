<?php
/**
 * Company Filter Form
 *
 * @since   1.1.0
 * @package oep
 */
?>

	<div class="field filters">
		<form method="post" action="<?php echo esc_url( home_url( '/' ) ) ; ?>">

			<ul class="fields">

	            <!-- industries -->
	            <li class="field industry">
	                <label for="<?php echo OEP_INDUSTRIES_KEY;?>"><?php _e( 'Industry', 'oep' ); ?></label>
					<?php wp_dropdown_categories([
						'show_option_none' 	=> __( 'All', 'all' ) ,
						'option_none_value'  => '',
						'name' 				=> OEP_INDUSTRIES_KEY ,
						'taxonomy' 			=> OEP_INDUSTRIES_KEY ,
						'value_field'       => 'slug',
						'selected'			=> get_query_var('oep_taxonomies_industries')
					]); ?>
	            </li>

	            <!-- categories -->
	            <li class="field size">
	                <label for="<?php echo OEP_SIZE_KEY;?>"><?php _e( 'Company Size', 'oep' ); ?></label>
					<?php wp_dropdown_categories([
						'show_option_none' 	=> __( 'All', 'all' ) ,
						'option_none_value'  => '',
						'name' 				=> OEP_SIZE_KEY ,
						'taxonomy' 			=> OEP_SIZE_KEY ,
						'value_field'       => 'slug',
						'selected'			=> get_query_var('oep_taxonomies_size')
					]); ?>
	            </li>
				<!-- categories -->

			</ul>

			<div class="buttons">
				<?php \OEP\Common\oep_fn_button([
					'tag'   => 'button',
					'type'  => 'submit',
					'text'  => _x( 'Filter', 'submit button' ),
					'icon'  => 'search',
				]); ?>

				<?php \OEP\Common\oep_fn_button([
					'text'  => __( 'Clear Filters', 'oep' ),
					'url'   => get_post_type_archive_link( OEP_COMPANY_KEY ),
					'icon'  => 'times-circle',
					'class' => [ 'light', 'ghost', 'reset', 'text', 'offset-padding' ],
				]); ?>

			</div>
	        <input type="hidden" class="types" name="post_type" value="<?php echo OEP_COMPANY_KEY; ?>" />
		</form>
	</div>