<?php

/**
 * Render recent posts
 *
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/public/partials
 */
?>
<?php
$title = $args['attributes']['title'];
$recent_posts_query = $args['recent_posts_query'];
//$big   = 999999999; // need an unlikely integer
//$base = str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) );
//$base = "https://oep.test:7890/news/page/%#%/";
//d($base);

if ( have_posts() ) : global $count; ?>

<div class="results">

    <h2><?php oep_svg( 'heading-accent' ); ?><?php echo $title; ?></h2>
    <div class="field types">
        <label for="post_type"><?php _e( 'Filter by', 'oep' ); ?></label>
		<?php \OEP\Common\oep_post_type_select(); ?>
    </div>
	<?php do_action('taxonomy_filter_form'); //@TODO to be filled in later ?>

			<?php
			/* Start the Loop */
	        $count = 0;
			while ( have_posts() ) : the_post();
				/**
				 * @TODO we should probably move this template part to the plugin so that this doesn't break when the theme is not activated.
				 */
				get_template_part( 'template-parts/content-grid', get_post_type() );

			endwhile;

	oep_numbered_pagination();

else :

	get_template_part( 'template-parts/content', 'none' );

endif; ?>

	
</div>
<?php

