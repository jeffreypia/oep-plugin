<?php
/**
 * Social Cards Block
 *
 * @since   1.0.0
 * @package oep
 */

extract( OEP\Common\get_fields([
	'title',
	'subtitle',
	'social_cards',
]));

$attrs = OEP\Common\oep_fn_attrs_class([
	'social-cards',
	'full-width',
	@$block['className'],
]);
?>

<section <?php echo $attrs; ?>>

	<div class="triangle">
		<img src="<?php echo( get_template_directory_uri() . '/assets/images/triangle-dots-outline-TR.svg'); ?>" alt="decorative dots"/>
	</div>

	<div class="heading">
		<h1><?php echo $title ?: __("#ThisIsOrlando"); ?></h1>
		<h2><?php echo $subtitle ?: __( "Become part of Orlando's Story", 'oep' ); ?></h2>
	</div>

	<div class="social-card-container">

		<?php if ( !empty ( $social_cards ) ) : ?>

			<?php foreach( $social_cards as $card ) : ?>

				<a class="social-card" href="<?php echo $card['link'] ?: '#'; ?>" target="_blank">

					<div class="card-text">
						<?php echo $card['quote'] ? '<h3>"' . $card['quote'] . '"</h3>' : ''; ?>
						<div class="social-handle">
							<i class="fab fa-<?php echo esc_attr( $card['social_platform'] ) ?: 'instagram'; ?>"></i>
							<?php echo $card['social_handle'] ? '<p class="profile">@' . $card['social_handle'] . '</p>' : ''; ?>
						</div>
					</div>

					<?php oep_cover_image($card['image']); ?>
				</a>
			<?php endforeach; ?>

		<?php endif; ?>

	</div>

</section>
