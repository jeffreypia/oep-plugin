<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit348b5051f7e46d811ab9d4cd892577cd
{
    public static $files = array (
        '16eed290c5592c18dc3f16802ad3d0e4' => __DIR__ . '/..' . '/ivopetkov/html5-dom-document-php/autoload.php',
    );

    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Masterminds\\' => 12,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Masterminds\\' => 
        array (
            0 => __DIR__ . '/..' . '/masterminds/html5/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit348b5051f7e46d811ab9d4cd892577cd::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit348b5051f7e46d811ab9d4cd892577cd::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
