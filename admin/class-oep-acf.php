<?php

/**
 * The ACF specific functionality of the plugin.
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/admin
 */

/**
 * Plugin-specific ACF handling
 *
 * @package    OEP
 * @subpackage OEP/admin
 * @author     Ryan Smith <ryan@designzillas.com>
 */

namespace OEP\Common;

// If this file is called directly, or if ACF isn't loaded, abort.
if ( ! defined( 'ABSPATH' ) ) {
	wp_die( "Yeah, that's not gonna work here either" );
}

class ACF {

	/**
	 * Local JSON path
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	protected static $json_path = OEP_PLUGIN_DIR . 'acf-json';
	/**
	 * "Name" of new field group that can be moved
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	protected static $group_name_key = 'oep_group_name';
	/**
	 * Field group post ID key
	 *
	 * @var   integer
	 * @since 1.0.0
	 */
	protected static $group_id_key = 'oep_group_id';
	/**
	 * Move field group notice flag key
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	protected static $do_notice_key = 'oep_move_notice';
	/**
	 * Flag for actually moving the field group
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	protected static $do_move_key = 'oep_move_field';

	/**
	 * Post object field link field setting key
	 *
	 * @var   string
	 * @since 1.2.0
	 */
	private static $setting_post_object_link = 'oep_post_object_link';

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $oep The ID of this plugin.
	 */
	protected $oep;
	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	protected $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 *
	 * @param      string $oep     The name of this plugin.
	 * @param      string $version The version of this plugin.
	 *
	 * @TODO     move these hooks to a Loader call instead of initializing in the constructor
	 */
	public function __construct( $oep, $version ) {

		$this->oep     = $oep;
		$this->version = $version;
		add_action( 'acf/settings/save_json', [ __CLASS__, 'save_json' ] );
		add_action( 'acf/settings/load_json', [ __CLASS__, 'load_json' ] );
		add_action( 'save_post', [ __CLASS__, 'maybe_set_move_notice' ], 10, 3 );
		add_action( 'admin_notices', [ __CLASS__, 'maybe_add_move_notice' ] );
		add_action( 'admin_notices', [ __CLASS__, 'maybe_move_group' ] );
		add_action( 'acf/render_field/type=post_object'          , [ __CLASS__ , 'do_post_object_field_link' ] );
		add_action( 'acf/render_field_settings/type=post_object' , [ __CLASS__ , 'do_post_link_field'        ] );
	}

	/**
	 * Safe field group to plugin if field group already exists here
	 *
	 * @param  string $path existing save path
	 *
	 * @return string  $path
	 *
	 * @since  1.0.0
	 */
	public static function save_json( $path ) {

		global $post;

		// don't run if no post (like when duplicating)
		if ( empty( $post ) ) {
			return $path;
		}

		// json file name is the field group's post name
		$file_name = $post->post_name . '.json';

		// if the file already exists in the plugin, re-save it here
		if ( file_exists( self::$json_path . "/$file_name" ) ) {
			return self::$json_path;
		}

		return $path;
	}


	/**
	 * Load in local ACF fields
	 *
	 * @param  array $paths existing ACF load paths
	 *
	 * @return array
	 *
	 * @since  1.0.0
	 */
	public static function load_json( $paths ) {

		$paths[] = self::$json_path;

		return $paths;
	}

	/**
	 * Maybe register an admin notice for moving a newly created field to plugin
	 *
	 * @param string  $post_id saved ACF post
	 * @param object  $post    saved ACF post
	 * @param boolean $update
	 *
	 * @since 1.0.0
	 */
	public static function maybe_set_move_notice( $post_id, $post, $update ) {

		// only for field groups and if this is the initialization of the post
		if (
			$post->post_type !== 'acf-field-group' ||
			$post->post_modified_gmt !== $post->post_date_gmt
		) {
			return;
		}

		// add the notice
		/*
		 * @TODO move this to class loader add_filter function
		 */
		add_filter( 'redirect_post_location', [ __CLASS__, 'add_move_notice_vars' ], 99, 2 );
	}


	/**
	 * Add the field group move notice vars
	 *
	 * @since 1.0.0
	 *
	 * @param $location
	 * @param $post_id
	 *
	 * @return string
	 */
	public static function add_move_notice_vars( $location, $post_id ) {

		remove_filter( 'redirect_post_location', [ __CLASS__, __FUNCTION__ ], 99 );

		$group_post = get_post( $post_id );

		return add_query_arg( [
			self::$do_notice_key  => true,
			self::$group_name_key => $group_post->post_name,
		], $location );
	}

	/**
	 * Maybe add the actual field group move admin notice
	 *
	 * @since 1.0.0
	 */
	public static function maybe_add_move_notice() {

		// check for flag and group
		$do_notice = oep_get( self::$do_notice_key );
		$group     = oep_get( self::$group_name_key );

		if ( oep_fn_any_empty( $do_notice, $group ) ) {
			return;
		}

		$group = acf_get_field_group( $group );
		if ( empty( $group ) ) {
			return;
		}

		$title    = $group['title'];
		$url      = add_query_arg( 'post_type', 'acf-field-group', admin_url( 'edit.php' ) );
		$move_url = add_query_arg( [
			self::$group_name_key => $group['key'],
			self::$group_id_key   => $group['ID'],
			self::$do_move_key    => true,
		], $url );

		?>
        <div class="notice notice-warning">

            <h3><?php _e( 'HI ZILLA', 'oep' ); ?></h3>

            <p><?php printf(
					__( 'It looks like you\'ve added a new ACF field group. If
					"%s" is for a %splugin-related%s entity or feature,
					click the button to move the field\'s JSON to the plugin\'s
					local %sacf-json%s folder for better organization.', 'oep' ),
					$title, '<b>', '</b>', '<code>', '</code>'
				); ?></p>

            <a href="<?php echo $url; ?>" class="button">
				<?php _e( 'Nah, it\'s theme related', 'oep' ); ?>
            </a>&nbsp;

            <a href="<?php echo $move_url; ?>" class="button button-primary">
				<?php printf( __( 'Move "%s" To Plugin', 'oep' ), $title ) ?>
            </a>

            <br><br>
        </div>

		<?php
	}

	/**
	 * Maybe move a field group from the theme to the plugin
	 *
	 * @since 1.0.0
	 */
	public static function maybe_move_group() {

		$do_move    = oep_get( self::$do_move_key );
		$group_name = oep_get( self::$group_name_key );
		$group_id   = oep_get( self::$group_id_key );

		if ( oep_fn_any_empty( $do_move, $group_name, $group_id ) ) {
			return;
		}

		$title        = get_the_title( $group_id );
		$local_theme  = get_stylesheet_directory() . "/acf-json/$group_name.json";
		$local_plugin = self::$json_path . "/$group_name.json";

		// if it's in the theme, we're good to go
		if ( file_exists( $local_theme ) ) {

			$moved   = rename( $local_theme, $local_plugin );
			$type    = $moved ? 'success' : 'error';
			$message = $moved
				? __( 'The "%s" field group was successfully moved to the plugin and is ready to commit.', 'oep' )
				: __( 'Although the "%s" field group was found in your theme, there was an error moving it to the plugin.', 'oep' );

		} elseif ( file_exists( $local_plugin ) ) {

			$type    = 'error';
			$message = __( 'The "%s" field group is already in the plugin.', 'oep' );

		} else {

			$type    = 'error';
			$message = __( 'The field group could not be found.', 'oep' );

		}

		printf(
			'<div class="notice notice-%s"><p>%s</p></div>',
			$type, sprintf( $message, $title )
		);
	}


	/**
	 * Output link to post selected in post object field
	 *
	 * Only if it's turned on for the field
	 *
	 * @param array  $field  ACF field
	 *
	 * @see   self::do_post_link_field()
	 * @since 1.2.0
	 */
	public static function do_post_object_field_link( $field ) {

		// check value and setting
		if ( ( ! $post_id = $field['value'] ) || ( ! @$field[ self::$setting_post_object_link ] ) ) {
			return;
		}

		// get post type name
		$post_type = get_post_type_object( get_post_type( $post_id ) );
		$post_type = get_post_type_labels( $post_type )->singular_name;
		?>
		<p>
			<a href="<?php echo get_permalink( $post_id ); ?>" target="_blank"><?php printf( __( 'View %s', 'oep' ), $post_type ); ?></a>
		</p>
		<?php
	}


	/**
	 * Add a field to enable a link to the post selected in a "Post Object"
	 *
	 * @since 1.2.0
	 */
	public static function do_post_link_field( $field ) {

		acf_render_field_setting( $field, [
			'label' => __( 'Link to post in field', 'oep' ),
			'name'  => self::$setting_post_object_link,
			'type'  => 'true_false',
		]);
	}
}
