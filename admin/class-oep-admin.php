<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    OEP
 * @subpackage OEP/admin
 * @author     Ryan Smith <ryan@designzillas.com>
 */



namespace OEP\Common;
use OEP\CPT\PostType;

require "vendor/autoload.php";

use IvoPetkov\HTML5DOMDocument;

class Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $oep The ID of this plugin.
	 */
	protected $oep;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	protected $version;

	/**
	 * ACF customizations class instance.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string acf The current version of this plugin.
	 */
	protected $acf;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 *
	 * @param      string $oep     The name of this plugin.
	 * @param      string $version The version of this plugin.
	 */
	public function __construct( $oep, $version ) {

		$this->oep     = $oep;
		$this->version = $version;
		$this->load_acf(); //load ACF customizations

	}

	/**
	 * Load ACF customizations for moving groups to the plugin folder
	 */
	public function load_acf() {
		require_once 'class-oep-acf.php';
		$this->acf = new ACF( $this->oep, $this->version );

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in OEP_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The OEP_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->oep, plugin_dir_url( __FILE__ ) . 'css/oep-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 * @param $hook_suffix where we at
	 * @since    0.1.0
	 */
	public function enqueue_scripts($hook_suffix) {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in OEP_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The OEP_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->oep, plugin_dir_url( __FILE__ ) . 'js/oep-admin.js', array(
			'jquery',
			'wp-edit-post',
		), $this->version, false );

		/**
		 * gutenberg customizations for landing pages and posts
		 */

		$cpts = [OEP_LANDING_PAGE_KEY, OEP_HOOD_KEY];

		if( in_array($hook_suffix, array('post.php', 'post-new.php') ) ){
			$screen = get_current_screen();
			if( is_object( $screen ) && in_array($screen->post_type, $cpts) ){

				// Register, enqueue scripts and styles here
				wp_enqueue_script( 'oep-editor-customizations', plugin_dir_url( __FILE__ ) . 'js/oep-editor-customizations-'. $screen->post_type .'.js', array(
					'jquery'
				), $this->version, false );

			} elseif( is_object( $screen ) ) {
				wp_enqueue_script( 'oep-editor-customizations', plugin_dir_url( __FILE__ ) . 'js/oep-editor-customizations.js', array(
					'jquery'
				), $this->version, false );
			}
		}


	}

	/**
	 * @param $categories
	 * @param $post
	 *
	 * @return array
	 */
	public function oep_block_categories( $categories, $post ) {
		//add oep common blocks category

		$categories = array_merge( array(
				array(
					'slug' => 'oep-common',
					'title' => __( 'OEP Common Blocks', 'oep' ),
					'icon'  => 'wordpress',
				),
				array(
					'slug' => 'oep-landing-page',
					'title' => __( 'OEP Landing Pages', 'oep' ),
					'icon'  => 'wordpress',
				),
				array(
					'slug'  => 'oep-col',
					'title' => __( 'OEP Cost of Living', 'oep' ),
					'icon'  => 'wordpress',
				),
		), $categories);

		/*if ( $post->post_type == OEP_LANDING_PAGE_KEY ) {
			return array_merge(
				array(
					array(
						'slug' => 'oep-landing-page',
						'title' => __( 'OEP Landing Pages', 'oep' ),
						'icon'  => 'wordpress',
					),
				),
				$categories
			);
		}*/

		return $categories;
	}

	public function register_blocks() {

		if ( ! function_exists( 'register_block_type' ) ) {
			// Gutenberg is not active.
			return;
		}

		// Admin styles for blocks in parent theme
		wp_register_style(
			'oep-block-editor',
			get_template_directory_uri() . '/assets/css/blocks-editor.css',
			array( 'wp-edit-blocks' ),
			null
		);

		// Admin styles for blocks in child theme
		wp_register_style(
			'oep-child-block-editor',
			get_stylesheet_directory_uri() . '/assets/css/blocks-editor.css',
			array( 'wp-edit-blocks' ),
			null
		);

		/*wp_register_style(
			'oep-block',
			get_template_directory_uri() . '/assets/css/blocks-styles.css',
			array( )
		);*/

		/**
		 * Register block editor helper functions/components
		 */
		wp_register_script(
			'oep-editor',
			plugin_dir_url( __FILE__ ) . 'js/oep-editor.js',
			array( 'wp-blocks', 'wp-editor', 'wp-data', 'wp-i18n', 'wp-element', 'wp-components', 'underscore' )
		);

		/**
		 * Hooked: OEP Editor JavaScript object
		 *
		 * OEP\Common\add_network_sites - 10
		 */
		wp_localize_script( 'oep-editor', 'oepEditor', apply_filters( 'oep_editor_object', [] ) );

		wp_register_script(
			'oep-excerpt',
			plugin_dir_url( __FILE__ ) . 'js/blocks/excerpt.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore' )
		);

		wp_register_script(
			'oep-hero-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/hero.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore' )
		);

		wp_register_script(
			'oep-hero-cta-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/hero-cta.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore' , 'wp-components' )
		);

		wp_register_script(
			'oep-video-text-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/video-text.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore', 'wp-components' )
		);

		wp_register_script(
			'oep-event-text-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/event-text.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore', 'wp-components' )
		);

		wp_register_script(
			'oep-media-text-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/media-text.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore', 'wp-components' )
		);

		wp_register_script(
			'oep-headline-text-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/headline-text.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore', 'wp-components' )
		);

		wp_register_script(
			'oep-text-list-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/text-list.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore', 'wp-components' )
		);

		wp_register_script(
			'oep-column-sidebar-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/column-sidebar.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore', 'wp-components' )
		);

		wp_register_script(
			'oep-recent-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/recent-content.js',
			array( 'wp-blocks', 'wp-element', 'wp-data', 'wp-i18n', 'wp-editor', 'underscore', 'wp-components' )
		);

		wp_register_script(
			'oep-stories-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/stories.js',
			array( 'wp-blocks', 'wp-element', 'wp-data', 'wp-i18n', 'wp-editor', 'underscore', 'wp-components' )
		);

		wp_register_script(
			'oep-contact-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/contact.js',
			array( 'wp-blocks', 'wp-element', 'wp-data', 'wp-i18n', 'wp-editor', 'underscore', 'wp-components' )
		);

		wp_register_script(
			'oep-authorpicks-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/authorpicks.js',
			array( 'wp-blocks', 'wp-element', 'wp-data', 'hm-gb-tools-editor'  )
		);

		wp_register_script(
			'oep-general-text-header-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/general-text-header.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-data'  )
		);

		wp_register_script(
			'oep-industry-card-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/industry-card.js',
			array( 'wp-blocks', 'wp-editor', 'wp-i18n', 'wp-element', 'wp-components' )
		);

		wp_register_script(
			'oep-col-featured-cards-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/col-featured-cards.js',
			array( 'wp-blocks', 'wp-editor', 'wp-i18n', 'wp-element' )
		);

		wp_register_script(
			'oep-col-table-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/col-table.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element' )
		);

		wp_register_script(
			'oep-angled-background-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/angled-background.js',
			array( 'wp-blocks', 'wp-editor', 'wp-i18n', 'wp-element', 'wp-components' )
		);

		wp_register_script(
			'oep-archive-loop-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/archive-loop.js',
			array( 'wp-blocks', 'wp-i18n', 'wp-element' )
		);

		wp_register_script(
			'oep-section-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/section.js',
			array( 'wp-blocks', 'wp-editor', 'wp-i18n', 'wp-element' )
		);

		wp_register_script(
			'oep-taxonomy-featured-posts-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/taxonomy-featured-posts.js',
			array( 'wp-blocks', 'wp-editor', 'wp-i18n', 'wp-element', 'wp-components', 'wp-data', 'oep-editor' )
		);

		/*wp_register_script(
			'oep-newsletter-block',
			plugin_dir_url( __FILE__ ) . 'js/blocks/newsletter.js',
			array( 'wp-blocks', 'wp-element', 'wp-data' )
		);*/

		/*
		 * end register scripts
		 */

		/*
		 * begin register blocks
		 */

		register_block_type( 'oep/excerpt', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-excerpt',
		) );

		register_block_type( 'oep/hero', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-hero-block',
			'render_callback' => 'OEP\Common\render_hero_block',
		) );

		register_block_type( 'oep/hero-cta', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-hero-cta-block',
			'render_callback' => 'OEP\Common\render_hero_block',
		) );

		register_block_type( 'oep/media-text', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-media-text-block',
		) );

		register_block_type( 'oep/video-text', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-video-text-block',
		) );

		register_block_type( 'oep/event-text', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-event-text-block',
		) );

		register_block_type( 'oep/headline-text', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-headline-text-block',
		) );

		register_block_type( 'oep/text-list', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-text-list-block',
		) );

		register_block_type( 'oep/column-sidebar', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-column-sidebar-block',
		) );

		register_block_type( 'oep/recent', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-recent-block',
			'render_callback' => 'OEP\Common\render_recent_posts',
		) );

		register_block_type( 'oep/stories', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-stories-block',
			'render_callback' => 'OEP\Common\render_stories_block',
		) );

		register_block_type( 'oep/contact', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-contact-block',
			'render_callback' => 'OEP\Common\render_contact_form',
		) );

		register_block_type( 'oep/authorpicks', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-authorpicks-block',
			'render_callback' => 'OEP\Common\render_author_picks',
		) );

		register_block_type( 'oep/general-text-header', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-child-block-editor',
			'editor_script' => 'oep-general-text-header-block',
		) );

		register_block_type( 'oep/industry-card', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-child-block-editor',
			'editor_script' => 'oep-industry-card-block',
		) );

		register_block_type( 'oep/col-featured-cards', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-col-featured-cards-block',
		) );

		register_block_type( 'oep/col-table', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-col-table-block',
		) );

		register_block_type( 'oep/angled-background', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-angled-background-block',
			'render_callback' => 'OEP\Common\render_angled_background_block',
		) );

		register_block_type( 'oep/archive-loop', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-archive-loop-block',
			'render_callback' => 'OEP\Common\render_archive_loop_block',
		) );

		register_block_type( 'oep/section', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-section-block',
		) );


		// we're using ServerSideRender, so we need the attributes arg, plus the
		// type of data in terms' array
		register_block_type( 'oep/taxonomy-featured-posts', array(
			'style' => 'oep-block',
			'editor_style' => 'oep-block-editor',
			'editor_script' => 'oep-taxonomy-featured-posts-block',
			'render_callback' => 'OEP\Common\render_taxonomy_featured_posts_block',
			'attributes' => [
				'taxonomy' => [
					'type'    => 'string',
					'default' => OEP_COMPETITIVENESS_KEY,
				],
				'terms' => [
					'type'    => 'array',
					'default' => [],
					'items'   => [
						'type' => 'string',
					],
				],
				'featuredTag' => [
					'type'    => 'boolean',
					'default' => true,
				],
				'blogs' => [
					'type'    => 'array',
					'default' => [],
					'items'   => [
						'type' => 'integer',
					],
				],
			]
		) );
	}

	/**
	 * Other gutenberg stuff I don't need to register blocks for, but need for editing
	 */

	public function enqueue_block_scripts() {
		/**
		 * Recommended stories meta block
		 */
		wp_enqueue_script(
			'oep-recommended-stories',
			plugin_dir_url( __FILE__ ) . 'js/blocks/recommendedstories.js',
			array( 'wp-blocks', 'wp-element', 'hm-gb-tools-editor' )
		);

		/**
		 * Slider block
		 */
		wp_enqueue_script(
			'oep-slider',
			plugin_dir_url( __FILE__ ) . 'js/blocks/slides.js',
			array( 'wp-blocks', 'wp-element', 'hm-gb-tools-editor' )
		);

		/**
		 * Title w/ menu block
		 */
		wp_enqueue_script(
			'oep-title-menu',
			plugin_dir_url( __FILE__ ) . 'js/blocks/title-menu.js',
			array( 'wp-blocks', 'wp-element', 'wp-data', 'wp-i18n', 'wp-editor', 'underscore', 'wp-components' )
		);

		/**
		 * Hubspot js for rendering forms in the backend
		 */
		wp_enqueue_script(
			'oep-hubspot',
			'//js.hsforms.net/forms/v2.js'
		);

		/**
		 * front end scripts and stuff we need
		 */
		wp_enqueue_script( 'oep-admin-theme', get_template_directory_uri() . '/assets/js/foot.min.js', [ 'jquery' ], false, true );
		wp_enqueue_script( 'admin-font-awesome', '//pro.fontawesome.com/releases/v5.8.1/js/all.js' );
	}

	/**
	 * register ACF newsletter block
	 */

	public function newsletter_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-newsletter-block',
				'title'				=> __('Newsletter Signup'),
				'description'		=> __('Embed a Hubspot signup form'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/newsletter.php',
				'category'			=> 'oep-common',
				'icon'				=> 'megaphone',
				'keywords'			=> array( 'newsletter' ),
				'mode'              => 'preview',
				'enqueue_assets' => function(){
					wp_enqueue_script( 'hsforms', '//js.hsforms.net/forms/v2.js', array(), '2', false );
				},
			));
		}
	}

	/**
	 * register ACF social cards block
	 */

	public function social_cards_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-social-cards-block',
				'title'				=> __('Social Cards'),
				'description'		=> __('Create a list of social posts for #thisisorlando.'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/social-cards.php',
				'category'			=> 'oep-common',
				'icon'				=> 'images-alt',
				'keywords'			=> array( 'social cards' ),
				'mode'              => 'preview',
			));
		}
	}

	/**
	 * register ACF success story block
	 */

	public function success_story_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-success-story-block',
				'title'				=> __('Success Story'),
				'description'		=> __('Display a featured success story.'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/success-story.php',
				'category'			=> 'oep-common',
				'icon'				=> 'id',
				'keywords'			=> array( 'success' ),
				'mode'              => 'preview',
			));
		}
	}

	/**
	 * register ACF stats block
	 */

	public function stats_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-stats-block',
				'title'				=> __('Stats'),
				'description'		=> __('Create a list of stats about Orlando.'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/stats.php',
				'category'			=> 'oep-common',
				'icon'				=> 'images-alt',
				'keywords'			=> array( 'stat' ),
				'mode'              => 'preview',
			));
		}
	}

	/**
	 * Register hero slider block
	 *
	 * @since 1.2.0
	 */
	public function hero_slider_block() {

		acf_register_block_type( array(
			'name'            => 'oep-hero-slider-block',
			'title'           => __( 'Hero Slider', 'oep' ),
			'description'     => __( 'Featured headings/images slider for page banner', 'oep' ),
			'render_template' => OEP_PLUGIN_DIR . 'public/partials/hero-slider.php',
			'category'        => 'oep-common',
			'icon'            => 'images-alt',
			'keywords'        => array( 'hero', 'banner', 'slider', 'slide', 'slides', 'image' ),
			'mode'            => 'preview',
		));
	}

	/**
	 * Trending stories block
	 */
	public function trending_stories_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-trending-stories',
				'title'				=> __('Trending Stories'),
				'description'		=> __('Trending Stories from Amplified'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/trending_stories.php',
				'category'			=> 'oep-common',
				'icon'				=> 'images-alt',
				'keywords'			=> array( 'trending stories' ),
				'mode'              => 'preview',
			));
		}
	}

	/**
	 * register ACF CTA Option block
	 */

	public function cta_option_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-cta-option-block',
				'title'				=> __('CTA Option'),
				'description'		=> __('Display 1 or 2 CTAs'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/cta-option.php',
				'category'			=> 'oep-common',
				'icon'				=> 'images-alt',
				'keywords'			=> array( 'social cards' ),
				'mode'              => 'auto',
			));
		}
	}

	/**
	 * register ACF newsletter block
	 */

	public function company_cards_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-company-cards-block',
				'title'				=> __('Company Cards'),
				'description'		=> __('Grid of Company Cards'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/company-cards.php',
				'category'			=> 'oep-common',
				'icon'				=> 'megaphone',
				'keywords'			=> array( 'company cards' ),
				'mode'              => 'auto',
			));
		}
	}

	/**
	 * Register welcome collage block
	 *
	 * @since 1.2.0
	 */
	public function welcome_collage_block() {

		acf_register_block_type( array(
			'name'            => 'oep-welcome-collage-block',
			'title'           => __( 'Welcome Collage', 'oep' ),
			'description'     => __( 'Some intro text and styled image cards around the sides.', 'oep' ),
			'render_template' => OEP_PLUGIN_DIR . 'public/partials/welcome-collage.php',
			'category'        => 'oep-landing-page',
			'icon'            => 'layout',
			'keywords'        => array( 'images', 'collage', 'welcome', 'cta', 'warm welcome', 'intro' ),
			'mode'            => 'preview',
		));
	}

	/**
	 * register ACF (single) industry card block
	 * For use w/ the ACF cards block below and Things To Do section
	 * Do not confuse w/ the native Guntenberg block above
	 */

	public function industry_card_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-industry-card-block',
				'title'				=> __('ACF Industry Card'),
				'description'		=> __('Single Industry Card'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/industry-card.php',
				'category'			=> 'oep-common',
				'icon'				=> 'images-alt',
				'keywords'			=> array( 'industry card' ),
				'mode'              => 'auto',
			));
		}
	}

	/**
	 * register ACF industry cards block
	 */

	public function industry_cards_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-industry-cards-block',
				'title'				=> __('Industry Cards'),
				'description'		=> __('Industry Cards slider'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/industry-cards.php',
				'category'			=> 'oep-common',
				'icon'				=> 'images-alt',
				'keywords'			=> array( 'industry cards' ),
				'mode'              => 'auto',
			));
		}
	}

	/**
	 * register ACF Neighborhoods map block
	 */

	public function neighborhoods_map_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-neighborhoods-map-block',
				'title'				=> __('Neighborhoods Map'),
				'description'		=> __('Neighborhoods map'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/neighborhoods-map.php',
				'category'			=> 'oep-common',
				'icon'				=> 'images-alt',
				'keywords'			=> array( 'neighborhood' ),
				'mode'              => 'auto',
			));
		}
	}

	/**
	 * register misc post_meta
	 */

	public function register_post_meta() {
		/**
		 * Recommended stories meta
		 */
		$registered = register_meta( 'post', '_oep_recommended_stories', array(
			'show_in_rest' => true,
			'single' => true,
			'type' => 'string',
			'auth_callback' => function() {
				return current_user_can( 'edit_posts' );
			}
		) );
		$registered = register_meta( 'post', '_oep_selected_slides', array(
			'show_in_rest' => true,
			'single' => true,
			'type' => 'string',
			'auth_callback' => function() {
				return current_user_can( 'edit_posts' );
			}
		) );
	}

	/**
	 * Load Google Fonts
	 *
	 * @since 1.0.0
	 */
	public static function add_webfonts() { ?>

		<script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
		<script>
		  WebFont.load({
			google: {
			  families: [
				'Merriweather:400i',
				'Andada'
			  ],
			  testStrings: {
				'Andada': '\u201c'
			  }
			},
			custom: {
			  families: [
				'Modelica Regular', 'Modelica Medium', 'Modelica Medium Italic', 'Modelica Bold', 'Modelica Extra Bold'
			  ]
			}
		  })
		</script>

	<?php }


	/**
	 * Admin notices when things don't go the way we want them to.
	 *
	 * @since 0.1.0
	 */
	public function admin_notice__error() {
		$class = 'notice notice-error';
		/*
		 * no ACF, that's a problem. I'd die first.
		 */
		if ( ! class_exists( 'ACF' ) ) {
			$message = __( 'The OEP plugin really needs Advanced Custom Fields to be enabled to work very well, if at all. Please enable to continue using this plugin.', 'oep' );
			printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
		}
	}

	/**
	 * Customize editor experience based on custom post type being created.
	 *
	 * @param $body_classes
	 *
	 * @return string
	 */
	public function customize_editor( $body_classes ) {
		$body_classes .= ' is-fullscreen-mode';

		return $body_classes;
	}

	/**
	 * Enqueue front-end assets for blocks.
	 * @var $fragment \DOMDocumentFragment
	 * @var $nodes \DOMNodeList
	 * @var $node IvoPetkov\HTML5DOMElement
	 * @var $dom \DOMDocument
	 *
	 * @return array
	 *
	 * @access public
	 */
	public function block_gallery_scripts($block) {

		if ( $block['blockName'] == 'blockgallery/carousel' ) {
			$dom = new HTML5DOMDocument();
			$dom->loadHTML( $block['innerHTML'], LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
			$nodes = $dom->getElementsByTagName( 'figure' );
			foreach ($nodes as $node){
				$image_id = $node->querySelector("img")->getAttribute("data-id");

				$image_caption = wp_get_attachment_caption($image_id);
				$figcaption = $dom->createElement( 'figcaption' );
				$figcaption->textContent = $image_caption;
				$node->appendChild($figcaption);
				$html = $dom->saveHTML();
			}
			$block['innerHTML'] = $html;
			$block['innerContent'][0] = $html;


		}

		return $block;

		/*// Define where the asset is loaded from.
		$dir = Block_Gallery()->asset_source( 'js' );

		// Define where the vendor asset is loaded from.
		$vendors_dir = Block_Gallery()->asset_source( 'js', 'vendors' );

		// Masonry block.
		if ( has_block( 'blockgallery/masonry' ) ) {
			wp_enqueue_script(
				$this->_slug . '-masonry',
				$dir . $this->_slug . '-masonry' . BLOCKGALLERY_ASSET_SUFFIX . '.js',
				array( 'jquery', 'masonry', 'imagesloaded' ),
				$this->_version,
				true
			);
		}

		// Carousel block.
		if ( has_block( 'blockgallery/carousel' ) ) {
			wp_enqueue_script(
				$this->_slug . '-flickity',
				$vendors_dir . 'flickity' . BLOCKGALLERY_ASSET_SUFFIX . '.js',
				array( 'jquery' ),
				$this->_version,
				true
			);
		}*/
	}
}

//functions needed outside of class but within namespace

/**
 * Server rendering for author picks
 * @param $author_picks array of post ids
 * @param $content mixed content
 * @return mixed
 */
function render_author_picks($author_picks, $content) {
	if ( is_admin() ) {
		return $content;
	}
	if ( empty( $author_picks ) ) {
		return;
	}

	$post_ids = wp_list_pluck( $author_picks['postsToDisplay'], 'id');
	$author_picks_query_args = array(
		'ignore_sticky_posts' => true,
		'post__in' => $post_ids,
		'post_type' => array_merge( [ 'post' ], PostType::get_cpt_keys())
	);
	$author_picks_query = new \WP_Query($author_picks_query_args);
	ob_start();
	oep_fn_template_part( 'author-picks', '', array( 'author_picks_query' => $author_picks_query ) );
	return ob_get_clean();

}

function render_hero_block($attributes, $content) {
	if ( is_admin() ) {
		return $content;
	}
	if ( empty( $attributes ) ) {
		return;
	}

	ob_start();
	oep_fn_template_part( 'hero', '', array( 'attributes' => $attributes, 'content' => $content ) );
	return ob_get_clean();
}

function render_contact_form($attributes, $content) {
	if ( is_admin() ) {
		return $content;
	}

	$style = isset($attributes['style']) ? $attributes['style'] : "normal"; //default

	ob_start();
	oep_fn_template_part( 'contact', $style, array( 'attributes' => $attributes, 'content' => $content ) );
	return ob_get_clean();
}


/**
 * render function for events_block
 * @param $block array ACF custom block
 * @return mixed
 */

function events_block_render_callback( $block ){
	if ( is_admin() ) {
		return;
	}
	if ( empty( $block ) ) {
		return;
	}
	ob_start();
	oep_fn_template_part( 'event', '', array( 'block' => $block ) );
	return ob_get_clean();
}

/**
 * render function for recent content block
 * @param $attributes array of gutenberg block attributes
 * @param $content string block content, probably empty
 * @return mixed
 */

function render_recent_posts( $attributes, $content ){
	if ( is_admin() ) {
		return;
	}
	/**
	 * @TODO so gutenberg doesn't send back default values from the inspector controls, so we're resetting them here until we can figure out why
	 */

	$defaults = array(
		'title' => __("Recent Stories", "oep"),
		'num_posts' => 4,
		'post_type' => 'post',
		'style' => 'horizontal'
	);

	$args = wp_parse_args( $attributes, $defaults);

	if($args["style"] === "full") {
		//doing full results listing with filter
		/**
		 * @TODO fix pills display and pagination issues, as well as filtering
		 */
		$exclude = [ OEP_PROFILE_KEY, OEP_LANDING_PAGE_KEY, OEP_COMPANY_KEY, OEP_EVENT_KEY ];
		$post_types = array_diff( array_merge( [ "post" ], \OEP\CPT\PostType::get_cpt_keys()), $exclude );
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		global $landing_page_terms;
		$tax_query = array();
		//do we have terms? build out a tax_query to pass to the query_posts call
		if( !empty( $landing_page_terms ) ){
			foreach ($landing_page_terms as $landing_page_term){
				$tax_query[] = array(
						'taxonomy'  => $landing_page_term["taxonomy"],
						'field'     => "term_id",
						'terms'     => $landing_page_term["term_id"],
				);
			}
		}
		$recent_posts_args = array(
			'post_type' => $post_types,
			'paged'     => $paged,
//			'posts_per_page' => 999
		);

		!empty($tax_query) ? $recent_posts_args['tax_query'] = array_merge(array('relation' => "OR"), $tax_query) : "";

		/**
		 * ok hear me out, there's a really good reason I'm using the much reviled query_posts function here, because this isn't really an archive page so pre_get_posts wouldn't work for setting global wp_query arguments, and it only happens when this gutenberg block is available on a landing page, which is difficult and redundant to check for on every pre_get_posts call we would have made otherwise. kthxbye.
		 */
		$recent_posts_query = query_posts($recent_posts_args);
		ob_start();
		oep_fn_template_part( 'recent-posts', 'full', array( 'attributes' => $args, 'recent_posts_query' => $recent_posts_query ) );
		global $wp_query;
		global $ajax_query_vars;
		$big   = 999999999; // need an unlikely integer
		$ajax_query_vars = $wp_query->query;

		wp_localize_script(
			'oep-foot',
			'ajaxpagination',
			array(
				'ajaxurl' => admin_url('admin-ajax.php'),
				'query_vars' => json_encode( $wp_query->query ),
				'spinner_url' => OEP_DIR_URI .'/assets/images/ajax.svg',
				'base_url' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) )
			)
		);

		//not a taxonomy, sooo...
		$wp_query->is_tax     = false;

		wp_reset_query();
		return ob_get_clean();
	} else {
		$recent_posts_args = array(
			'post_type' => $args['post_type'],
			'posts_per_page' => $args['num_posts']
		);

		$recent_posts_query = new \WP_Query($recent_posts_args);
		ob_start();
		oep_fn_template_part( 'recent-posts', '', array( 'attributes' => $args, 'recent_posts_query' => $recent_posts_query ) );
		return ob_get_clean();
	}
}


/**
 * @param $attributes
 *
 * @return false|string
 */
function render_stories_block($attributes) {
	$defaults = array(
		'title' => __("Stories", "oep"),
		'num_posts' => 3,
		'taxonomy' => OEP_COMPETITIVENESS_KEY,
		'terms' => []
	);

	$args = wp_parse_args( $attributes, $defaults);

	$tax_query = array(
//        'relation' => 'OR',
		array(
			'taxonomy' => $args['taxonomy'],
			'field' => 'term_id',
			'terms' => $args['terms'],
		)
	);

	$stories_args = array(
		'post_type' => ['post', OEP_VIDEO_KEY, OEP_PODCAST_KEY, OEP_SUCCESS_KEY, ],
		'posts_per_page' => $args['num_posts'],
		'tax_query' => $tax_query
	);

	$stories_query = new \WP_Query($stories_args);

	ob_start();
	oep_fn_template_part( 'stories', '', array( 'attributes' => $args, 'stories_query' => $stories_query ) );
	return ob_get_clean();
}


/**
 * Get the newsletter sign up block
 * @param $attributes
 * @param $content
 *
 * @return false|string
 */
function render_newsletter_block($attributes, $content) {
	ob_start();
	oep_fn_template_part( 'newsletter' );
	return ob_get_clean();
}


/**
 * Render angled background block
 *
 * @since 1.2.0
 */
function render_angled_background_block( $attributes, $content ) {
	ob_start();
	oep_fn_template_part( 'angled', 'background', [ 'attrs' => $attributes, 'content' => $content ] );
	return ob_get_clean();
}

/**
 * Render archive loop block
 *
 * @since 1.2.0
 */
function render_archive_loop_block() {

	// bounce if a single page
	if ( ! is_archive() ) {
		return;
	}

	ob_start();

	/**
	 * Hooked
	 *
	 * Search theme(s) for extra post-type specific externsions that put in
	 * wrappers and stuff.
	 *
	 * Oep_Archive_Blocks::do_loop_wrap_open - 10
	 * Oep_Archive_Blocks::do_the_loop - 30
	 * Oep_Archive_Blocks::do_loop_wrap_close - 50
	 */
	do_action( 'oep_archive_loop' );

	return ob_get_clean();
}


/**
 * Render block for taxonomy featured posts
 *
 * @param  array   $attributes  block attributes: 'taxonomy', 'terms'
 * @return string
 *
 * @since  1.2.0
 */
function render_taxonomy_featured_posts_block( $attributes ) {

	ob_start();

	echo '<section class="wp-block-oep-taxonomy-featured-posts">';
		do_action(
			'taxonomy_featured_posts',
			$attributes['taxonomy'],
			$attributes['terms'],
			$attributes['featuredTag'],
			$attributes['blogs']
		);
	echo '</section>';

	return ob_get_clean();
}


/**
 * Add network sites for editor controls
 *
 * @param   array  $object  existing localized JS var
 * @return  array  $object
 * @since   1.2.0
 */
function add_network_sites( $object ) {

	foreach ( get_sites([ 'fields' => 'ids' ]) as $blog_id ) {

		$blog = get_blog_details( $blog_id );

		$object['sites'][] = [
			'label' => $blog->blogname,
			'value' => $blog_id,
		];
	}

	return $object;
}
add_filter( 'oep_editor_object', '\OEP\Common\add_network_sites' );
