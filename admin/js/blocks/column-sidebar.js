( function( blocks, editor, element, components, i18n ) {
var el = element.createElement;
var InnerBlocks = editor.InnerBlocks;
var RadioControl = components.RadioControl;
var InspectorControls = editor.InspectorControls;

var BLOCKS_TEMPLATE = [
  ['core/columns', {}, [
    ['core/column', {}, [
      ['core/paragraph', {placeholder: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. '}],
    ]
    ],
    ['core/column', {}, [

    ]],
  ]
  ]]

blocks.registerBlockType('oep/column-sidebar', {
  title: 'Column + sidebar',
  category: 'oep-landing-page',
  attributes: {
    alignment: {
      type: 'string',
      default: 'left',
    },
  },
  edit: (props) => {
    var alignment = props.attributes.alignment;

    return[
      el(
        InspectorControls,
        { key: 'inspector' },
        el(components.PanelBody, {
            title: i18n.__('Block Options'),
            className: 'block-options',
            initialOpen: true
          },
          el('p', {}, i18n.__('Where should the sidebar be positioned?')),// Radio buttons
          el( RadioControl,
            {
              label: 'Sidebar position',
              //help: 'Some kind of description',
              options : [
                { label: 'Left', value: 'left' },
                { label: 'Right', value: 'right' },
              ],
              onChange: ( value ) => {
                props.setAttributes( { alignment: value } );
              },
              selected: props.attributes.alignment
            }
          ),
        ),
      ),
      el('div', {className: props.className},
        el(InnerBlocks, {
          className:'align-' + props.attributes.alignment,
          template: BLOCKS_TEMPLATE,
          templateLock: false
        })
      )
    ]
  },
  save: (props) => {
    return el('div', {className:'align-' + props.attributes.alignment},
      el(InnerBlocks.Content)
    )
  },
})
}(
  window.wp.blocks,
  window.wp.editor,
  window.wp.element,
  window.wp.components,
  window.wp.i18n
) );