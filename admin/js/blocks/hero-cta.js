(function (blocks, editor, i18n, element, components, _) {
  var el = element.createElement
  var RichText = editor.RichText
  var InnerBlocks = editor.InnerBlocks;
  var MediaUpload = editor.MediaUpload
  var Button = components.Button

  blocks.registerBlockType('oep/hero-cta', {
    title: i18n.__('Hero + CTA', 'oep'),
    icon: 'slides',
    category: 'oep-landing-page',

    attributes: {
      title: {
        type: 'string',
        format: 'string',
        selector: 'h1',
      },
      subtitle: {
        type: 'string',
        format: 'string',
        selector: 'h3',
      },
      body: {
        type: 'string',
        format: 'string',
        selector: 'p',
      },
      cta: {
        type: 'string',
        format: 'string',
        selector: 'a',
      },
      mediaID: {
        type: 'number',
      },
      mediaURL: {
        type: 'string'
      },
    },
    edit: function (props) {
      var attributes = props.attributes

      var onSelectImage = function (media) {
        return props.setAttributes({
          mediaURL: media.sizes.banner.url,
          mediaID: media.id,
        })
      }
      return (
        el('div', {className: props.className},
          el(RichText, {
            tagName: 'h3',
            inline: true,
            placeholder: i18n.__('Hero Subtitle', 'oep'),
            value: attributes.subtitle,
            onChange: function (value) {
              props.setAttributes({subtitle: value})
            },
          }),
          el(RichText, {
            tagName: 'h1',
            inline: true,
            placeholder: i18n.__('Hero Title', 'oep'),
            value: attributes.title,
            onChange: function (value) {
              props.setAttributes({title: value})
            },
          }),
          el(RichText, {
            tagName: 'p',
            inline: true,
            placeholder: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. ',
            value: attributes.body,
            onChange: function (value) {
              props.setAttributes({body: value})
            },
          }),
          el('div', {className: 'cta'},
            el(InnerBlocks, {
              template: [["core/button"]],
            }),
          ),
          el('div', {className: 'bg-image'},
            el(MediaUpload, {
              onSelect: onSelectImage,
              allowedTypes: 'image',
              value: attributes.mediaID,
              render: function (obj) {
                return el(components.Button, {
                    className: attributes.mediaID ? 'image-button' : 'button button-large',
                    onClick: obj.open
                  },
                  !attributes.mediaID ? i18n.__('Upload Image', 'oep') : el('img', {src: attributes.mediaURL})
                )
              }
            })
          ),
        )
      );
    },
    save: function (props) {
      var attributes = props.attributes;

      return el('div', {className:'cta'},
        el(InnerBlocks.Content)
      )
    },
  })

}(
  window.wp.blocks,
  window.wp.editor,
  window.wp.i18n,
  window.wp.element,
  window.wp.components,
  window._,
))

