/**
 * Industry card template
 *
 * @since 1.2.0
 */
( function( blocks, editor, i18n, element, components ) {

	var el = element.createElement,
		registerBlockType = blocks.registerBlockType,
		__ = i18n.__,
		RichText = editor.RichText,
		MediaUpload = editor.MediaUpload

	registerBlockType( 'oep/industry-card', {

		// default stuff
		title: 'Industry Card',
		icon: 'editor-table',
		category: 'oep-col',

		/**
		 * Attributes
		 */
		attributes: {

			// custom, passed in by other blocks
			className: {
				type: 'string'
			},

			// custom, passed in by other blocks
			wrapperTag: {
				type: 'string',
			},

			title: {
				type: 'array',
				source: 'children',
				selector: '.industry-card_title',
			},

			// custom, disables inline title editing
			titleLock: {
				type: 'boolean',
				default: false
			},

			imageID: {
				type: 'number'
			},

			imageURL: {
				type: 'string',
				source: 'attribute',
				selector: 'img',
				attribute: 'src'
			}
		},


		/**
		 * Block editor template
		 */
		edit: ( props ) => {

			var attrs = props.attributes

			// image handler
			var onSelectImage = ( media ) => {
				return props.setAttributes({
					imageURL: media.sizes.large ? media.sizes.large.url : media.sizes.full.url,
					imageID: media.id,
				})
			}

			// title handler
			var onTitleChange = ( value ) => {
				props.setAttributes({ title: value })
			}

			// show media button or image
			var doImageEdit = ( obj ) => {
				return el( components.Button, {
					className: 'image-button button button-large', // attrs.imageID ? 'image-button' : 'button button-large',
					onClick: obj.open
				}, __( 'Set Image', 'oep' ) );
			}

			// apply custom class to figure if there's no wrapper item
			var customClass = 'industry-card' + ( attrs.wrapperTag ? '' : ' ' + attrs.className )

			// actual figure (in case a wrapper is needed)
			var figure = el( 'figure', { className: customClass },

				// image uploader
				el( MediaUpload, {
					onSelect: onSelectImage,
					allowedTypes: 'image',
					value: attrs.imageID,
					render: doImageEdit,
				}),

				// card background wrapper
				el( 'div', { className: 'industry-card_thumbnail'},
					attrs.imageID &&
						el( 'img', { src: attrs.imageURL } )
				),

				// figcaption with title
				el( 'figcaption', { className: 'industry-card_content' },
					el( attrs.titleLock ? RichText.Content : RichText, {
						key: 'richtext',
						tagName: 'h3',
						className: 'industry-card_title',
						inline: ! attrs.titleLock,
						value: attrs.title,
						onChange: onTitleChange
					})
				)

			) // .industry-card

			// conditionally put in wrapper tag
			return attrs.wrapperTag
				? el( attrs.wrapperTag, { className: attrs.className }, figure )
				: figure;

		}, // edit


		/**
		 * Post content template
		 */
		save: ( props ) => {
			var attrs = props.attributes

			// apply custom class to figure if there's no wrapper item
			var customClass = 'industry-card' + ( attrs.wrapperTag ? '' : ' ' + attrs.className )

			// actual figure (in case a wraopper is needed)
			var figure = el( 'figure', { className: customClass },

				attrs.imageURL &&
					el( 'div', { className: 'industry-card_thumbnail' },
						el( 'img', { src: attrs.imageURL } )
					),

				el( 'figcaption', { className: 'industry-card_content' },
					el( RichText.Content, {
						tagName: 'h3',
						className: 'industry-card_title',
						value: attrs.title,
					})
				)

			) // figure.industry-card

			// conditionally put in wrapper tag
			return attrs.wrapperTag
				? el( attrs.wrapperTag, { className: attrs.className }, figure )
				: figure;

		} // save
	})
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components
));
