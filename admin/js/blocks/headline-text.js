( function( blocks, editor, element, components, i18n ) {
var el = element.createElement;
var InnerBlocks = editor.InnerBlocks;

var BLOCKS_TEMPLATE = [
  ['core/heading', {placeholder: 'Subtitle of Section', level: 4, align: 'center'}],
  ['core/heading', {placeholder: 'Headline', level: 1, align: 'center'}],
  ['core/paragraph', {placeholder: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. ', align: 'left'}],
  ['core/button', {text: 'Button Text', align: 'center'}]
]

blocks.registerBlockType('oep/headline-text', {
  title: 'Headline + text',
  category: 'oep-landing-page',

  edit: (props) => {

    return[
      el(InnerBlocks, {
        className:'align-center',
        template: BLOCKS_TEMPLATE,
        templateLock: false
      })
    ]
  },
  save: (props) => {
    return el('div', {className:'align-center'
      },
      el(InnerBlocks.Content)
    )
  },
})
}(
  window.wp.blocks,
  window.wp.editor,
  window.wp.element,
  window.wp.components,
  window.wp.i18n
) );