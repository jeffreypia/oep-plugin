(function (blocks, editor, i18n, element, components, _) {
  var el = element.createElement
  var RichText = editor.RichText
  var MediaUpload = editor.MediaUpload
  var InspectorControls = editor.InspectorControls

  blocks.registerBlockType('oep/hero', {
    title: i18n.__('Hero Region', 'oep'),
    icon: 'slides',
    category: 'oep-landing-page',

    attributes: {
      title: {
        type: 'string',
        format: 'string',
        selector: 'h1',
      },
      subtitle: {
        type: 'string',
        format: 'string',
        selector: 'h3',
      },
      mediaID: {
        type: 'number',
      },
      mediaURL: {
        type: 'string',
      },
      dividerColor: {
        type: 'string',
      },
    },
    edit: function (props) {
      var attributes = props.attributes

      var onSelectImage = function (media) {
        return props.setAttributes({
          mediaURL: media.sizes.banner.url,
          mediaID: media.id,
        })
      }
      return [

        el(element.Fragment, { key: 'inspector-wrap' },
          el(InspectorControls, { key: 'inspector' },
            el(components.PanelBody, {
              title: i18n.__( 'Hero Settings' ),
              className: 'block-options',
              initialOpen: true },

              el(components.SelectControl, {
                label: i18n.__('Divider Color', 'oep'),
                options: [
                  { label: i18n.__( 'White' ), value: 'white' },
                  { label: i18n.__( 'Gray' ), value: 'gray' },
                  { label: i18n.__( 'Transparent' ), value: 'transparent' },
                ],
                value: props.attributes.dividerColor,
                onChange: ( value ) => {
                  props.setAttributes({ dividerColor: value });
                }
              }),
            )
          ),
        ),

        el('div', { key: 'hero-wrap', className: props.className},
          el(RichText, {
            tagName: 'h3',
            inline: true,
            placeholder: i18n.__('Hero Subtitle', 'oep'),
            value: attributes.subtitle,
            onChange: function (value) {
              props.setAttributes({subtitle: value})
            },
          }),
          el(RichText, {
            tagName: 'h1',
            inline: true,
            placeholder: i18n.__('Hero Title', 'oep'),
            value: attributes.title,
            onChange: function (value) {
              props.setAttributes({title: value})
            },
          }),
          el('div', {className: 'bg-image'},
            el(MediaUpload, {
              onSelect: onSelectImage,
              allowedTypes: 'image',
              value: attributes.mediaID,
              render: function (obj) {
                return el(components.Button, {
                    className: attributes.mediaID ? 'image-button' : 'button button-large',
                    onClick: obj.open
                  },
                  !attributes.mediaID ? i18n.__('Upload Image', 'oep') : el('img', {src: attributes.mediaURL})
                )
              }
            })
          ), // .bg-image
        ) // outer wrapper div
      ]
    },
    save: function (props) {
      return null
    },
  })

}(
  window.wp.blocks,
  window.wp.editor,
  window.wp.i18n,
  window.wp.element,
  window.wp.components,
  window._,
))

