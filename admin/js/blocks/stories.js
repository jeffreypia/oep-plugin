( function (blocks, element, i18n, data, components, editor) {

  var el = element.createElement,
    registerBlockType = blocks.registerBlockType,
    postSelect = data.withSelect

  var __ = i18n.__

  var RangeControl = components.RangeControl;
  var RadioControl = components.RadioControl;
  var SelectControl = components.SelectControl;
  var TextControl = components.TextControl;
  var InspectorControls = editor.InspectorControls;

  var Terms = [];
  var Taxonomy = "";

  registerBlockType('oep/stories', {
    title: 'Stories',
    icon: 'star-filled',
    category: 'oep-common',

    attributes: {
      title: {
        type: 'string',
        default: i18n.__('Stories')
      },
      post_type: {
        type: 'string',
        default: 'post'
      },
      taxonomy: {
        type: 'string',
        default: 'oep_taxonomies_competitiveness'
      },
      terms: {
        type: 'array',
      },
      num_posts: {
        type: 'number',
        default: 3
      },
      postsToDisplay: {
        type: 'array,'
      },
    },

    edit: postSelect(function (select, ownProps) {
      if (ownProps.selected_posts) {
        return
      }

       Terms = _.map(select('core').getEntityRecords('taxonomy', ownProps.attributes.taxonomy, {
         per_page: 99,
       }), function (term){
         return { label: term.name, value: term.id }
       })

      return {
        selected_posts: _.map(select('core').getEntityRecords('postType', ownProps.attributes.post_type, _.object(
          [
            ["per_page", ownProps.attributes.num_posts],
            [ownProps.attributes.taxonomy, ownProps.attributes.terms]
          ]
        )), function (postToDisplay) {
          var current_post = select('core').getEntityRecord('postType', postToDisplay.type, postToDisplay.id)
          // console.log(current_post)
          if( current_post && current_post.meta == undefined ){
            Object.defineProperty(current_post, "meta", {value : [],
              writable : true,
              enumerable : true,
              configurable : true});
            current_post.meta['featured_media_rendered'] = "";
          }
          if (current_post && current_post.featured_media && undefined !== current_post.meta) {
            var current_post_image = select('core').getMedia(current_post.featured_media)
            if (current_post_image && undefined !== current_post_image) {
              // console.log(current_post_image.source_url)
              current_post.meta['featured_media_rendered'] = current_post_image.description.rendered
            }
            return current_post
          }
          return current_post
        })
      }
    })(function (props) {
      // console.log(props)

      if (!props.selected_posts || undefined === props.selected_posts) {
        return 'Loading...'
      }
      //is postsToDisplay set? return the rendered post
      var thumbHTML = ''
      if (props.selected_posts && undefined !== props.selected_posts && props.selected_posts.length > 0) {

        return [
          el(
            InspectorControls,
            { key: 'inspector' },
            el(components.PanelBody, {
                title: i18n.__('Block Options'),
                className: 'block-options',
                initialOpen: true
              },
              el( TextControl,
                {
                  label: 'Title',
                  help: 'What should the title be?',
                  onChange: ( value ) => {
                    props.setAttributes( { title: value } );
                  },
                }
              ),
              el( SelectControl,
                {
                  label: 'Taxonomy',
                  help: 'Choose a taxonomy to pick terms from',
                  options : [
                    { label: 'Competitiveness',   value: 'oep_taxonomies_competitiveness' },
                    { label: 'Quality of Life',   value: 'oep_taxonomies_qol' },
                    { label: 'Industries',        value: 'oep_taxonomies_industries' },
                    { label: 'Regions',           value: 'oep_taxonomies_regions' },
                    { label: 'Activity',          value: 'oep_taxonomies_activity' },
                    { label: 'Teams',             value: 'oep_taxonomies_teams' },
                    { label: 'Departments',       value: 'oep_taxonomies_departments' },
                    { label: 'Memberships',       value: 'oep_taxonomies_memberships' },
                    { label: 'Ideas',             value: 'oep_taxonomies_ideas' },
                    { label: 'Values',            value: 'oep_taxonomies_values' },
                  ],
                  onChange: ( value ) => {
                    props.setAttributes( { taxonomy: value } );
                  },
                  selected: props.attributes.taxonomy,
                  value: props.attributes.taxonomy
                }
              ),
              el( SelectControl,
                {
                  label: 'Terms',
                  multiple: true,
                  help: 'Choose your desired terms',
                  options : Terms,
                  onChange: ( value ) => {
                    props.setAttributes( { terms: value } );

                  },
                  selected: props.attributes.terms,
                  value: props.attributes.terms
                }
              ),
              el( RangeControl,
                {
                  label: 'Number of items',
                  help: 'How many items should be displayed?',
                  min: 3,
                  max: 6,
                  onChange: ( value ) => {
                    props.setAttributes( { num_posts: value } );
                  },
                  initialPosition: props.attributes.num_posts,
                  value: props.attributes.num_posts,
                }
              )
            ),
          ),
          el('h2', {}, props.attributes.title),
          el('div', {className: 'rendered-posts' + ' has-' + props.attributes.num_posts + '-posts'},
          _.map(props.selected_posts, function (selected_post) {
            if (undefined === selected_post) {
              return
            }
            if (undefined === selected_post.meta) {
              thumbHTML = '<div>Nothing to see here</div>'
            } else {
              thumbHTML = selected_post.meta['featured_media_rendered']
            }
            // console.log(selected_post)
            return el('div', {className: 'post'},
              el('div', {className: 'pills-container'},
                el('ul', {className: 'pills solid'},
                  el('li', {},
                    el('a', {href: '#'},
                      __("Sample Term", "oep") //filled out on front-end
                    )
                  )
                )
              ),
              el('figure', {
                  className: 'featured-image',
                  dangerouslySetInnerHTML: { //there's really good reason we're doing this, the content is already XSS checked because it's generated as a description when media is uploaded.
                    __html: thumbHTML
                  }
                }
              ),
              el('header', {className: 'post-header'},
                el('h4', {},
                  selected_post.title.raw
                )
              )
            )
          }),
            el('ul', {className: 'terms'},
              _.map(props.attributes.terms, function (selected_term) {
                  if (undefined === selected_term) {
                    return
                  }
                  // console.log(selected_term)
                  var term_object = wp.data.select('core').getEntityRecord('taxonomy', props.attributes.taxonomy, selected_term);
                  // console.log(term_object)
                  if (undefined === term_object || null === term_object) {
                    return
                  }
                  return el('li', {},
                    term_object.name
                    // "Sample Term"
                  )
                }
              )
            )
        )
        ]
      }
      return [
        el(
          InspectorControls,
          { key: 'inspector' },
          el(components.PanelBody, {
              title: i18n.__('Block Options'),
              className: 'block-options',
              initialOpen: true
            },
            el( TextControl,
              {
                label: 'Title',
                help: 'What should the title be?',
                onChange: ( value ) => {
                  props.setAttributes( { title: value } );
                },
              }
            ),
            el( SelectControl,
              {
                label: 'Taxonomy',
                help: 'Choose a taxonomy to pick terms from',
                options : [
                  { label: 'Competitiveness',   value: 'oep_taxonomies_competitiveness' },
                  { label: 'Quality of Life',   value: 'oep_taxonomies_qol' },
                  { label: 'Industries',        value: 'oep_taxonomies_industries' },
                  { label: 'Regions',           value: 'oep_taxonomies_regions' },
                  { label: 'Activity',          value: 'oep_taxonomies_activity' },
                  { label: 'Teams',             value: 'oep_taxonomies_teams' },
                  { label: 'Departments',       value: 'oep_taxonomies_departments' },
                  { label: 'Memberships',       value: 'oep_taxonomies_memberships' },
                  { label: 'Ideas',             value: 'oep_taxonomies_ideas' },
                  { label: 'Values',            value: 'oep_taxonomies_values' },
                ],
                onChange: ( value ) => {
                  props.setAttributes( { taxonomy: value } );
                },
                value: props.attributes.taxonomy
              }
            ),
            el( SelectControl,
              {
                label: 'Terms',
                multiple: true,
                help: 'Choose your desired terms',
                options : Terms,
                onChange: ( value ) => {
                  props.setAttributes( { terms: value } );

                },
                selected: props.attributes.terms,
                value: props.attributes.terms
              }
            ),
            el( RangeControl,
              {
                label: 'Number of items',
                help: 'How many items should be displayed?',
                min: 3,
                max: 6,
                onChange: ( value ) => {
                  props.setAttributes( { num_posts: value } );
                },
                initialPosition: props.attributes.num_posts,
                value: props.attributes.num_posts,
              }
            )
          ),
        ),
        el('div', {},
          __("Choose from the options on the right", "oep")
        )
      ]
    }),
    save: function () {
      return null
    },
  })

  }(
    window.wp.blocks,
    window.wp.element,
    window.wp.i18n,
    window.wp.data,
    window.wp.components,
    window.wp.editor,
  ))
