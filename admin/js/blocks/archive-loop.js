/**
 * Archive loop block
 *
 * Intended for "archive content override" pages so we can get the archive's
 * post loop in the page.
 *
 * @since   1.2.0
 * @package oep
 */
( function( blocks, i18n, element ) {

	var el = element.createElement,
	    registerBlockType = blocks.registerBlockType,
	    __ = i18n.__


	registerBlockType( 'oep/archive-loop', {

		// default stuff
		title: __( 'Archive Post Loop', 'oep' ),
		icon: 'list-view',
		category: 'oep-common',

		/**
		 * Block editor template
		 */
		edit: ( props ) => {
			return el( 'div', { className: props.className },
				el( 'h2', {}, __( 'Archive Post Loop', 'oep' ) )
			)
		},

		/**
		 * Post content template
		 */
		save: () => {
			return null
		}

	})
}(
	window.wp.blocks,
	window.wp.i18n,
	window.wp.element,
));
