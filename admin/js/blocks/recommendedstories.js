/**
 * @TODO use https://stackoverflow.com/questions/53404030/wordpress-gutenberg-withselect-get-list-of-post-types to get list of post types
 */
( function( blocks, element, i18n, data, components) {
  var el = wp.element.createElement;
  var registerBlockType = wp.blocks.registerBlockType;
  var PostSelectButton = components.PostSelectButton
  var postSelect = data.withSelect
  var __ = i18n.__

  registerBlockType( 'oep/recommendedstories', {
    title: 'Recommended Stories',
    icon: 'star-filled',
    category: 'oep-common',

    attributes: {
      postIds: {
        type: 'array,'
      },
      blockValue: {
        type: 'string',
        source: 'meta',
        meta: '_oep_recommended_stories'
      }
    },

    edit: postSelect(function (select, ownProps) {
      // console.log(ownProps)
      if (ownProps.selected_posts) {
        return
      }
      return {
        selected_posts: _.map(ownProps.attributes.postIds, function (postID) {
          var current_post = select('core').getEntityRecord('postType', 'post', postID)
          if (current_post && current_post.featured_media) {
            var current_post_image = select('core').getMedia(current_post.featured_media)
            if (current_post_image && undefined !== current_post_image) {
              // console.log(current_post_image.source_url)
              current_post.meta['featured_media_rendered'] = current_post_image.description.rendered
            }
            return current_post
          }
        })
      }
    })(function( props ) {
      var className = props.className;

      if (!props.selected_posts || undefined === props.selected_posts) {
        return 'Loading...'
      }

      //is postIds set? return the rendered post
      if (props.selected_posts && undefined !== props.selected_posts && props.selected_posts.length > 0) {
        return el('div', {className: className},
          el('h2', {}, __("Recommended Stories", "oep")),
          _.map(props.selected_posts, function (selected_post) {
            if (undefined === selected_post) {
              return
            }
            // console.log(selected_post)
            return el('div', {className: 'post'},
              el('div', {className: 'pills-container'},
                el('ul', {className: 'pills solid'},
                  el('li', {},
                    el('a', {href: '#'},
                      __("Sample Term", "oep") //filled out on front-end
                    )
                  )
                )
              ),
              el('figure', {
                  className: 'featured-image',
                  dangerouslySetInnerHTML: { //there's really good reason we're doing this, the content is already XSS checked because it's generated as a description when media is uploaded.
                    __html: selected_post.meta['featured_media_rendered']
                  }
                }
              ),
              el('header', {className: 'post-header'},
                el('h4', {},
                  selected_post.title.raw
                )
              )
            )
          }),
          el('div', {className: 'button-holder'},
            el(
              PostSelectButton, {
                value: props.attributes.postIds,
                onSelect: function (posts) {
                  props.setAttributes(
                    {
                      postIds: _.map(posts, function (post) {
                        return post.id
                      })
                    }
                  )
                  props.setAttributes(
                    {
                      blockValue: (_.map(posts, function (post) {
                        return post.id
                      })).toString()
                    }
                  )
                },
                postType: ['post','oep_cpts_video'],
                maxPosts: 4,
                btnProps: {isLarge: true},
              }, __('Select Posts')
            )
          )
        )
      }

      return el('div', {},
          el('h2', {}, __("Recommended Stories", "oep")),
              el(
          PostSelectButton, {
            value: props.attributes.postIds,
            onSelect: function (posts) {
              props.setAttributes(
                {
                  postIds: _.map(posts, function (post) {
                    return post.id
                  })
                }
              )
              props.setAttributes(
                {
                  blockValue: (_.map(posts, function (post) {
                    return post.id
                  })).toString()
                }
              )
            },
            // postType: 'post',
            maxPosts: 4,
            btnProps: {isLarge: true},
          }, __('Select Posts')
        )
      )

      /*return el(
        'div',
        { className: className },
        el('h2', {}, __("Recommended Stories", "oep")),
        el('div', {},
          props.attributes.blockValue
        ),
        el(
          PostSelectButton, {
            value: props.attributes.postIds,
            onSelect: function (posts) {
              props.setAttributes(
                {
                  postIds: _.map(posts, function (post) {
                    return post.id
                  })
                }
              )
              props.setAttributes(
                {
                  blockValue: (_.map(posts, function (post) {
                    return post.id
                  })).toString()
                }
              )
            },
            // postType: 'post',
            maxPosts: 4,
            btnProps: {isLarge: true},
          }, __('Select Posts')
        )
      );*/
    }),

    // No information saved to the block
    // Data is saved to post meta via attributes
    save: function() {
      return null;
    }
  } );
} )(
  window.wp.blocks,
  window.wp.element,
  window.wp.i18n,
  window.wp.data,
  window.hm.components,
);