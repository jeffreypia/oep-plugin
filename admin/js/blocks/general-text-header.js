/**
 * Title/subtitle + body and button block
 *
 * @since   1.0.0
 * @package oep
 */
( function( blocks, editor, element, components, i18n ) {

	var el = element.createElement,
		__ = i18n.__,
		InnerBlocks = editor.InnerBlocks,
		RadioControl = components.RadioControl,
		InspectorControls = editor.InspectorControls

	var BLOCKS_TEMPLATE = [

		[ 'core/heading', {
			placeholder: __( 'Subtitle of Section', 'oep' ),
			level: 4,
			align: 'center',
		}],

		[ 'core/heading' , {
			placeholder: __( 'Headline', 'oep' ),
			level: 1,
			align: 'center',
		}],

		[ 'core/paragraph', {
			placeholder: __( 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', 'oep' ),
			align: 'center',
		}],

		[ 'core/button', {
			text: __( 'Button Text', 'oep' ),
			className: 'text-only text-only-black',
			align: 'center',
		}],
	]

	blocks.registerBlockType( 'oep/general-text-header', {

		title: __( 'General Text Header', 'oep' ),
		category: 'oep-landing-page',

		attributes: {
			background: {
				type: 'string',
				default: '',
			},
		},

		edit: ( props ) => {

			return[
				el(
					InspectorControls,
					{ key: 'inspector' },
					el( components.PanelBody, {
							title: i18n.__( 'Block Options' ),
							className: 'block-options',
							initialOpen: true
						},
						el( RadioControl,
							{
								label: __( 'Bold Text Gradient', 'oep' ),
								options : [
									{ label: __( 'No' ), value: '' },
									{ label: __( 'Yes' ), value: '-light' },
									// { label: 'Light', value: '-light' },
									// { label: 'Dark',  value: '-dark' },
								],
								onChange: ( value ) => {
									props.setAttributes( { background: value } );
								},
								selected: props.attributes.background
							}
						),
					),
				),
				el(
					'div',
					{ key: 'inner-blocks', className: props.className + ' ' + props.attributes.background },
					el( InnerBlocks, {
						className: props.attributes.background ? '-gradiated' : '',
						template: BLOCKS_TEMPLATE,
						templateLock: false
					})
				),
			]
		},

		save: ( props ) => {
			return el( 'div', { className:props.attributes.background },
				el( InnerBlocks.Content )
			)
		},
})
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.element,
	window.wp.components,
	window.wp.i18n
) );