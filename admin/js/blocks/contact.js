( function( blocks, editor, element, components, i18n ) {
var el = element.createElement;
var InnerBlocks = editor.InnerBlocks;
var RadioControl = components.RadioControl;
var InspectorControls = editor.InspectorControls;

var BLOCKS_TEMPLATE = [
  ['core/heading', {placeholder: 'Contact Us', level: 1, align: 'center'}],
  ['core/paragraph', {placeholder: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. ', align: 'center'}]
]

blocks.registerBlockType('oep/contact', {
  title: 'Contact Form',
  category: 'oep-landing-page',
  attributes: {
    style: {
      type: 'string',
      default: 'normal',
    },
  },

  edit: (props) => {

    var style = props.attributes.style;

    return[
      el(
        InspectorControls,
        { key: 'inspector' },
        el(components.PanelBody, {
            title: i18n.__('Block Options'),
            className: 'block-options',
            initialOpen: true
          },
          el('p', {}, i18n.__('What style of contact form would you like to use?')),// Radio buttons
          el( RadioControl,
            {
              label: 'Style',
              //help: 'Some kind of description',
              options : [
                { label: 'Normal', value: 'normal' },
                { label: 'Small', value: 'small' },
              ],
              onChange: ( value ) => {
                props.setAttributes( { style: value } );
              },
              selected: props.attributes.style
            }
          ),
        ),
      ),
      el('div', {className: props.className},
        el(InnerBlocks, {
          className:'align-center',
          template: BLOCKS_TEMPLATE,
          templateLock: false
        }),
        el('div', {},
          el('input', {
            placeHolder: i18n.__('Name'),
            disabled: true,
            type: 'text'
          }),
          el('input', {
            placeHolder: i18n.__('Email Address'),
            disabled: true,
            type: 'email'
          }),
          el('textarea', {
            placeHolder: i18n.__('Comments and questions'),
            disabled: true
          }),
        )
      ),
    ]
  },
  save: (props) => {
    return el('div', {className:'align-center'
      },
      el(InnerBlocks.Content)
    )
  },
})
}(
  window.wp.blocks,
  window.wp.editor,
  window.wp.element,
  window.wp.components,
  window.wp.i18n
) );