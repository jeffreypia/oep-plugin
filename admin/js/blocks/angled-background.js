/**
 * Angled background block
 *
 * @since 1.2.0
 */
( function( blocks, editor, i18n, element, components ) {

	var el = element.createElement,
		registerBlockType = blocks.registerBlockType,
		__ = i18n.__,
		InspectorControls = editor.InspectorControls,
		TextControl = components.TextControl,
		SelectControl = components.SelectControl,
		RadioControl = components.RadioControl,
		CheckboxControl = components.CheckboxControl,
		MediaUpload = editor.MediaUpload,
		InnerBlocks = editor.InnerBlocks

	var BLOCKS_TEMPLATE = [
		[ 'oep/general-text-header' ]
	]

	registerBlockType( 'oep/angled-background', {

		// default stuff
		title: __( 'Angled Background', 'oep' ),
		icon: 'tablet',
		category: 'oep-common',

		/**
		 * Attributes
		 */
		attributes: {

			clientId: {
				type: 'string',
			},

			direction: {
				type: 'string',
				default: 'down',
			},

			backgroundGradient: {
				type: 'string',
				default: 'default',
			},

			backgroundImageID: {
				type: 'number',
			},

			backgroundImageThumb: {
				type: 'string',
			},

			topLeftAccent: {
				type: 'string',
			},

			topRightAccent: {
				type: 'string',
			},

			bottomLeftAccent: {
				type: 'string',
			},

			bottomRightAccent: {
				type: 'string',
			},

			leftIconAccent: {
				type: 'boolean',
			},

			rightIconAccent: {
				type: 'boolean',
			},

			hideBackgroundOverflow: {
				type: 'boolean',
				default: true,
			},

			noTopMargin: {
				type: 'boolean',
			},

			noBottomMargin: {
				type: 'boolean',
			},

			noTopPadding: {
				type: 'boolean',
			},

			noBottomPadding: {
				type: 'boolean',
			},

			topOffset: {
				type: 'number',
				default: 0,
			},

			bottomOffset: {
				type: 'number',
				default: 0,
			},
		},


		/**
		 * Block editor template
		 */
		edit: ( props ) => {

			var attrs = props.attributes

			// set clientId so we have something unique in template
			props.setAttributes({ clientId: props.clientId });

			/**
			 * Set the angled direction attribute
			 *
			 * @param {string}  value  up or down
			 */
			var setDirection = ( value ) => {
				props.setAttributes({ direction: value })
			}

			/**
			 * Set the background gradient attribute
			 *
			 * @param {string}  value  'default' (no change) or 'none' (just makes image 100% opacity)
			 */
			var setBackgroundGradient = ( value ) => {
				props.setAttributes({ backgroundGradient: value })
			}

			/**
			 * Set the editor thumbnail and attachment ID for background image
			 *
			 * @param {object}  image  attachment details
			 */
			var setBackgroundImage = ( image ) => {
				props.setAttributes({
					backgroundImageID: image.id,
					backgroundImageThumb: image.sizes.thumbnail.url,
				})
			}

			/**
			 * Set whether or not the background's accents are supposed to go
			 * outside the container
			 *
			 * @param {boolean}  value
			 */
			var setHideBackgroundOverflow = ( value ) => {
				props.setAttributes({ hideBackgroundOverflow: value })
			}

			/**
			 * Kill the background image attachment ID + thumbnail
			 */
			var removeBackgroundImage = () => {
				props.setAttributes({
					backgroundImageID: '',
					backgroundImageThumb: ''
				})
			}

			// direction control
			var directionControl = el( RadioControl, {
				label: __( 'Direction', 'oep' ),
				options: [
					{ label: __( 'Up' ), value: 'up' },
					{ label: __( 'Down' ), value: 'down' },
				],
				selected: attrs.direction,
				onChange: setDirection,
			})

			// gradient control
			var gradientControl = el( SelectControl, {
				label: __( 'Gradient', 'oep' ),
				options: [
					{ label: __( 'Default' ), value: 'default' },
					{ label: __( 'Dark Gray' ), value: 'dark-gray' },
					{ label: __( 'None' ), value: '' },
				],
				value: attrs.backgroundGradient,
				onChange: setBackgroundGradient,
			})

			// image buttons
			var doImageButton = ( obj ) => {

				return [
					el( components.Button, {
						key: 'button-set-image',
						className: 'button',
						onClick: obj.open
					}, __( 'Set Image', 'oep' ) ),

					el( 'span', { key: 'button-spacer' }, ' ' ),

					attrs.backgroundImageThumb &&
						el( components.Button, {
							key: 'button-remove-image',
							className: 'button',
							onClick: removeBackgroundImage,
						}, __( 'Remove', 'oep' ) ),
				]
			}

			// image control (wrapped in base control for spacing/label)
			var imageControl = el(
				components.BaseControl, {
					label: __( 'Background Image' ),
					help: __( 'Background overflow will always be hidden if image is set.' ),
				},

				attrs.backgroundImageThumb &&
					el( 'img', { src: attrs.backgroundImageThumb }),

				el( MediaUpload, {
					onSelect: setBackgroundImage,
					type: 'image',
					value: attrs.backgroundImageID,
					render: doImageButton,
				})
			)

			/**
			 * Get a gradient accent dropdown control
			 *
			 * @param {string}  attribute  block attribute to set
			 * @param {string}  label      label for field UI
			 */
			var accentControl = ( attribute, label ) => {
				return el( SelectControl, {
					label: label,
					options: [
						{ label: __( 'None' ),   value: '' },
						{ label: __( 'Orange' ), value: 'orange' },
						{ label: __( 'Yellow' ), value: 'gold' },
					],
					value: attrs[ attribute ],
					onChange: ( value ) => { // [] around prop makes it use var
						props.setAttributes({ [attribute]: value })
					},
				})
			}

			/**
			 * Get a number control
			 *
			 * For offset fields and stuff
			 *
			 * @param {*} attribute  block attribute to set
			 * @param {*} label      label for field UI
			 */
			var numberControl = ( attribute, label ) => {
				return el( TextControl, {
					label: label,
					type: 'number',
					value: attrs[ attribute ],
					onChange: ( value ) => {
						props.setAttributes({ [attribute]: Number( value ) })
					}
				})
			}

			/**
			 * Get a toggle/checkbox control
			 *
			 * @param {*} attribute  block attribute to set
			 * @param {*} label      label for field UI
			 */
			var checkboxControl = ( attribute, label ) => {
				return el( CheckboxControl, {
					label: label,
					checked: attrs[ attribute ] ? true : false,
					onChange: ( value ) => { // [] around prop makes it use var
						props.setAttributes({ [attribute]: value })
					}
				})
			}

			// background overflow toggle
			var overflowControl = el( CheckboxControl, {
				label: __( 'Hide Background Overflow', 'oep' ),
				checked: attrs.hideBackgroundOverflow ? true : false,
				onChange: setHideBackgroundOverflow,
			})

			// inspector controls container
			var inspectorControls = el( components.PanelBody, {
				title: __( 'Angled Background', 'oep' ),
				initialOpen: true,
			},
				directionControl,
				gradientControl,
				imageControl,
				accentControl( 'topLeftAccent', __( 'Top Left Accent', 'oep' ) ),
				accentControl( 'topRightAccent', __( 'Top Right Accent', 'oep' ) ),
				accentControl( 'bottomLeftAccent', __( 'Bottom Left Accent', 'oep' ) ),
				accentControl( 'bottomRightAccent', __( 'Bottom Right Accent', 'oep' ) ),
				checkboxControl( 'leftIconAccent', __( 'Left Background Accent', 'oep' ) ),
				checkboxControl( 'rightIconAccent', __( 'Right Background Accent', 'oep' ) ),
				overflowControl,
				checkboxControl( 'noTopMargin', __( 'Remove Top Margin', 'oep' ) ),
				checkboxControl( 'noBottomMargin', __( 'Remove Bottom Margin', 'oep' ) ),
				checkboxControl( 'noTopPadding', __( 'Remove Top Padding', 'oep' ) ),
				checkboxControl( 'noBottomPadding', __( 'Remove Bottom Padding', 'oep' ) ),
				numberControl( 'topOffset', __( 'Top Background Offset (px)', 'oep' ) ),
				numberControl( 'bottomOffset', __( 'Bottom Background Offset (px)', 'oep' ) ),
			)

			// configure extra block classes
			var blockClass = [
				'angled-background-' + attrs.direction,
				attrs.backgroundGradient ? 'angled-background-gradient-' + attrs.backgroundGradient : '',
				! attrs.backgroundGradient ? 'angled-background-no-gradient' : '',
				attrs.backgroundImageID ? 'angled-background-has-image' : '',
			].join( ' ' )

			// configure extra content wrapper classes
			var bgClass = [
				'angled-background',
			].join( ' ' )

			// return controls and editor template
			return [
				el( InspectorControls, { key: 'inspector' }, inspectorControls ),
				el( 'div', { key: 'innerblocks', className: blockClass },
					el( 'div', { className: bgClass },
						el( 'img', { src: attrs.backgroundImageThumb } )
					),
					el( 'div', { className: 'angled-background-content entry-content-dark' },
						el( InnerBlocks, { template: BLOCKS_TEMPLATE } )
					)
				)
			]

		}, // edit


		/**
		 * Post content template
		 */
		save: () => {
			return el( InnerBlocks.Content )
		}

	})
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components
));
