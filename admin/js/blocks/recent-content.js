( function (blocks, element, i18n, data, components, editor) {

  var el = element.createElement,
    registerBlockType = blocks.registerBlockType,
    postSelect = data.withSelect

  var __ = i18n.__

  var RangeControl = components.RangeControl;
  var RadioControl = components.RadioControl;
  var SelectControl = components.SelectControl;
  var TextControl = components.TextControl;
  var InspectorControls = editor.InspectorControls;

  registerBlockType('oep/recent', {
    title: 'Recent Content',
    icon: 'star-filled',
    category: 'oep-common',

    attributes: {
      title: {
        type: 'string',
        default: i18n.__('Recent Posts')
      },
      post_type: {
        type: 'string',
        default: 'post'
      },
      num_posts: {
        type: 'number',
        default: 4
      },
      postsToDisplay: {
        type: 'array,'
      },
      style: {
        type: 'string',
        default: 'horizontal',
      },
    },

    edit: postSelect(function (select, ownProps) {
      if (ownProps.selected_posts) {
        return
      }

      return {
        selected_posts: _.map(select('core').getEntityRecords('postType', ownProps.attributes.post_type, {
          per_page: ownProps.attributes.num_posts
        }), function (postToDisplay) {
          var current_post = select('core').getEntityRecord('postType', postToDisplay.type, postToDisplay.id)
          // console.log(current_post)
          if( current_post && current_post.meta == undefined ){
            Object.defineProperty(current_post, "meta", {value : [],
              writable : true,
              enumerable : true,
              configurable : true});
            current_post.meta['featured_media_rendered'] = "";
          }
          if (current_post && current_post.featured_media && undefined !== current_post.meta) {
            var current_post_image = select('core').getMedia(current_post.featured_media)
            if (current_post_image && undefined !== current_post_image) {
              // console.log(current_post_image.source_url)
              current_post.meta['featured_media_rendered'] = current_post_image.description.rendered
            }
            return current_post
          }
          return current_post
        })
      }
    })(function (props) {
      // console.log(props)

      if (!props.selected_posts || undefined === props.selected_posts) {
        return 'Loading...'
      }
      //is postsToDisplay set? return the rendered post
      var thumbHTML = ''
      if (props.selected_posts && undefined !== props.selected_posts && props.selected_posts.length > 0) {

        return [
          el(
            InspectorControls,
            { key: 'inspector' },
            el(components.PanelBody, {
                title: i18n.__('Block Options'),
                className: 'block-options',
                initialOpen: true
              },
              el( TextControl,
                {
                  label: 'Title',
                  help: 'What should the title be?',
                  onChange: ( value ) => {
                    props.setAttributes( { title: value } );
                  },
                }
              ),
              el( SelectControl,
                {
                  label: 'Post Types',
                  help: 'Choose which types of content to display',
                  options : [
                    { label: 'Articles and Press Releases', value: 'post' },
                    { label: 'Success Stories', value: 'oep_cpts_success' },
                    { label: 'Events', value: 'oep_cpts_event' },
                  ],
                  onChange: ( value ) => {
                    props.setAttributes( { post_type: value } );
                  },
                  value: props.attributes.post_type
                }
              ),
              el( RangeControl,
                {
                  label: 'Number of items',
                  help: 'How many items should be displayed?',
                  min: 2,
                  max: 8,
                  onChange: ( value ) => {
                    props.setAttributes( { num_posts: value } );
                  },
                  initialPosition: props.attributes.num_posts,
                  value: props.attributes.num_posts,
                }
              ),
              el( RadioControl,
                {
                  label: 'Block Style',
                  help: 'Is this block oriented vertically or horizontally?',
                  options : [
                    { label: 'Horizontal', value: 'horizontal' },
                    { label: 'Vertical', value: 'vertical' },
                    { label: 'Full Page Listing', value: 'full' },
                  ],
                  onChange: ( value ) => {
                    props.setAttributes( { style: value } );
                    if(value === "full"){
                      props.setAttributes( { num_posts: 12 } )
                    }
                  },
                  selected: props.attributes.style
                }
              )
            ),
          ),
          el('div', {className: 'rendered-posts' + ' ' + props.attributes.style + ' ' + props.attributes.post_type},
          el('h2', {}, props.attributes.title),
          _.map(props.selected_posts, function (selected_post) {
            if (undefined === selected_post) {
              return
            }
            if (undefined === selected_post.meta) {
              thumbHTML = '<div>Nothing to see here</div>'
            } else {
              thumbHTML = selected_post.meta['featured_media_rendered']
            }
            // console.log(selected_post)
            return el('div', {className: 'post'},
              el('div', {className: 'pills-container'},
                el('ul', {className: 'pills solid'},
                  el('li', {},
                    el('a', {href: '#'},
                      __("Sample Term", "oep") //filled out on front-end
                    )
                  )
                )
              ),
              el('figure', {
                  className: 'featured-image',
                  dangerouslySetInnerHTML: { //there's really good reason we're doing this, the content is already XSS checked because it's generated as a description when media is uploaded.
                    __html: thumbHTML
                  }
                }
              ),
              el('header', {className: 'post-header'},
                el('h4', {},
                  selected_post.title.raw
                )
              )
            )
          })
        )]
      }
      return null
    }),
    save: function () {
      return null
    },
  })

  }(
    window.wp.blocks,
    window.wp.element,
    window.wp.i18n,
    window.wp.data,
    window.wp.components,
    window.wp.editor,
  ))
