( function( blocks, element, components ) {

    var el = element.createElement,
    registerBlockType = blocks.registerBlockType,
    ServerSideRender = components.ServerSideRender;

    registerBlockType( 'oep/newsletter', {
        title: 'Newsletter Signup',
        icon: 'megaphone',
        category: 'oep-common',

        edit: function( props ) {

            return (
                el(ServerSideRender, {
                    block: "oep/newsletter",
                    attributes: props.attributes
                } )
            );
        },
        save: function () {
            return null
        },
    } );
}(
    window.wp.blocks,
    window.wp.element,
    window.wp.components,
) );