( function( blocks, editor, element, components, i18n ) {
    var el = element.createElement;
    var InnerBlocks = editor.InnerBlocks;
    var RadioControl = components.RadioControl;
    var InspectorControls = editor.InspectorControls;

    var BLOCKS_TEMPLATE = [
        ['core/heading',    {placeholder: 'Subtitle of Section', level: 4}],
        ['core/heading',    {placeholder: 'Headline', level: 1}],
        ['core/paragraph',  {placeholder: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. '}],
        ['core/button',     {text: 'Button Text'}]
    ]

    blocks.registerBlockType('oep/industry-cards', {
        title: 'Industry Cards',
        category: 'oep-landing-page',
        attributes: {
            background: {
                type: 'string',
                default: '',
            },
        },
        edit: (props) => {
            var background = props.attributes.background;

            return[
                el(
                    InspectorControls,
                    { key: 'inspector' },
                    el(components.PanelBody, {
                            title: i18n.__('Block Options'),
                            className: 'block-options',
                            initialOpen: true
                        },
                        el('p', {}, i18n.__('Should this text on a light or dark bg?')),
                        el( RadioControl,
                            {
                                label: 'Gradiated background',
                                //help: 'Some kind of description',
                                options : [
                                    { label: 'Light', value: '-light' },
                                    { label: 'Dark',  value: '-dark' },
                                ],
                                onChange: ( value ) => {
                                    props.setAttributes( { background: value } );
                                },
                                selected: props.attributes.background
                            }
                        ),
                    ),
                ),
                el(InnerBlocks, {
                    className: props.attributes.background ? '-gradiated' : '',
                    template: BLOCKS_TEMPLATE,
                    templateLock: false
                })
            ]
        },
        save: (props) => {
            return el('div', {className:props.attributes.background},
                el(InnerBlocks.Content)
            )
        },
})
}(
    window.wp.blocks,
    window.wp.editor,
    window.wp.element,
    window.wp.components,
    window.wp.i18n
) );