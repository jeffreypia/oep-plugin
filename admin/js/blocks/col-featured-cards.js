/**
 * Cost of Living: featured cards
 *
 * @since 1.2.0
 */
( function( blocks, editor, i18n, element ) {

	var el = element.createElement,
		registerBlockType = blocks.registerBlockType,
		__ = i18n.__,
		InnerBlocks = editor.InnerBlocks

	var templateDefaults = {
		titleLock: true,
		wrapperTag: 'li'
	}

	var BLOCKS_TEMPLATE = [

		['oep/industry-card', Object.assign( {}, templateDefaults, {
			title: __( 'Housing in {{city_to}} is {{{cost_housing}}}' ),
			className: 'col-housing',
		})],

		['oep/industry-card', Object.assign( {}, templateDefaults, {
			title: __( 'Groceries in {{city_to}} are {{{cost_groceries}}}' ),
			className: 'col-groceries',
		})],

		['oep/industry-card', Object.assign( {}, templateDefaults, {
			title: __( 'Transportation in {{city_to}} is {{{cost_transportation}}}' ),
			className: 'col-transportation',
		})],

		['oep/industry-card', Object.assign( {}, templateDefaults, {
			title: __( 'Healthcare in {{city_to}} is {{{cost_healthcare}}}' ),
			className: 'col-healthcare',
		})],

		['oep/industry-card', Object.assign( {}, templateDefaults, {
			title: __( 'If you earn {{{income_city_from}}}, your comparable income {{{income_adjusted}}}' ),
			className: 'col-income col-large',
		})],

		['oep/industry-card', Object.assign( {}, templateDefaults, {
			title: __( 'Overall, the cost of living in {{{col_city_to}}} than {{city_from}}.'),
			className: 'col-col-overall col-large',
		})],
	]

	registerBlockType( 'oep/col-featured-cards', {

		title: 'Cost of Living Featured Cards',
		icon: 'editor-table',
		category: 'oep-col',

		edit: ( props ) => {
			return ( el( 'ul', { className: props.className },

				el( InnerBlocks, {
					template: BLOCKS_TEMPLATE,
					templateLock: true,
				})

			));
		}, // edit

		save: ( props ) => {
			return ( el( 'ul', { className: 'full-width' },
				el( InnerBlocks.Content )
			))
		}
	})
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
));
