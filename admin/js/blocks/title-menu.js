( function (blocks, element, i18n, data, components, editor) {

  var el = element.createElement,
    registerBlockType = blocks.registerBlockType,
    postSelect = data.withSelect

  var __ = i18n.__

  var RichText = editor.RichText

  var RangeControl = components.RangeControl;
  var RadioControl = components.RadioControl;
  var SelectControl = components.SelectControl;
  var TextControl = components.TextControl;
  var InspectorControls = editor.InspectorControls;

  var Terms = [];
  var Taxonomy = "";

  function Title( props ) {
    return el( 'h1', {}, props.title );
  }

  var EditorPostTitle = wp.data.withSelect( function( select ) {
    return {
      title: select( 'core/editor' ).getEditedPostAttribute( 'title' )
    };
  } )( Title );

  registerBlockType('oep/title-menu', {
    title: 'Title with menu',
    icon: 'star-filled',
    category: 'oep-landing-page',

    attributes: {
      description: {
        type: 'string',
        format: 'string',
        selector: 'p',
      },
      taxonomy: {
        type: 'string',
        default: 'oep_taxonomies_competitiveness'
      },
      terms: {
        type: 'array',
      },
      termsToDisplay: {
        type: 'array',
      },
    },

    edit: postSelect(function (select, ownProps) {
      if (ownProps.selected_posts) {
        return
      }

      Terms = _.map(select('core').getEntityRecords('taxonomy', ownProps.attributes.taxonomy, {
        per_page: 99,
      }), function (term){
        return { label: term.name, value: term.id }
      })

      // console.log(Terms)

      return {
        selected_terms:  _.map(ownProps.attributes.terms, function (term) {

          // console.log(ownProps.attributes.terms);
          // console.log(ownProps.attributes.termsToDisplay);
          var current_term = select('core').getEntityRecord('taxonomy', term.taxonomy, term.term_id)
          if (current_term) {
            return { label: current_term.name, value: current_term.id }
          }
        })
      }
    })(function (props) {
      // console.log(props)

      if (!props.selected_terms || undefined === props.selected_terms) {
        return 'Loading...'
      }

      if (props.selected_terms && undefined !== props.selected_terms && props.selected_terms.length > 0) {

        return [
          el(
            InspectorControls,
            { key: 'inspector' },
            el(components.PanelBody, {
                title: i18n.__('Block Options'),
                className: 'block-options',
                initialOpen: true
              },
              el( SelectControl,
                {
                  label: 'Taxonomy',
                  help: 'Choose a taxonomy to pick terms from',
                  options : [
                    { label: 'Competitiveness', value: 'oep_taxonomies_competitiveness' },
                    { label: 'Quality of Life', value: 'oep_taxonomies_qol' },
                    { label: 'Industries', value: 'oep_taxonomies_industries' },
                    { label: 'Programs', value: 'oep_taxonomies_teams' },
                  ],
                  onChange: ( value ) => {
                    props.setAttributes( { taxonomy: value } );
                  },
                  value: props.attributes.taxonomy
                }
              ),
              el( SelectControl,
                {
                  label: 'Terms',
                  multiple: true,
                  help: 'Choose your desired terms',
                  options : Terms,
                  onChange: ( value ) => {
                    /*props.setAttributes( { terms: _.groupBy(_.union(props.attributes.terms,_.map(value, function (id) {
                        return {term_id: id, taxonomy: props.attributes.taxonomy}
                      })), 'term_id') } );*/
                    props.setAttributes( {terms: _.uniq(_.union(props.attributes.terms, _.map(value, function (id) {
                        return {term_id: id, taxonomy: props.attributes.taxonomy}
                      })), false, _.property('term_id'))} );
                    props.setAttributes( { termsToDisplay: value } );
                  },
                  selected: props.attributes.termsToDisplay,
                  value: props.attributes.termsToDisplay
                }
              ),
            ),
          ),
          el('div', {className: props.className},
            el( EditorPostTitle ),
            el(RichText, {
              tagName: 'p',
              className: 'detail',
              inline: false,
              placeholder: i18n.__('Page description', 'oep'),
              value: props.attributes.description,
              onChange: function (value) {
                props.setAttributes({description: value})
              },
            }),
            el('ul', {className: 'rendered-terms'},
              _.map(props.selected_terms, function (selected_term) {
                if (undefined === selected_term) {
                  return
                }
                // console.log(selected_post)
                return el('li', {className: 'term'},
                  el('span', {},
                    selected_term.label,
                  )
                )
              })
            ),
          ),
        ]
      }
      return [
        el(
          InspectorControls,
          { key: 'inspector' },
          el(components.PanelBody, {
              title: i18n.__('Block Options'),
              className: 'block-options',
              initialOpen: true
            },
            el( SelectControl,
              {
                label: 'Taxonomy',
                help: 'Choose a taxonomy to pick terms from',
                options : [
                  { label: 'Competitiveness', value: 'oep_taxonomies_competitiveness' },
                  { label: 'Quality of Life', value: 'oep_taxonomies_qol' },
                  { label: 'Industries', value: 'oep_taxonomies_industries' },
                  { label: 'Programs', value: 'oep_taxonomies_teams' },
                ],
                onChange: ( value ) => {
                  props.setAttributes( { taxonomy: value } );
                },
                value: props.attributes.taxonomy
              }
            ),
            el( SelectControl,
              {
                label: 'Terms',
                multiple: true,
                help: 'Choose your desired terms',
                options : Terms,
                onChange: ( value ) => {
                  props.setAttributes( {terms: _.uniq(_.union(props.attributes.terms, _.map(value, function (id) {
                      return {term_id: id, taxonomy: props.attributes.taxonomy}
                    })), false, _.property('term_id'))} );
                  props.setAttributes( { termsToDisplay: value } );
                },
                selected: props.attributes.termsToDisplay,
                value: props.attributes.termsToDisplay
              }
            ),
          ),
        ),
        el('div', {className: props.className},
          el( EditorPostTitle ),
          el(RichText, {
            tagName: 'p',
            className: 'detail',
            inline: false,
            placeholder: i18n.__('Page description', 'oep'),
            value: props.attributes.description,
            onChange: function (value) {
              props.setAttributes({description: value})
            },
          }),
          el('ul', {className: 'rendered-terms'},
            el('li', {}, "Enter some terms from the right to begin"),
          ),
        ),
      ]
    }),
    save: function () {
      return null
    },
  })

}(
  window.wp.blocks,
  window.wp.element,
  window.wp.i18n,
  window.wp.data,
  window.wp.components,
  window.wp.editor,
))
