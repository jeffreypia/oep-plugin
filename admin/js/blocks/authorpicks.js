(function (blocks, element, i18n, data, components) {

  var el = element.createElement,
    registerBlockType = blocks.registerBlockType,
    postSelect = data.withSelect

  var PostSelectButton = components.PostSelectButton
  var __ = i18n.__

  registerBlockType('oep/authorpicks', {
    title: 'Author Picks',
    icon: 'star-filled',
    category: 'oep-common',

    attributes: {
      postsToDisplay: {
        type: 'array,'
      }
    },

    edit: postSelect(function (select, ownProps) {
      // console.log(ownProps)
      if (ownProps.selected_posts) {
        return
      }
      return {
        selected_posts: _.map(ownProps.attributes.postsToDisplay, function (postToDisplay) {
          var current_post = select('core').getEntityRecord('postType', postToDisplay.type, postToDisplay.id)
          // console.log(current_post)
          if( current_post && current_post.meta == undefined ){
            Object.defineProperty(current_post, "meta", {value : [],
              writable : true,
              enumerable : true,
              configurable : true});
            current_post.meta['featured_media_rendered'] = "";
          }
          if (current_post && current_post.featured_media && undefined !== current_post.meta) {
            var current_post_image = select('core').getMedia(current_post.featured_media)
            if (current_post_image && undefined !== current_post_image) {
              // console.log(current_post_image.source_url)
              current_post.meta['featured_media_rendered'] = current_post_image.description.rendered
            }
            return current_post
          }
        })
      }
    })(function (props) {
      // console.log(props)

      if (!props.selected_posts || undefined === props.selected_posts) {
        return 'Loading...'
      }
      //is postsToDisplay set? return the rendered post
      var thumbHTML = ''
      if (props.selected_posts && undefined !== props.selected_posts && props.selected_posts.length > 0) {

        return el('div', {className: 'rendered-posts'},
          el('h4', {}, __("Author's Pick", "oep")),
          _.map(props.selected_posts, function (selected_post) {
            if (undefined === selected_post) {
              return
            }
            if (undefined === selected_post.meta) {
              thumbHTML = '<div>Nothing to see here</div>'
            } else {
              thumbHTML = selected_post.meta['featured_media_rendered']
            }
            // console.log(selected_post)
            return el('div', {className: 'post'},
              el('div', {className: 'pills-container'},
                el('ul', {className: 'pills solid'},
                  el('li', {},
                    el('a', {href: '#'},
                      __("Sample Term", "oep") //filled out on front-end
                    )
                  )
                )
              ),
              el('figure', {
                  className: 'featured-image',
                  dangerouslySetInnerHTML: { //there's really good reason we're doing this, the content is already XSS checked because it's generated as a description when media is uploaded.
                    __html: thumbHTML
                  }
                }
              ),
              el('header', {className: 'post-header'},
                el('h4', {},
                  selected_post.title.raw
                )
              )
            )
          }),
          el('div', {className: 'button-holder'},
            el(
              PostSelectButton, {
                value: props.attributes.postsToDisplay,
                onSelect: function (posts) {
                  props.setAttributes(
                    {
                      postsToDisplay: _.map(posts, function (post) {
                        return post
                      })
                    })
                },
                postType: ['post','oep_cpts_video'],
                maxPosts: 2,
                btnProps: {isLarge: true},
              }, __('Select Posts')
            )
          )
        )
      }
      return el(
        PostSelectButton, {
          value: props.attributes.postsToDisplay,
          onSelect: function (posts) {
            props.setAttributes(
              {
                postsToDisplay: _.map(posts, function (post) {
                  return post
                })
              })
          },
          postType: ['post','oep_cpts_video'],
          maxPosts: 2,
          btnProps: {isLarge: true},
        }, __('Select Posts')
      )
    }),
    save: function () {
      return null
    },
  })
}(
  window.wp.blocks,
  window.wp.element,
  window.wp.i18n,
  window.wp.data,
  window.hm.components,
))