/**
 * Featured posts for a taxonomy
 *
 * @since 1.2.0
 */
( function( blocks, editor, i18n, element, components, data ) {

	var el = element.createElement,
		registerBlockType = blocks.registerBlockType,
		__ = i18n.__,
		withSelect = data.withSelect,
		InspectorControls = editor.InspectorControls,
		ServerSideRender = components.ServerSideRender,
		CheckboxControl = components.CheckboxControl

	registerBlockType( 'oep/taxonomy-featured-posts', {

		// default stuff
		title: __( 'Taxonomy Featured Posts', 'oep' ),
		description: __( 'Show a grid of featured posts for a taxonomy.', 'oep' ),
		icon: 'tablet',
		category: 'oep-common',
		keywords: [
			__( 'taxonomy' ),
			__( 'term' ),
			__( 'featured' ),
			__( 'post' ),
			__( 'stories' )
		],

		/**
		 * Attributes
		 *
		 * Remember to update register_block_type()'s attributes if changing
		 */
		attributes: {

			taxonomy: {
				type: 'string',
				default: 'oep_taxonomies_competitiveness'
			},

			terms: {
				type: 'array',
			},

			featuredTag: {
				type: 'boolean',
				default: true,
			},

			blogs: {
				type: 'array',
			}
		},


		/**
		 * Prep data for block editor template
		 *
		 * It looks we need to query data within the context of a block. We're
		 * going to set the "terms", then "pass it" to the anonymous function
		 * that we normally have for .edit.
		 *
		 * @param  {function}  select  the thing withSelect uses to get stuff
		 * @param  {object}    props   block properties
		 * @return {object}            additional properties for "props" in the next function
		 */
		edit: withSelect( function( select, props ) {

			return {
				terms: oepEditor.getTaxonomyTerms( select, props )
			}
		})


		/**
		 * Actual editor template
		 *
		 * @param {object}  props  block properties
		 */
		( function( props ) {

			var attrs = props.attributes

			/**
			 * Handle "featured" tag check toggle
			 *
			 * @param {boolean}  value
			 * @since 1.2.0
			 */
			var setFeaturedTag = ( value ) => {
				props.setAttributes({ featuredTag: value })
			}

			// "featured" tag checkbox
			var featuredTagControl = el( CheckboxControl, {
				label: __( 'Check For "Featured" Tag' ),
				checked: attrs.featuredTag ? true : false,
				onChange: setFeaturedTag,
			})

			// inspector controls container
			var inspectorControls = el( components.PanelBody, {
				title: __( 'Featured Taxonomy Posts', 'oep' ),
				initialOpen: true,
			},
				oepEditor.getTaxSelect( props ),
				oepEditor.getTermSelect( props ),
				featuredTagControl,
				oepEditor.getBlogSelect( props )
			)

			// return controls and editor template
			return [
				el( InspectorControls, { key: 'inspector' }, inspectorControls ),
				el( ServerSideRender, {
					key: 'rendered-block',
					block: 'oep/taxonomy-featured-posts',
					attributes: props.attributes,
				})
			]

		}), // edit


		/**
		 * Post content template
		 */
		save: () => {
			return null
		}

	})
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window.wp.data,
));
