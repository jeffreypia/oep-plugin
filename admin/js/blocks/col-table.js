/**
 * Cost of Living Table block
 *
 * @since 1.2.0
 */
( function( blocks, i18n, element ) {

	var el = element.createElement,
		registerBlockType = blocks.registerBlockType,
		__ = i18n.__

	// header/labels setup
	var header = []
	var rows = []

	/**
	 * Add header cell to table header
	 *
	 * @param {string} label  table head cell contents
	 */
	var addHeaderCell = ( label ) => {
		header.push( el( 'th', {}, label ) )
	}

	/**
	 * Set up table header
	 */
	var setHeader = () => {
		[
			__( 'Comparison Item', 'oep' ),
			__( 'Cost in {{city_to}}', 'oep' ),
			__( 'Cost in {{city_from}}', 'oep' ),
			__( 'National Average', 'oep' )
		].forEach( addHeaderCell )
	}

	/**
	 * Add a row to the table body
	 *
	 * @param
	 */
	var addRow = ( metric ) => {

		rows.push( el( 'tr', { 'data-col-metric': metric.id },
			el( 'td', {}, metric.label ),
			el( 'td', {
				'data-city' : 'to',
				'data-label' : '{{city_to}}',
			}, '' ),
			el( 'td', {
				'data-city' : 'from',
				'data-label' : '{{city_from}}',
			}, '' ),
			el( 'td', {
				'data-city' : 'average',
				'data-label' : __( 'National Average', 'oep' ),
			}, '' ),
		))
	}

	/**
	 * Set up table rows
	 */
	var setRows = () => {
		[
			{
				label: __( 'Home Price', 'oep' ),
				id: 35,
			},
			{
				label: __( 'Apartment Rent', 'oep' ),
				id: 34,
			},
			{
				label: __( 'Total Energy', 'oep' ),
				id: 36,
			},
			// {
			// 	label: __( 'Groceries', 'oep' ),
			// 	id: ,
			// },
			{
				label: __( 'Gasoline', 'oep' ),
				id: 30,
			},
			{
				label: __( 'Phone', 'oep' ),
				id: 37,
			},
			{
				label: __( 'Doctor Visit', 'oep' ),
				id: 41,
			},
			{
				label: __( 'Optometrist Visit', 'oep' ),
				id: 40,
			},
			{
				label: __( 'Coffee', 'oep' ),
				id: 22,
			},
			{
				label: __( 'Man\'s Dress Shirt', 'oep' ),
				id: 53,
			},
			{
				label: __( 'Women\'s Slacks', 'oep' ),
				id: 55,
			},
			{
				label: __( 'Dry Cleaning', 'oep' ),
				id: 52,
			},
			{
				label: __( 'Beer', 'oep' ),
				id: 62,
			},
			{
				label: __( 'Wine', 'oep' ),
				id: 63,
			},
			{
				label: __( 'Veterinarian Services', 'oep' ),
				id: 61,
			},
		].forEach( addRow )
	}

	// spin everything up
	setHeader();
	setRows();

	// block setup
	registerBlockType( 'oep/col-table', {

		title: __( 'Cost of Living Comparison Table', 'oep' ),
		icon: 'editor-table',
		category: 'oep-col',


		/**
		 * Block editor template (table)
		 */
		edit: ( props ) => {
			return ( el( 'table', { className: props.className },

				el( 'thead', {},
					el( 'tr', {}, header )
				),

				el( 'tbody', {}, rows )
			))
		}, // edit


		/**
		 * Post content template
		 */
		save: ( props ) => {
			return ( el( 'table', { className: props.className },

				el( 'thead', {},
					el( 'tr', {}, header )
				),

				el( 'tbody', {}, rows )
			))
		}

	}) // register type

}(
	window.wp.blocks,
	window.wp.i18n,
	window.wp.element,
));
