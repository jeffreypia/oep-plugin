/**
 * "Section" grouper block
 *
 * @since 1.2.0
 */
( function( blocks, editor, i18n, element ) {

	var el = element.createElement,
		registerBlockType = blocks.registerBlockType,
		__ = i18n.__,
		InnerBlocks = editor.InnerBlocks

	var BLOCKS_TEMPLATE = [
		[ 'oep/general-text-header' ]
	]

	registerBlockType( 'oep/section', {

		// default stuff
		title: __( 'Section', 'oep' ),
		description: __( 'Group some blocks together with reduced spacing.', 'oep' ),
		icon: 'editor-table',
		category: 'oep-common',

		/**
		 * Block editor template
		 */
		edit: () => {
			return (
				el( 'section', { className: 'full-width' },
					el( InnerBlocks, { template: BLOCKS_TEMPLATE } )
				)
			)
		},

		/**
		 * Post content template
		 */
		save: () => {
			return el( 'section', { className: 'full-width' },
				el( InnerBlocks.Content )
			)
		}

	})
}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
));
