( function( blocks, editor, element, components, i18n ) {
var el = element.createElement;
var InnerBlocks = editor.InnerBlocks;

var BLOCKS_TEMPLATE = [
  ['core/paragraph', {placeholder: 'Excerpt...', className: 'detail'},
  ]]

blocks.registerBlockType('oep/excerpt', {
  title: 'Excerpt',
  category: 'oep-common',

  edit: (props) => {
    return[
      el(InnerBlocks, {
        template: BLOCKS_TEMPLATE,
        templateLock: false
      })
    ]
  },
  save: (props) => {
    return el(InnerBlocks.Content)
  },
})
}(
  window.wp.blocks,
  window.wp.editor,
  window.wp.element,
  window.wp.components,
  window.wp.i18n
) );