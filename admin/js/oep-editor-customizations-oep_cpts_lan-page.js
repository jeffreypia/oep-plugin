wp.domReady(function () {
  var allowedBlocks = [
    'core/cover',
    'core/paragraph',
    'core/heading',
    'core/image',
    'core/html',
    'core/freeform',
    'core/audio',
    'core/button',
    'core/column',
    'core/columns',
    'core/embed',
    'core-embed/youtube',
    'core-embed/vimeo',
    'core-embed/twitter',
    'core/list',
    'core/latest-posts',
    'core/video',
    'core/block',
    'core/template',
    'oep/excerpt',
    'oep/hero',
    'oep/slider',
    'oep/hero-cta',
    'oep/recent',
    'oep/contact',
    'oep/media-text',
    'oep/video-text',
    'oep/event-text',
    'oep/headline-text',
    'oep/text-list',
    'oep/column-sidebar',
    'oep/stories',
    'oep/title-menu',
    'acf/oep-event',
    'acf/oep-newsletter-block',
    'acf/oep-social-cards-block',
    'acf/oep-stats-block',
    'acf/oep-success-story-block',
    'acf/oep-hero-slider-block',
    'oep/authorpicks',
    'oep/general-text-header',
    'acf/oep-industry-card-block',
    'oep/industry-card',
    'editor-blocks/wrapper',    // Editor Blocks Wrapper block
    'acf/oep-col-form',         // ACF via OEP\Common\Cost_Of_Living
    'oep/col-featured-cards',
    'oep/col-table',
    'oep/angled-background',
    'oep/archive-loop',
    'oep/section',
    'oep/taxonomy-featured-posts',
    'acf/oep-trending-stories',
    'acf/oep-cta-option-block',
    'acf/oep-company-cards-block',
    'acf/oep-welcome-collage-block',
    'acf/oep-industry-cards-block',
    'acf/oep-neighborhoods-map-block',
  ]

  wp.blocks.unregisterBlockStyle( 'core/button', 'squared' );
  wp.blocks.unregisterBlockStyle( 'core/button', 'default' );
  wp.blocks.unregisterBlockStyle( 'core/button', 'outline' );
  wp.blocks.unregisterBlockStyle( 'core/button', 'rounded' );
  wp.blocks.getBlockTypes().forEach(function (blockType) {
    if (allowedBlocks.indexOf(blockType.name) === -1) {
      wp.blocks.unregisterBlockType(blockType.name)
    }
  })
})
