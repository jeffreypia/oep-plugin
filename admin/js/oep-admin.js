(function ($) {
	'use strict'

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	 function loadHubspotForms() {
	 	var $hsCounter = 0;
	 	
	 	$('[data-hs-portal-id]').each(function(){
	 		$hsCounter++;
	 		
	 		var $this = $(this);
	 		var portalID = $this.data('hs-portal-id'),
	 			formID = $this.data('hs-form-id');

	 		// Generate unique class to target
	 		$this.addClass('hubspot-form-' + $hsCounter);

	 		// Render form
	 		hbspt.forms.create({
	 			portalId: portalID,
	 			formId: formID,
	 			target: '.hubspot-form-' + $hsCounter,
	 		});
	 	});
	 }

	 $( window ).load(function() {
	 	loadHubspotForms();
	 });

	$(function () {
		if (wp.data) {
			var fullScreenActive = wp.data.select('core/edit-post').isFeatureActive('fullscreenMode')
			if (!fullScreenActive) {
				wp.data.dispatch('core/edit-post').toggleFeature('fullscreenMode')
			}
		}

		var defaultset = false;
		var targetNode = document.body;
		var config = { attributes: true, childList: true, subtree: true };
		var callback = function(mutationsList, observer) {
			for(var mutation of mutationsList) {
				// The trace unfortunatelly doesn't contain the function
				// "addSomeElement", which I am trying to receive at this point.
				if(defaultset == false && mutation.target.className == "editor-post-taxonomies__hierarchical-terms-list"){
					if (undefined !== mutation.target.attributes[3] && mutation.target.attributes[3].value == "Blog Types") {
						observer.disconnect();
						setDefaultOption();
					}
				}
			}
		};

		var observer = new MutationObserver(callback);
		observer.observe(targetNode, config);

		//set default blog type on posts to article
		function setDefaultOption () {
			if(defaultset){
				return;
			}
			var $radio_group = $('input[name="radio_tax_input-oep_taxonomies_blog-types"]');
			if($radio_group.length > 0 && ! defaultset) {
				var $selected = false;
				$radio_group.each(function (index, radioInput) {
					if($(radioInput).checked == true || $(radioInput).attr('checked')){
						$selected = true;
					}
				});
				if($selected == false){
					$('input[value=20]').click();
				 // $('input[value=20]').prop('checked', true); //@TODO this is really really bad practice, need to set this value with a localized js variable
					defaultset = true;
				}
			}
		};
	})

})(jQuery)
