wp.domReady(function () {
  var allowedBlocks = [
    'core/cover',
    'core/paragraph',
    'core/table',
    'core/heading',
    'core/image',
    'core/file',
    'core/gallery',
    'core/reusableBlock',
    'core/html',
    'core/freeform',
    'core/audio',
    'core/button',
    'core/column',
    'core/columns',
    'core/embed',
    'core-embed/youtube',
    'core-embed/vimeo',
    'core-embed/twitter',
    'core/list',
    'core/latest-posts',
    'core/video',
    'core/quote',
    'core/pullquote',
    'core/block',
    'core/template',
    'oep/excerpt',
    'oep/authorpicks',
    'oep/recommendedstories',
    'acf/oep-newsletter-block',
    'acf/oep-social-cards-block',
    'acf/oep-stats-block',
    'acf/oep-success-story-block',
    'blockgallery/carousel',
    'blockgallery/masonry',
    'blockgallery/stacked',
    'oep/general-text-header',
    'oep/archive-loop',
    'oep/section',
    'acf/oep-industry-card-block',
    'acf/oep-trending-stories',
    'acf/oep-cta-option-block',
    'acf/oep-company-cards-block',
    'acf/oep-industry-cards-block',
    'acf/oep-neighborhoods-map-block',
  ]

  wp.blocks.unregisterBlockStyle( 'core/button', 'squared' );
  wp.blocks.unregisterBlockStyle( 'core/button', 'default' );
  wp.blocks.unregisterBlockStyle( 'core/button', 'outline' );
  wp.blocks.unregisterBlockStyle( 'core/button', 'rounded' );
  wp.blocks.getBlockTypes().forEach(function (blockType) {
    if (allowedBlocks.indexOf(blockType.name) === -1) {
      wp.blocks.unregisterBlockType(blockType.name)
    }
  })
})
