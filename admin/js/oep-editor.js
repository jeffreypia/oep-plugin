/**
 * Misc OEP helpers for block editor
 *
 * @todo    clean up unused props!
 *
 * @since   1.2.0
 * @package oep
 */

( function( blocks, editor, data, i18n, element, components ) {

	var el = element.createElement,
	    __ = i18n.__,
	    SelectControl = components.SelectControl


	/**
	 * Get multi<select> with blogs
	 *
	 * Intended for passing into the multisite indexing plugin query
	 *
	 * @return {array}
	 * @since  1.2.0
	 */
	oepEditor.getBlogSelect = ( props ) => {

		return el( SelectControl, {
			label: __( 'Pull From Site(s)' ),
			help: __( 'Select site(s) to get content from (defaults to current)' ),
			multiple: true,
			options: _.map( oepEditor.sites, ( blog ) => {
				return { label: blog.label, value: blog.value }
			}),
			onChange: ( value ) => {
				props.setAttributes({ blogs: value });
			},
			selected: props.attributes.blogs,
			value: props.attributes.blogs,
		})
	}


	/**
	 * Get the terms from an existing taxonomy name prop
	 *
	 * @param  {function}  select  the thing withSelect uses to get stuff
	 * @param  {object}    props   block properties with "taxonomy" attribute
	 * @return {array}             taxonomy term entity record objects
	 *
	 * @since 1.2.0
	 */
	oepEditor.getTaxonomyTerms = ( select, props ) => {

		return select( 'core' ).getEntityRecords(
			'taxonomy',
			props.attributes.taxonomy,
			{
				per_page: 99
			}
		)
	}


	/**
	 * Get options for the taxonomies we're supporting
	 *
	 * @return {array}
	 * @since   1.2.0
	 */
	oepEditor.getTaxOptions = () => {
		return [
			{ label: __( 'Competitiveness' ), value: 'oep_taxonomies_competitiveness' },
			{ label: __( 'Quality of Life' ), value: 'oep_taxonomies_qol' },
			{ label: __( 'Industries' ),      value: 'oep_taxonomies_industries' },
			{ label: __( 'Regions' ),         value: 'oep_taxonomies_regions' },
			{ label: __( 'Activity' ),        value: 'oep_taxonomies_activity' },
			{ label: __( 'Teams' ),           value: 'oep_taxonomies_teams' },
			{ label: __( 'Departments' ),     value: 'oep_taxonomies_departments' },
			{ label: __( 'Memberships' ),     value: 'oep_taxonomies_memberships' },
			{ label: __( 'Ideas' ),           value: 'oep_taxonomies_ideas' },
			{ label: __( 'Values' ),          value: 'oep_taxonomies_values' },
		]
	}


	/**
	 * Get a <select> with taxonomies we want to allow grabbing content from
	 *
	 * @param  {object}         props  block props
	 * @return {SelectControl}
	 * @since  1.2.0
	 */
	oepEditor.getTaxSelect = ( props ) => {

		return el( SelectControl, {
			label: __( 'Taxonomy' ),
			help: __( 'Select a taxonomy to pick terms from' ),
			options: oepEditor.getTaxOptions(),
			onChange: ( value ) => {
				props.setAttributes({ taxonomy: value });
			},
			selected: props.attributes.taxonomy,
			value: props.attributes.taxonomy,
		})
	}


	/**
	 * Get a <select> with terms for the current "taxonomy" attribute
	 *
	 * Expects a "terms" prop to already exist.
	 *
	 * @since 1.2.0
	 */
	oepEditor.getTermSelect = ( props ) => {

		return el( SelectControl, {
			label: __( 'Terms' ),
			help: __( 'Select terms to pull content from' ),
			multiple: true,
			options: getTermsFormattedForSelect( props.terms ),
			onChange: ( value ) => {
				props.setAttributes({ terms: value })
			},
			selected: props.attributes.terms,
			value: props.attributes.terms,
		})
	}


	/**
	 * Parse term entity records into <select>able data
	 *
	 * @param  {array}  terms  array of term entity record objects
	 * @return {array}         new array with just label/value for each term
	 *
	 * @since  1.2.0
	 */
	function getTermsFormattedForSelect( terms ) {
		return _.map( terms, setTermOption )
	}


	/**
	 * Format a term for <select>
	 *
	 * @param  {object}  term  term entity object
	 * @return {object}        object with label and value
	 *
	 * @since  1.2.0
	 */
	function setTermOption( term ) {

		return {
			label: term.name,
			value: term.slug,
		}
	}

}(
	window.wp.blocks,
	window.wp.editor,
	window.wp.data,
	window.wp.i18n,
	window.wp.element,
	window.wp.components
));
