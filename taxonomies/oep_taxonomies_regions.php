<?php
/**
 * Created by PhpStorm.
 * User: ryansmith
 * Date: 2019-03-21
 * Time: 12:07
 */

namespace OEP\Taxonomies;


/**
 * Class Regions
 *
 * @package OEP\Taxonomies
 */
class Regions {

	/**
	 * Get args for taxonomy
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels  = array(
			'name'                       => _x( 'Regions', 'Taxonomy General Name', 'oep' ),
			'singular_name'              => _x( 'Region', 'Taxonomy Singular Name', 'oep' ),
			'menu_name'                  => __( 'Regions', 'oep' ),
			'all_items'                  => __( 'All Regions', 'oep' ),
			'parent_item'                => __( 'Parent Region', 'oep' ),
			'parent_item_colon'          => __( 'Parent Region:', 'oep' ),
			'new_item_name'              => __( 'New Region', 'oep' ),
			'add_new_item'               => __( 'Add New Region', 'oep' ),
			'edit_item'                  => __( 'Edit Region', 'oep' ),
			'update_item'                => __( 'Update Region', 'oep' ),
			'view_item'                  => __( 'View Region', 'oep' ),
			'separate_items_with_commas' => __( 'Separate regions with commas', 'oep' ),
			'add_or_remove_items'        => __( 'Add or remove regions', 'oep' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'oep' ),
			'popular_items'              => __( 'Popular Regions', 'oep' ),
			'search_items'               => __( 'Search Regions', 'oep' ),
			'not_found'                  => __( 'Not Found', 'oep' ),
			'no_terms'                   => __( 'No Regions', 'oep' ),
			'items_list'                 => __( 'Regions list', 'oep' ),
			'items_list_navigation'      => __( 'Regions list navigation', 'oep' ),
		);
		$rewrite = array(
			'slug'         => 'regions',
			'with_front'   => false,
			'hierarchical' => true,
		);
		$args    = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => false,
			'show_in_menu'      => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
			'show_in_rest'      => true,
			'rewrite'           => $rewrite,
		);

		return $args;
	}
}