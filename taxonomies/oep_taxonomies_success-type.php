<?php
/**
 * Created by PhpStorm.
 * User: leisyvidal
 * Date: 2019-08-27
 * Time: 2:30
 */

namespace OEP\Taxonomies;


/**
 * Class Story
 *
 * @package OEP\Taxonomies
 */
class SuccessType{

	/**
	 * Get args for taxonomy
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels  = array(
			'name'                       => _x( 'Success Type', 'Taxonomy General Name', 'oep' ),
			'singular_name'              => _x( 'Success Type', 'Taxonomy Singular Name', 'oep' ),
			'menu_name'                  => __( 'Success Types', 'oep' ),
			'all_items'                  => __( 'All Success Types', 'oep' ),
			'parent_item'                => __( 'Parent Success Type', 'oep' ),
			'parent_item_colon'          => __( 'Parent Success Type:', 'oep' ),
			'new_item_name'              => __( 'New Success Type', 'oep' ),
			'add_new_item'               => __( 'Add New Success Type', 'oep' ),
			'edit_item'                  => __( 'Edit Success Type', 'oep' ),
			'update_item'                => __( 'Update Success Type', 'oep' ),
			'view_item'                  => __( 'View Success Type', 'oep' ),
			'separate_items_with_commas' => __( 'Separate success types with commas', 'oep' ),
			'add_or_remove_items'        => __( 'Add or remove success type', 'oep' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'oep' ),
			'popular_items'              => __( 'Popular Success Types', 'oep' ),
			'search_items'               => __( 'Search Success Types', 'oep' ),
			'not_found'                  => __( 'Not Found', 'oep' ),
			'no_terms'                   => __( 'No Success Types', 'oep' ),
			'items_list'                 => __( 'Success Types list', 'oep' ),
			'items_list_navigation'      => __( 'Success Types list navigation', 'oep' ),
		);
		$rewrite = array(
			'slug'         => 'success-types',
			'with_front'   => false,
			'hierarchical' => true,
		);
		$args    = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_menu'      => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
			'show_in_rest'      => true,
			'rewrite'           => $rewrite,
		);

		return $args;
	}


	/**
	 * add configuration page for Taxonomy
	 *
	 * @since 0.2.0
	 */
	public static function add_options_page() {
		if ( ! function_exists( 'acf_add_options_page' ) ) {
			return;
		}

		acf_add_options_page( array(
			'post_id'     => OEP_SUCCESS_TYPE_KEY,
			'page_title'  => __( 'Success Types', 'oep' ),
			'menu_title'  => __( 'Success Types', 'oep' ),
			'menu_slug'   => OEP_SUCCESS_TYPE_KEY,
			'capability'  => 'edit_posts',
			'icon_url'    => 'dashicons-category',
			'position'    => 30.1,
		) );
	}

	/**
	 * Filter post types this taxonomy should appear on
	 *
	 * @param $post_types array of post types
	 *
	 * @return array of post types
	 * @since 0.2.0
	 */

	public static function set_post_types( $post_types ) {
		$exclude = [ OEP_PROFILE_KEY, OEP_LANDING_PAGE_KEY, OEP_EVENT_KEY, OEP_COMPANY_KEY, OEP_DOWNLOAD_KEY, OEP_HOOD_KEY, OEP_PODCAST_KEY ];
		$post_types = array_diff( $post_types, $exclude );

		return $post_types;
	}


	/**
	 * SEO options for taxonomy listings / archives, like meta descriptions, robots and ograph stuff
	 */

	public static function seo_settings() {
		//check to see if we're on this taxonomy listing page and not a term listing
		if(get_queried_object() && get_queried_object()->name == "oep_taxonomies_success_type"){
			add_filter('wpseo_robots', [__CLASS__, "robots"], 99 , 1 );
			add_filter('wpseo_opengraph_image', [__CLASS__, "set_ograph_image"], 99 , 1 );
			add_filter('wpseo_twitter_taxonomy_image', [__CLASS__, "add_twitter_image"], 99 , 1 );
			add_action('wpseo_add_opengraph_additional_images', [__CLASS__, "add_ograph_image"], 99 , 1 );
		}
	}


	/**
	 * Set correct robots meta for tax archive
	 * @param $robotsstr
	 *
	 * @return string
	 */
	public static function robots( $robotsstr ) {
		return ""; //an empty string prevents the robots meta tag from being printed
	}

	/**
	 * Set ograph image for tax listings
	 * @param $url
	 *
	 * @return mixed
	 */
	public static function set_ograph_image( $url ) {
		$url = get_field( "ograph_image", "oep_taxonomies_success_type");
		return $url;
	}

	/**
	 * Add ograph image for tax listings
	 * @param $ogfraph_image \WPSEO_OpenGraph_Image
	 */
	public static function add_ograph_image( $ogfraph_image ){
		$url = get_field( "ograph_image", "oep_taxonomies_success_type");
		$ogfraph_image->add_image_by_url( $url );
	}

	/**
	 * Add twitter image for tax listings
	 * @param $img
	 *
	 * @return mixed
	 */
	public static function add_twitter_image( $img ) {
		$url = get_field( "ograph_image", "oep_taxonomies_success_type");
		return $url;
	}
}
