<?php
/**
 * Created by PhpStorm.
 * User: ryansmith
 * Date: 2019-03-21
 * Time: 12:07
 */

namespace OEP\Taxonomies;


/**
 * Class Departments
 *
 * @package OEP\Taxonomies
 */
class Departments {

	/**
	 * Get args for taxonomy
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels  = array(
			'name'                       => _x( 'Departments', 'Taxonomy General Name', 'oep' ),
			'singular_name'              => _x( 'Department', 'Taxonomy Singular Name', 'oep' ),
			'menu_name'                  => __( 'Department', 'oep' ),
			'all_items'                  => __( 'All Departments', 'oep' ),
			'parent_item'                => __( 'Parent Department', 'oep' ),
			'parent_item_colon'          => __( 'Parent Department:', 'oep' ),
			'new_item_name'              => __( 'New Department', 'oep' ),
			'add_new_item'               => __( 'Add New Department', 'oep' ),
			'edit_item'                  => __( 'Edit Department', 'oep' ),
			'update_item'                => __( 'Update Department', 'oep' ),
			'view_item'                  => __( 'View Department', 'oep' ),
			'separate_items_with_commas' => __( 'Separate departments with commas', 'oep' ),
			'add_or_remove_items'        => __( 'Add or remove department', 'oep' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'oep' ),
			'popular_items'              => __( 'Popular Departments', 'oep' ),
			'search_items'               => __( 'Search Departments', 'oep' ),
			'not_found'                  => __( 'Not Found', 'oep' ),
			'no_terms'                   => __( 'No Departments', 'oep' ),
			'items_list'                 => __( 'Departments list', 'oep' ),
			'items_list_navigation'      => __( 'Departments list navigation', 'oep' ),
		);
		$rewrite = array(
			'slug'         => '',
			'with_front'   => false,
			'hierarchical' => false,
		);
		$args    = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_menu'      => false,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
			'show_in_rest'      => true,
			'rewrite'           => $rewrite,
		);

		return $args;
	}

	/**
	 * Filter post types this taxonomy should appear on
	 *
	 * @param $post_types array of post types
	 * @return array of post types
	 * @since 0.2.0
	 */

	public static function set_post_types( $post_types ) {
		$post_types = array('oep_cpts_profile'); //this taxonomy is only relevant for profiles
		return $post_types;
	}
}