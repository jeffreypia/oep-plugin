<?php
/**
 * Created by PhpStorm.
 * User: ryansmith
 * Date: 2019-03-21
 * Time: 12:07
 */

namespace OEP\Taxonomies;


/**
 * Class Programs
 *
 * @package OEP\Taxonomies
 */
class Teams {

	/**
	 * Get args for taxonomy
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels  = array(
			'name'                       => _x( 'Programs', 'Taxonomy General Name', 'oep' ),
			'singular_name'              => _x( 'Program', 'Taxonomy Singular Name', 'oep' ),
			'menu_name'                  => __( 'Programs', 'oep' ),
			'all_items'                  => __( 'All Programs', 'oep' ),
			'parent_item'                => __( 'Parent Program', 'oep' ),
			'parent_item_colon'          => __( 'Parent Program:', 'oep' ),
			'new_item_name'              => __( 'New Program', 'oep' ),
			'add_new_item'               => __( 'Add New Program', 'oep' ),
			'edit_item'                  => __( 'Edit Program', 'oep' ),
			'update_item'                => __( 'Update Program', 'oep' ),
			'view_item'                  => __( 'View Program', 'oep' ),
			'separate_items_with_commas' => __( 'Separate programs with commas', 'oep' ),
			'add_or_remove_items'        => __( 'Add or remove program', 'oep' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'oep' ),
			'popular_items'              => __( 'Popular Programs', 'oep' ),
			'search_items'               => __( 'Search Programs', 'oep' ),
			'not_found'                  => __( 'Not Found', 'oep' ),
			'no_terms'                   => __( 'No Programs', 'oep' ),
			'items_list'                 => __( 'Programs list', 'oep' ),
			'items_list_navigation'      => __( 'Programs list navigation', 'oep' ),
		);
		$rewrite = array(
			'slug'         => 'programs',
			'with_front'   => false,
			'hierarchical' => false,
		);
		$args    = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_menu'      => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
			'show_in_rest'      => true,
			'rewrite'           => $rewrite,
		);

		return $args;
	}

	/**
	 * add configuration page for Taxonomy
	 *
	 * @since 0.2.0
	 */
	public static function add_options_page() {
		if ( ! function_exists( 'acf_add_options_page' ) ) {
			return;
		}

		acf_add_options_page( array(
			'post_id'     => OEP_TEAMS_KEY,
			'page_title'  => __( 'Programs', 'oep' ),
			'menu_title'  => __( 'Programs', 'oep' ),
			'menu_slug'   => OEP_TEAMS_KEY,
			'capability'  => 'edit_posts',
			'icon_url'    => 'dashicons-category',
			'position'    => 31.1,
		) );
	}

	/**
	 * Filter post types this taxonomy should appear on
	 *
	 * @param $post_types array of post types
	 * @return array of post types
	 * @since 0.2.0
	 */

	public static function set_post_types( $post_types ) {
		$post_types = array('oep_cpts_profile', 'post', 'oep_cpts_event'); //this taxonomy is only relevant for profiles, events and posts
		return $post_types;
	}

	/**
	 * Set correct robots meta for tax archive
	 * @param $robotsstr
	 *
	 * @return string
	 */
	public static function robots( $robotsstr ) {
		return ""; //an empty string prevents the robots meta tag from being printed
	}

	/**
	 * Set ograph image for tax listings
	 * @param $url
	 *
	 * @return mixed
	 */
	public static function set_ograph_image( $url ) {
		$url = get_field( "ograph_image", "oep_taxonomies_teams");
		return $url;
	}

	/**
	 * Add ograph image for tax listings
	 * @param $ogfraph_image \WPSEO_OpenGraph_Image
	 */
	public static function add_ograph_image( $ogfraph_image ){
		$url = get_field( "ograph_image", "oep_taxonomies_teams");
		$ogfraph_image->add_image_by_url( $url );
	}

	/**
	 * Add twitter image for tax listings
	 * @param $img
	 *
	 * @return mixed
	 */
	public static function add_twitter_image( $img ) {
		$url = get_field( "ograph_image", "oep_taxonomies_teams");
		return $url;
	}
}