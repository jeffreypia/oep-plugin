<?php
/**
 * Created by PhpStorm.
 * User: ryansmith
 * Date: 2019-03-21
 * Time: 12:07
 */

namespace OEP\Taxonomies;


/**
 * Class Blog Type
 *
 * @package OEP\Taxonomies
 */
class BlogTypes {

	/**
	 * Get args for taxonomy
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels  = array(
			'name'                       => _x( 'Blog Types', 'Taxonomy General Name', 'oep' ),
			'singular_name'              => _x( 'Blog Type', 'Taxonomy Singular Name', 'oep' ),
			'menu_name'                  => __( 'Blog Types', 'oep' ),
			'all_items'                  => __( 'All Blog Types', 'oep' ),
			'parent_item'                => __( 'Parent Blog Type', 'oep' ),
			'parent_item_colon'          => __( 'Parent Blog Type:', 'oep' ),
			'new_item_name'              => __( 'New Blog Type', 'oep' ),
			'add_new_item'               => __( 'Add New Blog Type', 'oep' ),
			'edit_item'                  => __( 'Edit Blog Type', 'oep' ),
			'update_item'                => __( 'Update Blog Type', 'oep' ),
			'view_item'                  => __( 'View Blog Type', 'oep' ),
			'separate_items_with_commas' => __( 'Separate types with commas', 'oep' ),
			'add_or_remove_items'        => __( 'Add or remove type', 'oep' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'oep' ),
			'popular_items'              => __( 'Popular Blog Types', 'oep' ),
			'search_items'               => __( 'Search Blog Types', 'oep' ),
			'not_found'                  => __( 'Not Found', 'oep' ),
			'no_terms'                   => __( 'No Blog Types', 'oep' ),
			'items_list'                 => __( 'Blog Types list', 'oep' ),
			'items_list_navigation'      => __( 'Blog Types list navigation', 'oep' ),
		);
		$rewrite = array(
			'slug'         => '',
			'with_front'   => false,
			'hierarchical' => false,
		);
		$args    = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_menu'      => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
			'show_in_rest'      => true,
			'rewrite'           => $rewrite,
		);

		return $args;
	}

	/**
	 * Filter post types this taxonomy should appear on
	 *
	 * @param $post_types array of post types
	 * @return array of post types
	 * @since 0.2.0
	 */

	public static function set_post_types( $post_types ) {
		$post_types = array('post'); //this taxonomy is only relevant for posts
		return $post_types;
	}

	public static function add_dropdowns( $post_type ){
		if( 'post' !== $post_type ){
			return;
		}
		$slug = OEP_BLOG_TYPES_KEY;
		$taxonomy = get_taxonomy( $slug );
		$selected = '';
		// if the current page is already filtered, get the selected term slug
		$selected = isset( $_REQUEST[ $slug ] ) ? $_REQUEST[ $slug ] : '';
		// render a dropdown for this taxonomy's terms
		wp_dropdown_categories( array(
			'show_option_all' =>  $taxonomy->labels->all_items,
			'taxonomy'        =>  $slug,
			'name'            =>  $slug,
			'orderby'         =>  'name',
			'value_field'     =>  'slug',
			'selected'        =>  $selected,
			'hierarchical'    =>  true,
		) );
		}
}

//bonus: remove No Blog Type on add page @TODO move this somewhere else
//add_filter( "radio_buttons_for_taxonomies_no_term_oep_taxonomies_blog-types", "__return_FALSE" );