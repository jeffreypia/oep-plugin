<?php
/**
 * Created by PhpStorm.
 * User: leisyvidal
 * Date: 2019-03-28
 * Time: 10:00
 */

namespace OEP\Taxonomies;


/**
 * Class Values
 *
 * @package OEP\Taxonomies
 */
class Values {

	/**
	 * Get args for taxonomy
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels  = array(
			'name'                       => _x( 'Values', 'Taxonomy General Name', 'oep' ),
			'singular_name'              => _x( 'Value', 'Taxonomy Singular Name', 'oep' ),
			'menu_name'                  => __( 'Values', 'oep' ),
			'all_items'                  => __( 'All Values', 'oep' ),
			'parent_item'                => __( 'Parent Value', 'oep' ),
			'parent_item_colon'          => __( 'Parent Value:', 'oep' ),
			'new_item_name'              => __( 'New Value', 'oep' ),
			'add_new_item'               => __( 'Add New Value', 'oep' ),
			'edit_item'                  => __( 'Edit Value', 'oep' ),
			'update_item'                => __( 'Update Value', 'oep' ),
			'view_item'                  => __( 'View Value', 'oep' ),
			'separate_items_with_commas' => __( 'Separate values with commas', 'oep' ),
			'add_or_remove_items'        => __( 'Add or remove values', 'oep' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'oep' ),
			'popular_items'              => __( 'Popular Values', 'oep' ),
			'search_items'               => __( 'Search Values', 'oep' ),
			'not_found'                  => __( 'Not Found', 'oep' ),
			'no_terms'                   => __( 'No Values', 'oep' ),
			'items_list'                 => __( 'Values list', 'oep' ),
			'items_list_navigation'      => __( 'Values list navigation', 'oep' ),
		);
		$rewrite = array(
			'slug'         => 'values',
			'with_front'   => false,
			'hierarchical' => true,
		);
		$args    = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_menu'      => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
			'show_in_rest'      => true,
			'rewrite'           => $rewrite,
		);

		return $args;
	}

	/**
	 * Filter post types this taxonomy should appear on
	 *
	 * @param $post_types array of post types
	 * @return array of post types
	 * @since 0.2.0
	 */

	public static function set_post_types( $post_types ) {
		$post_types = array('post'); //this taxonomy is only relevant for posts for now @TODO add other relevant post types
		return $post_types;
	}
}
