<?php
/**
 * Created by PhpStorm.
 * User: leisyvidal
 * Date: 2019-03-28
 * Time: 10:00
 */

namespace OEP\Taxonomies;


/**
 * Class Champions
 *
 * @package OEP\Taxonomies
 */
class Memberships {

	/**
	 * Get args for taxonomy
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels  = array(
			'name'                       => _x( 'Champions', 'Taxonomy General Name', 'oep' ),
			'singular_name'              => _x( 'Champion', 'Taxonomy Singular Name', 'oep' ),
			'menu_name'                  => __( 'Champions', 'oep' ),
			'all_items'                  => __( 'All Champions', 'oep' ),
			'parent_item'                => __( 'Parent Champion', 'oep' ),
			'parent_item_colon'          => __( 'Parent Champion:', 'oep' ),
			'new_item_name'              => __( 'New Champion', 'oep' ),
			'add_new_item'               => __( 'Add New Champion', 'oep' ),
			'edit_item'                  => __( 'Edit Champion', 'oep' ),
			'update_item'                => __( 'Update Champion', 'oep' ),
			'view_item'                  => __( 'View Champion', 'oep' ),
			'separate_items_with_commas' => __( 'Separate champions with commas', 'oep' ),
			'add_or_remove_items'        => __( 'Add or remove champions', 'oep' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'oep' ),
			'popular_items'              => __( 'Popular Champions', 'oep' ),
			'search_items'               => __( 'Search Champions', 'oep' ),
			'not_found'                  => __( 'Not Found', 'oep' ),
			'no_terms'                   => __( 'No Champions', 'oep' ),
			'items_list'                 => __( 'Champions list', 'oep' ),
			'items_list_navigation'      => __( 'Champions list navigation', 'oep' ),
		);
		$rewrite = array(
			'slug'         => 'champions',
			'with_front'   => false,
			'hierarchical' => true,
		);
		$args    = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_menu'      => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
			'show_in_rest'      => true,
			'rewrite'           => $rewrite,
		);

		return $args;
	}

	/**
	 * Filter post types this taxonomy should appear on
	 *
	 * @param $post_types array of post types
	 * @return array of post types
	 * @since 0.2.0
	 */

	public static function set_post_types( $post_types ) {
		$post_types = array('oep_cpts_profile', 'post'); //this taxonomy is only relevant for profiles and posts for now @TODO add other relevant post types
		return $post_types;
	}
}
