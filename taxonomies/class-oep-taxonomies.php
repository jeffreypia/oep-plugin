<?php

/**
 * Add custom taxonomies via the plugin.
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 * @since      1.0.0 added shared taxonomies across multisite network
 *
 * @package    OEP
 * @subpackage OEP/taxonomies
 */

/**
 * Add and register multiple custom taxonomies, as well as define common functionality for terms
 *
 * @package    OEP
 * @subpackage OEP/taxonomies
 * @author     Ryan Smith <ryan@designzillas.com>
 *
 * @TODO       build taxonomies menu for admin
 */

namespace OEP\Taxonomies;

use OEP\Common\Loader;
use function OEP\Common\oep_fn_template_part;
use OEP\CPT;
use Roots\Soil\Nav\NavWalker;

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Taxonomy {

    /**
     * Categories and their details (not actual term objects though)
     *
     * @var   array
     * @since 1.0.0
     */
    private static $pills = null;

    /**
     * Transient for categories and their custom fields
     *
     * @var   string
     * @since 1.0.0
     */
    private static $t_pills = 'oep_pill_details';

	/**
	 * Taxonomy keys
	 *
	 * @since       0.1.0
	 * @access      protected
	 * @var         array $tax_keys key / string
	 */
	protected static $tax_keys = array(
		'OEP_BLOG_TYPES_KEY'       => 'oep_taxonomies_blog-types',
		'OEP_QOL_KEY'              => 'oep_taxonomies_qol',
		'OEP_COMPETITIVENESS_KEY'  => 'oep_taxonomies_competitiveness',
		'OEP_INDUSTRIES_KEY'       => 'oep_taxonomies_industries',
		'OEP_REGIONS_KEY'          => 'oep_taxonomies_regions',
		'OEP_ACTIVITY_KEY'         => 'oep_taxonomies_activity',
		'OEP_TEAMS_KEY'            => 'oep_taxonomies_teams',
		'OEP_DEPARTMENTS_KEY'      => 'oep_taxonomies_departments',
		'OEP_MEMBERSHIPS_KEY'      => 'oep_taxonomies_memberships',
		'OEP_IDEAS_KEY'            => 'oep_taxonomies_ideas',
		'OEP_VALUES_KEY'           => 'oep_taxonomies_values',
		'OEP_SUCCESS_TYPE_KEY'     => 'oep_taxonomies_success-type',
		'OEP_SIZE_KEY'        => 'oep_taxonomies_size',
	);
	/**
	 * Loader
	 *
	 * @var $loader
	 */

	protected $loader;
	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $oep The ID of this plugin.
	 */
	protected $oep;
	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	protected $version;
	/**
	 * Taxonomy classes array for invoking registration
	 *
	 * @since       0.1.0
	 * @access      protected
	 * @var         array $tax_classes key / string
	 */
	protected $tax_classes = array();


	/**
	 * Current taxonomy for use with filtering
	 *
	 * @var $current_taxonomy
	 */
	protected $current_taxonomy;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 *
	 * @param      string $oep     The name of this plugin.
	 * @param      string $version The version of this plugin.
	 *
	 * /**
	 * Initialize taxonomies and properties
	 *
	 * @since    0.1.0
	 */
	public function __construct( $oep, $version ) {
		$this->oep     = $oep;
		$this->version = $version;
		$this->define_keys();
		$this->load_taxonomies();

		add_action( 'init'                  , [ __CLASS__ , 'set_edit_hooks'    ], 15    );
		add_action( 'wp_head'               , [ __CLASS__ , 'do_css'            ]        );
	}

	/**
	 * Set the hooks to fire off on category edit.
	 *
	 * We want to do stuff whenever a category term is edited OR a post type
	 * associated with categories is edited.
	 *
	 * @since 1.0.0
	 */
	public static function set_edit_hooks() {

		$taxonomies = array(
			'oep_taxonomies_industries',
			'oep_taxonomies_qol',
			'oep_taxonomies_competitiveness',
			'oep_taxonomies_teams',
			'oep_taxonomies_activity'
		);

		foreach ( $taxonomies as $taxonomy ) {

			add_action( "edited_{$taxonomy}", [ __CLASS__ , 'set_pills' ] );

			$tax_settings = get_taxonomy( $taxonomy );

			foreach ( $tax_settings->object_type as $post_type ) {
				add_action( "save_post_{$post_type}", [ __CLASS__ , 'set_pills' ] );
			}
		}
	}

    /**
     * Grab custom category stuff and their fields, then cache them
     *
     * @return array  categories
     * @since  1.0.0
     */
    public static function set_pills() {
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
		}

		// always get and save from first blog
		switch_to_blog( 1 );

    	$terms = get_terms(array(
		    'oep_taxonomies_industries',
		    'oep_taxonomies_qol',
		    'oep_taxonomies_competitiveness',
		    'oep_taxonomies_teams',
		    'oep_taxonomies_activity'
    	), array(
		    'hide_empty' => true,
		));

        if ( ! $terms ) {
            set_transient( self::$t_pills, $terms );
            return $terms;
        }

        // generate pills
        foreach ( $terms as $term ) {
            $pills[ $term->slug ] = [
                'ID'    => $term->term_id,
                'name'  => $term->name,
                'color' => get_field( 'color' , $term  ) ?: self::get_color($term->taxonomy, false),
            ];
        }
        set_transient( self::$t_pills, $pills );

		restore_current_blog();

        return $pills;
    }

    /**
     * Get a category's details
     *
     * @param  string  $slug  category slug
     * @return array          category name, ID, icon, color etc.
     *
     * @since 1.0.0
     */
    public static function get_pill_colors( $slug = false ) {

		// always get transient from main blog
		switch_to_blog( 1 );

        // get/set all the categories
        self::$pills = self::$pills !== null ? self::$pills : get_transient( self::$t_pills );

		restore_current_blog();

        if ( $slug && empty( self::$pills[ $slug ] ) ) {
            return false;
        }

        return $slug ? self::$pills[ $slug ] : self::$pills;
    }

	/**
	 * Define keys
	 *
	 * @since 0.1.0
	 */
	private function define_keys() {
		$keys = self::get_tax_keys();
		foreach ( $keys as $key => $value ) {
			define( $key, $value );
		}
	}

	/**
	 * @return array
	 */
	public static function get_tax_keys() {
		return self::$tax_keys;
	}

	/**
	 * Load taxonomy arguments
	 *
	 * @since 0.1.0
	 */
	public function load_taxonomies() {
		$keys = self::get_tax_keys();
		foreach ( $keys as $key => $value ) {
			if ( file_exists( OEP_PLUGIN_DIR . 'taxonomies/' . $value . '.php' ) ) {
				require_once OEP_PLUGIN_DIR . 'taxonomies/' . $value . '.php';
			}
		}
	}

	public static function do_css() {
		$styles = [];

		$terms = self::get_pill_colors();
		if (!$terms || empty( $terms )) {
			return; //nothing to see here, bounce
		}

		foreach ( self::get_pill_colors() as $slug => $term ) {
			if( "" !== $term['color'] ) :
			    $styles[] = "\t.pill.--$slug {\n\t\tborder-color: ".\OEP\Common\hex2rgba( $term['color'], '0.65').";\n\t\tcolor: {$term['color']};\n\t\tbackground-color: ".\OEP\Common\hex2rgba( $term['color'], '0.65').";\n\t}\n";
			endif;
		}

		// $styles is undefined. figure out why
		echo "\n<style type='text/css'>\n\n" . implode( "\n", $styles ) . "\n</style>\n";
	}

	/**
	 * Function for returning a list of terms as pills
	 *
	 * @param $type       string type of pill (ghost or nothing for default)
	 * @param $taxonomies array which tax to get terms for
	 *
	 * @TODO build the output of this function
	 */
	public static function get_pills(
		$type = 'solid', $taxonomies = array(
		'oep_taxonomies_industries',
		'oep_taxonomies_qol',
		'oep_taxonomies_competitiveness',
		'oep_taxonomies_teams',
		'oep_taxonomies_activity'
	)
	) {
		$type       = ! empty( $type ) ? $type : 'solid'; //default

		if(! empty( $taxonomies ) && in_array( OEP_BLOG_TYPES_KEY, $taxonomies )){
			$taxonomies = [];
		}
		$taxonomies = ! empty( $taxonomies ) ? $taxonomies : array(
			'oep_taxonomies_industries',
			'oep_taxonomies_qol',
			'oep_taxonomies_competitiveness',
			'oep_taxonomies_teams',
			'oep_taxonomies_activity'
		); //default


		$terms = wp_get_post_terms( get_the_ID(), $taxonomies );
		$ul    = '<ul class="pills ' . $type . '">';
		foreach ( $terms as $term ):
			if( is_object($term) ) {
				$color = ! empty ( get_field( 'color', $term ) ) ? get_field( 'color', $term ) : self::get_color( $term->taxonomy, false );
				$term_link = get_term_link( $term ) ?: "" ;
//				get_term_link( $term ) ? $term_link = get_term_link( $term ) : $term_link = "";
				$ul    .= '<li class="pill --' . $term->slug . '"><a href="' . $term_link . '">' . $term->name . '</a></li>';

				if ( ! is_single() ) {
					break; //jump out after the first term
				}
			}

		endforeach;
		$ul .= '</ul>';
		echo $ul;
	}

	/**
	 * @param string $taxonomy
	 * @param bool   $echo
	 *
	 * @return mixed|string
	 */
	public static function get_color( $taxonomy = '', $echo = true ) {
		if ( $echo ) {
			echo get_field( 'color', ! empty( $taxonomy ) ? $taxonomy : get_query_var( 'taxonomy' ) ) ?? '#CCCCCC';
		} else {
			return get_field( 'color', ! empty( $taxonomy ) ? $taxonomy : get_query_var( 'taxonomy' ) ) ?? '#CCCCCC';
		}
	}

	/**
	 * Register taxonomies
	 *
	 * @since   0.1.0
	 */
	public function register() {
		//register taxonomies
		$taxonomies = self::get_tax_keys();
		foreach ( $taxonomies as $taxonomy => $value ) {
			/*
			 * @TODO move this to a helper function since we'll probably need to do this for CPTs as well.
			 */
			$filename_to_array = explode( '_', constant( $taxonomy ) );
			$class_name        = end( $filename_to_array );
			//$tax_class         = __NAMESPACE__ . '\\' . ucfirst( $class_name );
			$tax_class = __NAMESPACE__ . '\\' . str_replace( ' ', '', ucwords( str_replace( [
					'-',
					'_',
				], ' ', $class_name ) ) );
			$a         = new $tax_class;

			//create a new Loader and filter any custom post types declared in taxonomy
			$post_types = array_merge( array( "post" ), CPT\PostType::get_cpt_keys() );
			if ( method_exists( $a, 'set_post_types' ) ) {
				$this->loader = new Loader();
				$this->loader->add_filter( 'taxonomy_post_types_'.$taxonomy, $a, 'set_post_types', 10, 1 );
				$this->loader->run();
				$post_types = apply_filters( 'taxonomy_post_types_'.$taxonomy, $post_types );
			}

			//create another loader object and add options pages if declared in taxonomy
			if ( method_exists( $a, 'add_options_page' ) ) {
				$this->loader = new Loader();
				$this->loader->add_action( 'wp_loaded', $a, 'add_options_page' );
				$this->loader->run();
			}

			//posts listing dropdowns in admin
			if ( method_exists( $a, 'add_dropdowns' ) ) {
				$this->loader = new Loader();
				$this->loader->add_action( 'restrict_manage_posts', $a, 'add_dropdowns', 10, 1 );
				$this->loader->run();
			}

			//seo settings, run in wpseo_head so that it gets a chance to fire stuff before Yoast does anything
			if ( method_exists( $a, 'seo_settings' ) ) {
				$this->loader = new Loader();
				$this->loader->add_action( 'wpseo_head', $a, 'seo_settings', 10, 1 );
				$this->loader->run();
			}

			register_taxonomy( constant( $taxonomy ), $post_types, $a::get_args() );
		}
	}

	/**
	 * set body class for terms
	 *
	 * @since 0.2.0
	 *
	 * @param $classes array of body classes
	 *
	 * @return array
	 */

	public function set_term_body_class( $classes ) {
		if (is_tax()) {
			$classes[] = 'term';
		}

		return $classes;
	}

	/**
	 *
	 */
	public function rewrite_rules() {
		$args       = array(
			'public'   => true,
			'_builtin' => false,

		);
		$output     = 'objects'; // or names
		$operator   = 'and'; // 'and' or 'or'
		$taxonomies = get_taxonomies( $args, $output, $operator );

		foreach ( $taxonomies as $taxonomy ) {
			$slug = $taxonomy->rewrite['slug'];
			$name = $taxonomy->name;
			add_rewrite_rule( $slug . '(/page/([0-9]+))?/?$', 'index.php?taxonomy=' . $name . '&paged=$matches[2]', 'top' );
		}
	}

	/**
	 * Make adjustments to WordPress WP_Query to make taxonomy archives work.
	 *
	 * @param $wp_query
	 * @since 0.1 initial config
	 * @since 1.0.0 set term->name to empty string for SEO title purposes
	 */
	public function parse_query( &$wp_query ) {
		// is_tax is only true if both a taxonomy and a term are set, otherwise is_home is true
		// But we don't want that: is_tax and is_archive should be true, is_home should be false
		if ( ! $wp_query->is_tax && $taxonomy_query = $wp_query->get( 'taxonomy' ) ) {
			foreach ( $GLOBALS['wp_taxonomies'] as $taxonomy => $t ) {
				if ( $t->query_var && $taxonomy_query == $t->query_var ) {
					// Make sure the conditional tags work, so the right template is loaded
					$wp_query->is_tax     = true;
					$wp_query->is_archive = true;
					$wp_query->is_home    = false;

					// Make is_tax($taxonomy) work
					$wp_query->queried_object           = $t;
					$wp_query->queried_object->taxonomy = $taxonomy;
					$wp_query->queried_object->name  = '';
					$wp_query->queried_object->term_id  = null;
					$wp_query->queried_object->slug     = $t->rewrite['slug'];

					//set the args for _section and taxonomy archive display related stuff
					$this->current_taxonomy = $t;
					add_filter( 'body_class', [ $this, 'set_taxonomy_body_class' ] );
					add_filter( 'oep_section_post_id', [ $this, 'set_section_tax_id' ], 10, 3 );
					add_filter( 'meta/banner', [ $this, 'set_banner_args' ], 10, 3 );
					add_filter( 'wpseo_title', [ $this, 'set_seo_title' ], 99, 1 );
					add_filter( 'get_the_archive_title', [ $this, 'set_archive_title' ], 11, 1 );
					add_filter( 'single_term_title', '__return_empty_string' );
					add_filter( 'get_the_archive_description', [ $this, 'set_archive_desc' ] );
					// get term list under heading
					add_action( 'taxonomy_get_terms', [ $this, 'get_tax_terms' ], 10, 1 );

					//get featured posts for this taxonomy
					/**
					 * @TODO create interface for manually selecting these items in the back end taxonomy options page, for now it will use the featured post_tag
					 */
					add_action( 'taxonomy_featured_posts', [ $this, 'get_taxonomy_featured_posts' ] );

					$wp_query->set( 'term', null );

					//we might as well set the tax_query for wp_query while we're here

					$tax_query = array(
						array(
							'taxonomy'         => $taxonomy,
							'field'            => 'slug',
							'include_children' => true,
							'terms'            => get_terms( $taxonomy, array(
								'fields'     => 'names',
								'hide_empty' => false,
							) ),
						),
					);

					$wp_query->set( 'tax_query', $tax_query );

					break;
				}
			}
		}
	}

	/**
	 * @param $classes
	 *
	 * @return array
	 */
	public function set_taxonomy_body_class( $classes ) {
		$classes[] = 'taxonomy';

		return $classes;
	}

	/**
	 * Set post_id for _section related stuff
	 *
	 * @param $post_id
	 * @param $slug
	 * @param $name
	 *
	 * @return mixed
	 */
	public function set_section_tax_id( $post_id, $slug, $name ) {
		return $this->current_taxonomy;
	}

	/**
	 * @param $meta
	 * @param $meta_field
	 * @param $args
	 *
	 * @return mixed
	 */
	public function set_banner_args( $meta, $meta_field, $args ) {
		$meta['title'] = $this->current_taxonomy->label;

		return $meta;
	}

	/**
	 * Change taxonomy title in SEO to taxonomy label.
	 *
	 * @param $title
	 *
	 * @return string|string[]|null
	 * @since 0.1 initial config
	 * @since 1.0.0 disabled the search and replace since SEO term title variable is derived from the name property of the queried object, and that is set to an empty string in the parse_query function
	 *
	 * @TODO deprecate this function if it's no longer needed.
	 */
	public function set_seo_title( $title ) {
		$new_title = $this->current_taxonomy->label;
		//take out the taxonomy name (oep_taxonomies_xxx) and replace it with the title
//		$title = preg_replace( '/^(\w+\s)/', $new_title . ' ', $title );

		return $title;
	}

	/**
	 * @param $name
	 *
	 * @return mixed
	 */
	public function set_archive_title( $name ) {
		if(get_query_var( 'term' )){
			$name = get_queried_object()->name;
		} else {
			if( $this->current_taxonomy ) {
				$name = $this->current_taxonomy->label;
			} else {
				return $name;
			}
		}
		return $name;
	}

	/**
	 * @param $desc
	 *
	 * @return mixed
	 */
	public function set_archive_desc( $desc ) {
		$desc = get_field( 'description', $this->current_taxonomy->name );

		return $desc;
	}

	/**
	 * @param $terms array of terms with id and taxonomy is belongs to
	 * @return false|string
	 *
	 */
	public function get_tax_terms($terms = array()) {
		if(!empty( $terms )){
			$list = "<li>". __( 'All', 'oep' ) ."<ul>";
			foreach ($terms as $term) {
//				d($term, get_term_link( intval( $term["term_id"] ), $term["taxonomy"]));
				if( !is_wp_error( get_term_link( intval( $term["term_id"] ), $term["taxonomy"]) ) ) :
					$list .= "<li><a href='".get_term_link( intval( $term["term_id"] ), $term["taxonomy"]) ."'>".get_term(intval($term["term_id"]))->name."</a></li>";
				endif;
			}
			$list .= "</li></ul>";
		} else {
			$list = wp_list_categories( array(
				'taxonomy'   => get_query_var( 'taxonomy' ),
				'hide_empty' => true,
				'title_li'   => __( 'All', 'oep' ),
				'number'     => 4,
				'echo'       => false,
				'depth'       => 1,
			) );
		}
		$list .= wp_list_categories( array(
			'taxonomy'   => get_query_var( 'taxonomy' ),
			'class'      => 'pills ' . get_query_var( 'taxonomy' ),
			'hide_empty' => false,
			'title_li'   => sprintf( "<a href='#showall'>%1s <i class='fa fa-angle-down'></i></a>", __( 'More', 'oep' )),
			'echo'       => false,
			'depth'       => 1,
		) );
		echo $list;
	}


	/**
	 * retrieve the menu items from the nav based on the current page, echos an HTML list
	 * @param $object_id ID of current page
	 * @param $terms array of selected terms from landing page if available
	 */
	public function get_submenu( $object_id, $terms = array() ) {
		$current_category = 0;
		$child_of = 0;
		$is_taxonomy = false;
		$includes = array();
		$include = "";

		if(!empty( $terms )){
			foreach ($terms as $term) {
//				d($term, get_term_link( intval( $term["term_id"] ), $term["taxonomy"]));
				$includes[] = $term["term_id"];
			}
			$include = implode(",", $includes);
		}

		$defaults = array(
			'menu'            => '',
			'container'       => 'div',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'menu',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'item_spacing'    => 'preserve',
			'depth'           => 0,
			'walker'          => '',
			'theme_location'  => '',
		);

		// get commhub menu args
		$args = array(
			'theme_location' => 'commhub',
			'menu_id'        => 'commhub-menu',
			'depth'          => 2,
			'walker'         => new NavWalker(),
		);

		$args = wp_parse_args( $args, $defaults );
		//build wp_nav_menu args object
		$args = (object) $args;
		//get menu object
		$menu = wp_get_nav_menu_object( $args->menu_id );
		$menu_items = wp_get_nav_menu_items($menu);
		$temp_menu_items = array();
//		d($menu_items);
		//get menu item ids for current page
		$current_menu_item_ids = array();
		if(is_int( $object_id )){
			// this is a landing page probably
			$current_menu_item_ids = wp_get_associated_nav_menu_items($object_id);
		} else {
			// ugh, this is a taxonomy, use the slug instead
			$is_taxonomy = true;

			if($current_term = term_exists( $object_id, get_query_var( 'taxonomy' ) )){
				$current_category = $current_term['term_id'];
				$child_of = get_term($current_category)->parent;
//				d($current_category, $child_of);
			}
			foreach ($menu_items as $menu_item) {
				$temp_menu_items[$menu_item->ID] = $menu_item;
				if($object_id == str_replace('/', '', basename($menu_item->url))){
					if($menu_item->menu_item_parent !== "0") {
						$current_menu_item_ids[] = $temp_menu_items[$menu_item->menu_item_parent]->object_id; //use the parent to build the menu if it appears in a nav
					} else {
						$current_menu_item_ids[] = $menu_item->ID;
					}

//					d($current_menu_item_ids);
				}
			}
			$object_id = end($current_menu_item_ids);
		}

//		d($object_id);

		$current_menu_item_id = end($current_menu_item_ids); //get the last id from the array
		$current_menu_item = (object)[]; //new empty object to hold our menu item
		//we need to iterate through all the menu items to get the menu item, because, miraculously, WordPress doesn't have a function that just returns a single menu object

		//convert back to array
		$args = (array) $args;
		$args['start_in'] = $object_id;
		$args['echo'] = false;
		$args['items_wrap'] = '%3$s';
		$args['menu_id'] = "";
		$items = wp_nav_menu($args);
		if($items === false){ // we didn't find this taxonomy or term in a nav menu, let's try something else.
			$items = wp_list_categories( array(
				'taxonomy'   => get_query_var( 'taxonomy' ),
				'hide_empty' => true,
				'current_category' => $current_category,
				'child_of' => $child_of,
				'title_li'   => sprintf( '<a href="%1s">%2s</a>', "#", get_taxonomy( get_query_var( 'taxonomy' ))->label),
//				'number'     => 4,
				'echo'       => false,
				'depth'       => 1,
			) );

		}
		if($is_taxonomy){
			$items .= "<span class='shade'></span>";
			$items .= '<form id="category-select" class="category-select" action="'.esc_url( home_url( '/' ) ) . ' " method="get">';
			$items .= wp_dropdown_categories( array(
				'taxonomy'   => get_query_var( 'taxonomy' ),
				'name'   => get_query_var( 'taxonomy' ),
				'class'      => 'pills ' . get_query_var( 'taxonomy' ),
				'hide_empty' => false,
				'show_option_all'   => __( 'More', 'oep' ),
				'echo'       => false,
				'depth'       => 2,
				'value_field'       => 'slug',
				'include'     => $include,
				'hierarchical'       => true,
				'orderby'       => 'name',
				'order'       => 'ASC', //@TODO taxonomy order plugin is overriding all this, need to adjust
			) );
			$replace = "<select$1 onchange='return this.form.submit()'>";
			$items  = preg_replace( '#<select([^>]*)>#', $replace, $items );
			$items .= '</form>';
		} else {
			//do something else
			$items .= "<span class='shade'></span>";
			$items .= '<form id="term-dropdown-select" class="category-select" action="'.esc_url( home_url( '/' ) ) . ' " method="get">';
			$items .= '<select name="cat" id="term-dropdown" class="postform">;
    <option value="">More</option>';
			foreach ( $includes as $term ) {
				$items .= sprintf( '<option value="%1$s" data-taxonomy="%2$s">%3$s</option>',
					get_term($term)->slug,
					get_term($term)->taxonomy,
					get_term($term)->name
				);
			}
			$items .= '</select></form>';

		}

		echo $items;
	}

	/**
	 * @param $output
	 * @param $args
	 * @var $node \DOMNode
	 * @var $li \DOMElement
	 *
	 * @return mixed
	 */
	public static function add_term_classes_to_pills( $output, $args ) {
		// are we doing pills?
		if($args['hide_empty'] == false){
			$depth = 1;
			$markup = new \DOMDocument();
			$markup->loadHTML( $output );

			$li = $markup->getElementsByTagName( 'li' );
			foreach ($li as $node) {
				$classes = $node->getAttribute('class');
				// the term id is available via one of the classes on the li
				$term_id = preg_replace('/[^0-9]/', '', $classes);
				if($term_id){
					$term_name = get_term_by( 'id', $term_id, get_query_var( 'taxonomy' ))->slug;
				} else {
					$term_name = "default";
				}
				$classes .= ' pill --'.$term_name;
				$node->setAttribute('class', $classes);
			}
			$ul = $markup->getElementsByTagName( 'ul' );
			foreach ($ul as $node) {
				$node->setAttribute('class', 'pills ghosts');
			}
			return $markup->saveHTML();
		}
		return $output;
	}

	/**
	 * Get featured posts for parent level taxonomy
	 *
	 * @param string   $taxonomy  taxonomy to query (falls back to main query)
	 * @param array    $terms     terms to query (falls back to main query)
	 * @param boolean  $featured  actually apply filter for "featured" post tag
	 * @param array    $blogs     multisite query blogs
	 */
	public function get_taxonomy_featured_posts( $taxonomy = null, $terms = [], $featured = true, $blogs = null ) {

		// set taxonomy, falling back to current query
		$taxonomy = $taxonomy ?: get_query_var( 'taxonomy' ); // was ! empty( get_query_var( 'term' ) ) ? get_query_var( 'term' ) : get_query_var( 'taxonomy' );

		// are we working with a site network query (plugin)?
		$is_network = $blogs && class_exists( 'Network_Query' );

		// set terms: use those passed in, then fall back to current query,
		// using all if none are in the query
		if ( ! $terms ) {
			$terms = empty( get_query_var( 'term' ) )
				? get_terms( $taxonomy, [
					'fields'     => 'names',
					'hide_empty' => false,
				])
				: [ get_query_var( 'term' ) ];
		}

		// set up query args with compatible post types, results, etc
		$featured_posts_args = array(
			'post_type'           => array_merge( array( 'post' ), CPT\PostType::get_cpt_keys() ),
			'posts_per_page'      => ! empty( get_query_var( 'term' ) ) ? 4 : 7,
			'ignore_sticky_posts' => 0,
			// 'post__in'            => $sticky, // $sticky = get_option( 'sticky_posts' );
			// 'orderby'             => 'modified', @TODO create queue for this tag
			// 'order'               => 'ASC',
		);

		// intialize tax query
		$featured_posts_args['tax_query'] = array(
			'relation' => 'AND',
			array(
				'taxonomy'         => $taxonomy,
				'field'            => 'slug',
				'include_children' => true,
				'terms'            => $terms,
			),
		);

		// add in "featured" tag check
		if ( $featured ) {
			$featured_posts_args['tax_query'][] = [
				'taxonomy' => 'post_tag',
				'field'    => 'slug',
				'terms'    => __( 'featured', 'oep' ),
			];
		}

		// build standard or network query
		if ( $is_network ) {
			$featured_posts_args['blog_id'] = $blogs;
			$featured_posts_query = new \Network_Query( $featured_posts_args );
		} else {
			$featured_posts_query = new \WP_Query( $featured_posts_args );
		}

		// load the looper
		oep_fn_template_part( 'taxonomy-featured', 'posts', array(
			'featured_posts_query' => $featured_posts_query,
		));

		if ( $is_network ) {
			\network_reset_postdata();
		}
	}

	/**
	 * Adjust query to filter by post post_type whenever a blog type term is passed
	 *
	 * @param $filter_query WP_Query
	 */
	public function set_filter_query( $filter_query ) {
		if ( defined( "DOING_AJAX")) {
			if ( isset($_POST['post_type']) && in_array( $_POST['post_type'], get_terms( [
					'taxonomy' => OEP_BLOG_TYPES_KEY,
					'fields'   => 'id=>slug',
				] ) ) ) {
				$filter_query->set("post_type", "post");
//				error_log(json_encode($filter_query->get('tax_query')));
				$filter_query->set("tax_query", array_merge(
					array(
						'relation'      => 'AND',
						array(
							'taxonomy'  => OEP_BLOG_TYPES_KEY,
							'field'     => 'slug',
							'terms'     => $_POST['post_type']
						)
					),
					array($filter_query->get('tax_query'))
				));
//				$filter_query->set(OEP_BLOG_TYPES_KEY, $_POST['post_type']);
//				error_log(json_encode($filter_query->get('tax_query')));
			}
		}
	}

	/**
	 * Share all taxonomy terms across the multisite network.
	 *
	 * This allows sites to use the same terms for classifying content regardless of where the term was created.
	 *
	 * @see https://buddydev.com/want-global-categories-tags-taxonomies-across-wordpress-multisite-network/
	 * @since 1.0.0
	 */
	public static function change_tax_terms_table() {
		global $wpdb;
		//change terms table to use main site's
		$wpdb->terms = $wpdb->base_prefix . 'terms';
		//change taxonomy table to use main site's taxonomy table
		$wpdb->term_taxonomy = $wpdb->base_prefix . 'term_taxonomy';

		// change termmeta table to use main site's
		$wpdb->termmeta = $wpdb->base_prefix . 'termmeta';
	}
}
