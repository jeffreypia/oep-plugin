<?php
/**
 * Created by PhpStorm.
 * User: leisyvidal
 * Date: 2019-03-28
 * Time: 10:00
 */

namespace OEP\Taxonomies;


/**
 * Class Company Size
 *
 * @package OEP\Taxonomies
 */
class Size {

	/**
	 * Get args for taxonomy
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels  = array(
			'name'                       => _x( 'Size', 'Taxonomy General Name', 'oep' ),
			'singular_name'              => _x( 'Size', 'Taxonomy Singular Name', 'oep' ),
			'menu_name'                  => __( 'Size', 'oep' ),
			'all_items'                  => __( 'All Sizes', 'oep' ),
			'parent_item'                => __( 'Parent Size', 'oep' ),
			'parent_item_colon'          => __( 'Parent Size:', 'oep' ),
			'new_item_name'              => __( 'New Size', 'oep' ),
			'add_new_item'               => __( 'Add New Size', 'oep' ),
			'edit_item'                  => __( 'Edit Size', 'oep' ),
			'update_item'                => __( 'Update Size', 'oep' ),
			'view_item'                  => __( 'View Size', 'oep' ),
			'separate_items_with_commas' => __( 'Separate sizes with commas', 'oep' ),
			'add_or_remove_items'        => __( 'Add or remove sizes', 'oep' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'oep' ),
			'popular_items'              => __( 'Popular Size', 'oep' ),
			'search_items'               => __( 'Search Size', 'oep' ),
			'not_found'                  => __( 'Not Found', 'oep' ),
			'no_terms'                   => __( 'No Size', 'oep' ),
			'items_list'                 => __( 'Size list', 'oep' ),
			'items_list_navigation'      => __( 'Size list navigation', 'oep' ),
		);
		$rewrite = array(
			'slug'         => 'sizes',
			'with_front'   => false,
			'hierarchical' => true,
		);
		$args    = array(
			'labels'            => $labels,
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_menu'      => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
			'show_in_rest'      => true,
			'rewrite'           => $rewrite,
		);

		return $args;
	}

	/**
	 * Filter post types this taxonomy should appear on
	 *
	 * @param $post_types array of post types
	 * @return array of post types
	 * @since 0.2.0
	 */

	public static function set_post_types( $post_types ) {
		$post_types = array('oep_cpts_company'); //this taxonomy is only relevant for companies for now @TODO add other relevant post types
		return $post_types;
	}
}
