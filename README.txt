=== Orlando Economic Partnership (OEP) Plugin ===
Contributors: (dzryansmith & jeffreypia)
Requires at least: 5.0.0
Tested up to: 5.0.0
Stable tag: 5.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin enables custom functionality for the Orlando Economic Partnership's network of sites.

== Description ==

Included in this plugin are custom post type definitions, taxonomy and term definitions, ACF field group JSON, miscellaneous functionality as well as integration with third party services where applicable.

== Installation ==

1. Upload `OEP` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 0.1 =
* init plugin
