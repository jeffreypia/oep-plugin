<?php
/**
 * User: leisyvidal
 * Date: 2019-04-18
 * Time: 6:25PM
 */

namespace OEP\CPT;
use OEP\Common;

/**
 * Class Success Story
 *
 * @package OEP\CPT
 */
class Success {

	protected $loader;

	/**
	 * @var array
	 */
	protected static $template_args = array(
		array( 'core/image' ),
		array(
			'core/heading',
			array(
				'placeholder' => 'Section Title',
				'level'       => 4,

			),
		),
		array(
			'core/paragraph',
			array(
				'placeholder' => OEP_LOREM,
			),
		),
		array( 'oep/recommendedstories' ),
	);

	public function __construct() {
		$this->loader  = new Common\Loader();
		$this->run();
	}

	/**
	 * Get args for CPT
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels = [
			'name'                  => _x( 'Success Story', 'Post Type General Name', 'oep' ),
			'singular_name'         => _x( 'Success Story', 'Post Type Singular Name', 'oep' ),
			'menu_name'             => __( 'Success Stories', 'oep' ),
			'name_admin_bar'        => __( 'Success Story', 'oep' ),
			'archives'              => __( 'Success Story Archives', 'oep' ),
			'attributes'            => __( 'Success Story Attributes', 'oep' ),
			'parent_item_colon'     => __( 'Parent Success Story:', 'oep' ),
			'all_items'             => __( 'All Success Stories', 'oep' ),
			'add_new_item'          => __( 'Add New Success Story', 'oep' ),
			'add_new'               => __( 'Add New', 'oep' ),
			'new_item'              => __( 'New Success Story', 'oep' ),
			'edit_item'             => __( 'Edit Success Story', 'oep' ),
			'update_item'           => __( 'Update Success Story', 'oep' ),
			'view_item'             => __( 'View Success Story', 'oep' ),
			'view_items'            => __( 'View Success Stories', 'oep' ),
			'search_items'          => __( 'Search Success Stories', 'oep' ),
			'not_found'             => __( 'Not found', 'oep' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'oep' ),
			'featured_image'        => __( 'Featured Image', 'oep' ),
			'set_featured_image'    => __( 'Set featured image', 'oep' ),
			'remove_featured_image' => __( 'Remove featured image', 'oep' ),
			'use_featured_image'    => __( 'Use as featured image', 'oep' ),
			'insert_into_item'      => __( 'Insert into success story', 'oep' ),
			'uploaded_to_this_item' => __( 'Uploaded to this success story', 'oep' ),
			'items_list'            => __( 'Success Stories list', 'oep' ),
			'items_list_navigation' => __( 'Success Stories list navigation', 'oep' ),
			'filter_items_list'     => __( 'Filter success stories list', 'oep' ),
		];

		$rewrite = [
			'slug'                  => 'success-stories',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		];

		$args = [
			'label'                 => __( 'Success Story', 'oep' ),
			'description'           => 'Success Stories in and around Orlando',
			'labels'                => $labels,
			'supports'            => [ 'title', 'editor', 'thumbnail', 'revisions', 'author', 'custom-fields' ],
			'taxonomies'            => array( 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-format-status',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'success-stories',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
			'template'              => self::$template_args,
		];

		return $args;
	}

	/**
	 * Displays default set of Gutenberg / post_meta block for posts.
	 * Will be reused for other CPTs
	 *
	 * @TODO  move placeholder / lorem generation somewhere else
	 *
	 * @since 0.2.0
	 */
	public function register_template() {
		$lorem                      = "Cras ac tristique purus, a ultrices turpis. Nam consequat convallis interdum. Ut nisl lacus, ornare nec nisi in, tristique porta purus. Fusce vel risus ante. Etiam imperdiet mauris libero, ac placerat odio placerat eget. Pellentesque sagittis tempus tortor et ornare. Pellentesque iaculis tincidunt lobortis.";
		$post_type_object           = get_post_type_object( OEP_SUCCESS_KEY );
		$post_type_object->template = self::$template_args;
	}

	/**
	 * Set archive title
	 * @param $title
	 *
	 * @return string|void
	 */
	public function set_archive_title( $title ) {
		return is_post_type_archive('oep_cpts_success') ? __("Success Stories", "oep") : $title;
	}

	/**
	 * Set archive description because WordPress autop's the hell out of everything, so annoying
	 * @TODO switch this out with an ACF field in an options page
	 * @param $title
	 *
	 * @return string|void
	 */
	public function set_archive_description( $title ) {
		return is_post_type_archive('oep_cpts_success') ? __( 'After leading the nation in job growth, Orlando is attracting some of the biggest names in business, creating thousands of high-wage, high-tech jobs in a region traditionally known for tourism.', 'oep' ) : $title;
	}

	// /**
	//  * Add options page
	//  *
	//  * @since 1.2.0
	//  */
	// public function add_options_page() {
	//
	// 	$settings = __( 'Settings', 'oep' );
	//
	// 	acf_add_options_sub_page([
	// 		'post_id'     => OEP_SUCCESS_KEY,
	// 		'page_title'  => __('Success') . " $settings",
	// 		'menu_title'  => $settings,
	// 		'parent_slug' => add_query_arg( 'post_type', OEP_SUCCESS_KEY, 'edit.php' ),
	// 		'menu_slug'   => 'success-story-settings',
	// 	]);
	// }


	/**
	 * Run it
	 */
	public function run() {
		//set archive title, figured this is the best place to put this
		$this->loader->add_filter( 'get_the_archive_title', $this, 'set_archive_title', 99, 1);
		$this->loader->add_filter( 'get_the_archive_description', $this, 'set_archive_description', 99, 1);
		$this->loader->run();
	}

	/**
	 * @TODO helper function for generating thumbnails for featured images from video urls
	 *       maybe a filter inside The_post_thumbnail
	 */
}
