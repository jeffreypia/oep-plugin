<?php
/**
 * Created by PhpStorm.
 * User: ryansmith
 * Date: 2019-03-21
 * Time: 12:07
 */

namespace OEP\CPT;
use OEP\Common;
use OEP\Taxonomies;

/**
 * Class Landing Page
 *
 * @package OEP\CPT
 */
class LanPage {

	protected $loader;

	protected static $template_args = array(
		array(
			'oep/hero',
		),
	);

	public function __construct() {
		$this->loader  = new Common\Loader();
		$this->run();
	}

	/**
	 * Get args for CPT
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels = [
			'name'                  => _x( 'Landing Pages', 'Post Type General Name', 'oep' ),
			'singular_name'         => _x( 'Landing Page', 'Post Type Singular Name', 'oep' ),
			'menu_name'             => __( 'Landing Pages', 'oep' ),
			'name_admin_bar'        => __( 'Landing Page', 'oep' ),
			'archives'              => __( 'Landing Page Archives', 'oep' ),
			'attributes'            => __( 'Landing Page Attributes', 'oep' ),
			'parent_item_colon'     => __( 'Parent Landing Page:', 'oep' ),
			'all_items'             => __( 'All Landing Pages', 'oep' ),
			'add_new_item'          => __( 'Add New Landing Page', 'oep' ),
			'add_new'               => __( 'Add New', 'oep' ),
			'new_item'              => __( 'New Landing Page', 'oep' ),
			'edit_item'             => __( 'Edit Landing Page', 'oep' ),
			'update_item'           => __( 'Update Landing Page', 'oep' ),
			'view_item'             => __( 'View Landing Page', 'oep' ),
			'view_items'            => __( 'View Landing Pages', 'oep' ),
			'search_items'          => __( 'Search Landing Page', 'oep' ),
			'not_found'             => __( 'Not found', 'oep' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'oep' ),
			'featured_image'        => __( 'Featured Image', 'oep' ),
			'set_featured_image'    => __( 'Set featured image', 'oep' ),
			'remove_featured_image' => __( 'Remove featured image', 'oep' ),
			'use_featured_image'    => __( 'Use as featured image', 'oep' ),
			'insert_into_item'      => __( 'Insert into landing page', 'oep' ),
			'uploaded_to_this_item' => __( 'Uploaded to this landing page', 'oep' ),
			'items_list'            => __( 'Landing Pages list', 'oep' ),
			'items_list_navigation' => __( 'Landing Pages list navigation', 'oep' ),
			'filter_items_list'     => __( 'Filter landing pages list', 'oep' ),
		];

		$rewrite = [
			'slug'       => 'l',
			'with_front' => false,
			'pages'      => true,
			'feeds'      => true,
		];

		$args = [
			'label'               => __( 'Landing Page', 'oep' ),
			'description'         => __( 'Custom Landing Page Builder.', 'oep' ),
			'labels'              => $labels,
			'supports'            => [ 'title', 'editor', 'thumbnail', 'revisions', 'author', 'custom-fields' ],
			'taxonomies'          => array( 'post_tag' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-admin-page',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'page',
			'show_in_rest'        => true,
			'template'            => self::$template_args,
		];

		return $args;
	}

	/**
	 * Render landing page slider or title / menu block, depending on user
	 * selections. You can have either one or the other, but never both.
	 *
	 * @since 0.2.0
	 */
	public function render_header() {

		global $post;

		$slider_ids = get_post_meta( $post->ID, '_oep_selected_slides', false );

		if ( ! $slider_ids || empty( $slider_ids ) ) {

			// check for title / menu block
			$blocks = parse_blocks( $post->post_content );

			// find the block
			$title_menu_block_array = array_filter( $blocks, function ( $blocks_to_filter ) {
				return $blocks_to_filter['blockName'] == 'oep/title-menu';
			} );

			// did we find it?
			if ( ! empty( $title_menu_block_array ) ) {
				$title_menu_block = reset( $title_menu_block_array );

				/*
				 * we're gonna need this later for the recent posts full page listing if it exists
				 */
				global $landing_page_terms;

				$landing_page_terms = $title_menu_block['attrs']['terms'];

				usort( $landing_page_terms, function( $a, $b ) {
					return strcmp( get_term( $a["term_id"], $a["taxonomy"] )->name, get_term( $b["term_id"], $b["taxonomy"] )->name );
				});

				$description = $title_menu_block['attrs']['description'];
				// $term_menu = \OEP\Taxonomies\Taxonomy\get_tax_terms;

				Common\oep_fn_template_part( 'title-menu', '', array(
					'terms' => $landing_page_terms,
					'description' => $description
				));
			}

			return; // bounce
		}

		$sliders_query_args = array(
			'posts_per_page'      => 20,
			'ignore_sticky_posts' => true,
			'post_type'           => array_merge( [ 'post' ], PostType::get_cpt_keys() ),
			'post__in'            => is_array( $slider_ids ) ? explode( ',', $slider_ids[0] ) : explode( ',', $slider_ids ),
			'orderby'             => 'post__in',
		);
		$sliders_query = new \WP_Query(( $sliders_query_args ));

		Common\oep_fn_template_part( 'post', 'slider', array( 'sliders_query' => $sliders_query ) );

		wp_reset_query();
	}

	/**
	 * Add landing pages to list of possible static home pages
	 * @param $pages
	 * @param $r
	 *
	 * @return array
	 */

	public function add_landing_pages_to_dropdown( $pages, $r ){
		if(isset( $r['name']) && 'page_on_front' == $r['name']){
			$args = array(
				'post_type' => OEP_LANDING_PAGE_KEY
			);
			$landing_pages = get_posts($args);
			$pages = array_merge($pages, $landing_pages);
		}

		return $pages;
	}


	/**
	 * Add landing pages to list of post types available for the front page
	 * @param $query
	 */
	public function enable_front_page_landing_pages( $query ){
		if ( $query->is_main_query() ) {
			$post_type = $query->get( 'post_type' );
			$page_id = $query->get( 'page_id' );
			if ( empty( $post_type ) && ! empty( $page_id ) ) {
				$query->set( 'post_type', get_post_type( $page_id ) );
			}
		}
	}

	public function add_classes( $classes ) {
		if(is_front_page()){
			$landing_page_classes = array("single", "single-oep_cpts_lan-page");
			return array_merge( $classes, $landing_page_classes);
		}
		return $classes;
	}

	/**
	 * Displays default set of Gutenberg / post_meta block for posts.
	 * Will be reused for other CPTs
	 *
	 * @TODO  move placeholder / lorem generation somewhere else
	 *
	 * @since 0.2.0
	 */
	public function register_template() {
		$post_type_object           = get_post_type_object( OEP_LANDING_PAGE_KEY );
		$post_type_object->template = self::$template_args;
	}

	/**
	 * Run it
	 */
	public function run() {
		$this->loader->add_action( 'oep_header'	, $this, 'render_header', 99, 1 );
		$this->loader->add_filter( 'body_class'	, $this, 'add_classes', 10, 1 );
		$this->loader->add_filter( 'get_pages'	, $this, 'add_landing_pages_to_dropdown', 10, 2 );
		$this->loader->add_action( 'pre_get_posts'	, $this, 'enable_front_page_landing_pages', 999, 1 );
		$this->loader->run();
	}
}