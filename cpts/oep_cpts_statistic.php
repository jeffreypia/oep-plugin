<?php
/**
 * Created by PhpStorm.
 * User: leisyvidal
 * Date: 2019-08-27
 * Time: 11:40
 */

namespace OEP\CPT;
use OEP\Common;

/**
 * Class Statistic
 *
 * @package OEP\CPT
 */
class Statistic {

	protected $loader;

	/**
	 * @var array
	 */
	protected static $template_args = array(
		array(
			'core/paragraph',
		)
	);

	public function __construct() {
		$this->loader  = new Common\Loader();
		$this->run();
	}

	/**
	 * Get args for CPT
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels = [
			'name'                  => _x( 'Statistics', 'Post Type General Name', 'oep' ),
			'singular_name'         => _x( 'Statistic', 'Post Type Singular Name', 'oep' ),
			'menu_name'             => __( 'Statistic', 'oep' ),
			'name_admin_bar'        => __( 'Statistics', 'oep' ),
			'archives'              => __( 'Statistic Archives', 'oep' ),
			'attributes'            => __( 'Statistic Attributes', 'oep' ),
			'parent_item_colon'     => __( 'Parent Statistic:', 'oep' ),
			'all_items'             => __( 'All Statistics', 'oep' ),
			'add_new_item'          => __( 'Add New Statistic', 'oep' ),
			'add_new'               => __( 'Add New', 'oep' ),
			'new_item'              => __( 'New Statistic', 'oep' ),
			'edit_item'             => __( 'Edit Statistic', 'oep' ),
			'update_item'           => __( 'Update Statistic', 'oep' ),
			'view_item'             => __( 'View Statistic', 'oep' ),
			'view_items'            => __( 'View Statistics', 'oep' ),
			'search_items'          => __( 'Search Statistics', 'oep' ),
			'not_found'             => __( 'Not found', 'oep' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'oep' ),
			'featured_image'        => __( 'Featured Image', 'oep' ),
			'set_featured_image'    => __( 'Set featured image', 'oep' ),
			'remove_featured_image' => __( 'Remove featured image', 'oep' ),
			'use_featured_image'    => __( 'Use as featured image', 'oep' ),
			'insert_into_item'      => __( 'Insert into statistic', 'oep' ),
			'uploaded_to_this_item' => __( 'Uploaded to this statistic', 'oep' ),
			'items_list'            => __( 'Statistics list', 'oep' ),
			'items_list_navigation' => __( 'Statistics list navigation', 'oep' ),
			'filter_items_list'     => __( 'Filter statistics list', 'oep' ),
		];

		$rewrite = [
			'slug'                  => 'statistics',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		];

		$args = [
			'label'                 => __( 'Statistic', 'oep' ),
			'description'           => __( 'Statistics relevant to the Orlando region.', 'oep' ),
			'labels'                => $labels,
			'supports'              => [ 'title', 'editor', 'thumbnail', 'revisions', 'author' ],
			'taxonomies'            => array( 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5.1,
			'menu_icon'             => 'dashicons-awards',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'statsistics',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
			'template'              => self::$template_args,
		];

		return $args;
	}

	/**
	 * Displays default set of Gutenberg / post_meta block for posts.
	 * Will be reused for other CPTs
	 *
	 * @TODO  move placeholder / lorem generation somewhere else
	 *
	 * @since 0.2.0
	 */
	public function register_template() {
		$lorem                      = "Cras ac tristique purus, a ultrices turpis. Nam consequat convallis interdum. Ut nisl lacus, ornare nec nisi in, tristique porta purus. Fusce vel risus ante. Etiam imperdiet mauris libero, ac placerat odio placerat eget. Pellentesque sagittis tempus tortor et ornare. Pellentesque iaculis tincidunt lobortis.";
		$post_type_object           = get_post_type_object( OEP_STATISTIC_KEY );
		$post_type_object->template = self::$template_args;
	}


	public function set_statistic_title( $title ) {
		//change title placeholder
		$screen = get_current_screen();

		if ( OEP_STATISTIC_KEY == $screen->post_type ) {
			$title = 'Statistic';
		}

		return $title;
	}

	/**
	 * Run it
	 */
	public function run() {
		$this->loader->add_filter( 'enter_title_here'	, $this, 'set_statistic_title', 99, 1 );
		$this->loader->run();
	}

}
