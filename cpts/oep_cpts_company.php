<?php
/**
 * Created by PhpStorm.
 * User: ryansmith
 * Date: 2019-03-21
 * Time: 12:07
 */

namespace OEP\CPT;
use OEP\Common;

/**
 * Class Company
 *
 * @package OEP\CPT
 */
class Company {

	/**
	 * Order key (native)
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $orderby_key = 'orderby';
	/**
	 * Order options
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private static $orderby_options;
	/**
	 * Selected order value
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $orderby = '';
	/**
	 * Searching "all media" flag
	 *
	 * @var   boolean
	 * @since 1.0.0
	 */

	protected $loader;

	/**
	 * @var array
	 */
	protected static $template_args = array(
		array(
			'core/paragraph',
		)
	);

	public function __construct() {
		$this->loader  = new Common\Loader();
		$this->run();
	}

	/**
	 * Get args for CPT
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels = [
			'name'                  => _x( 'Companies', 'Post Type General Name', 'oep' ),
			'singular_name'         => _x( 'Company', 'Post Type Singular Name', 'oep' ),
			'menu_name'             => __( 'Companies', 'oep' ),
			'name_admin_bar'        => __( 'Company', 'oep' ),
			'archives'              => __( 'Company Archives', 'oep' ),
			'attributes'            => __( 'Company Attributes', 'oep' ),
			'parent_item_colon'     => __( 'Parent Company:', 'oep' ),
			'all_items'             => __( 'All Companies', 'oep' ),
			'add_new_item'          => __( 'Add New Company', 'oep' ),
			'add_new'               => __( 'Add New', 'oep' ),
			'new_item'              => __( 'New Company', 'oep' ),
			'edit_item'             => __( 'Edit Company', 'oep' ),
			'update_item'           => __( 'Update Company', 'oep' ),
			'view_item'             => __( 'View Company', 'oep' ),
			'view_items'            => __( 'View Companies', 'oep' ),
			'search_items'          => __( 'Search Company', 'oep' ),
			'not_found'             => __( 'Not found', 'oep' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'oep' ),
			'featured_image'        => __( 'Featured Image', 'oep' ),
			'set_featured_image'    => __( 'Set featured image', 'oep' ),
			'remove_featured_image' => __( 'Remove featured image', 'oep' ),
			'use_featured_image'    => __( 'Use as featured image', 'oep' ),
			'insert_into_item'      => __( 'Insert into company', 'oep' ),
			'uploaded_to_this_item' => __( 'Uploaded to this company', 'oep' ),
			'items_list'            => __( 'Companies list', 'oep' ),
			'items_list_navigation' => __( 'Companies list navigation', 'oep' ),
			'filter_items_list'     => __( 'Filter companies list', 'oep' ),
		];

		$rewrite = [
			'slug'                  => 'company',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		];

		$args = [
			'label'                 => __( 'Company', 'oep' ),
			'description'           => __( 'Companies relevant to the Orlando region.', 'oep' ),
			'labels'                => $labels,
			'supports'              => [ 'title', 'editor', 'thumbnail', 'revisions', 'author' ],
			'taxonomies'            => array( 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5.1,
			'menu_icon'             => 'dashicons-building',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'companies',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
			'template'              => self::$template_args,
		];

		return $args;
	}

	/**
	 * Displays default set of Gutenberg / post_meta block for posts.
	 * Will be reused for other CPTs
	 *
	 * @TODO  move placeholder / lorem generation somewhere else
	 *
	 * @since 0.2.0
	 */
	public function register_template() {
		$lorem                      = "Cras ac tristique purus, a ultrices turpis. Nam consequat convallis interdum. Ut nisl lacus, ornare nec nisi in, tristique porta purus. Fusce vel risus ante. Etiam imperdiet mauris libero, ac placerat odio placerat eget. Pellentesque sagittis tempus tortor et ornare. Pellentesque iaculis tincidunt lobortis.";
		$post_type_object           = get_post_type_object( OEP_COMPANY_KEY );
		$post_type_object->template = self::$template_args;
	}

	/**
	 * Add company filters (size, industry and region) to archive page.
	 *
	 * @since 1.1.0
	 */
	public function render_filters() {
		if( get_post_type() != OEP_COMPANY_KEY ) {
			return;
		}

		$args = [
			'orderby_key'     => self::$orderby_key,
			'orderby_options' => self::$orderby_options,
			'orderby'         => self::$orderby,
		];
		ob_start();
		Common\oep_fn_template_part( 'company', 'filter', $args );
		echo ob_get_clean();
	}

	/**
	 * Set query vars to taxonomy filter values on archive page using ajax_query_vars hook.
	 * @param $query_vars
	 *
	 * @return mixed
	 */
	public function set_ajax_query_vars( $query_vars ) {
		error_log( json_encode($_POST) );
		$query_vars['oep_taxonomies_industries'] = $_POST['oep_taxonomies_industries'];
		$query_vars['oep_taxonomies_size'] = $_POST['oep_taxonomies_size'];
		$query_vars['oep_taxonomies_regions'] = $_POST['oep_taxonomies_regions'];
		return $query_vars;
	}

	/**
	 * Changes title placeholder on company edit page.
	 *
	 * @param $title
	 *
	 * @return string
	 */
	public function set_company_title( $title ) {
		//change title placeholder
		$screen = get_current_screen();

		if ( OEP_COMPANY_KEY == $screen->post_type ) {
			$title = 'Company Name';
		}

		return $title;
	}

	/**
	 * Run it
	 */
	public function run() {
		$this->loader->add_filter( 'enter_title_here'	, $this, 'set_company_title', 99, 1 );
		$this->loader->add_filter( 'ajax_query_vars'	, $this, 'set_ajax_query_vars', 99, 1 );
		$this->loader->add_action( 'company_filter_form', $this, 'render_filters', 99 );
		$this->loader->add_action( 'oep_archive_loop'	, $this, 'render_filters', 20 );
		$this->loader->run();
	}

}
