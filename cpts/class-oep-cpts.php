<?php

/**
 * Add custom post types via the plugin.
 *
 * @link       https://designzillas.com
 * @since      0.1.0
 *
 * @package    OEP
 * @subpackage OEP/taxonomies
 */

/**
 * Add and register multiple custom post types, as well as define common functionality for CPTs
 *
 * @package    OEP
 * @subpackage OEP/cpts
 * @author     Ryan Smith <ryan@designzillas.com>
 */

namespace OEP\CPT;

use OEP\Common;

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'OEP_LOREM', "Cras ac tristique purus, a ultrices turpis. Nam consequat convallis interdum. Ut nisl lacus, ornare nec nisi in, tristique porta purus. Fusce vel risus ante. Etiam imperdiet mauris libero, ac placerat odio placerat eget. Pellentesque sagittis tempus tortor et ornare. Pellentesque iaculis tincidunt lobortis." );


class PostType {

	/**
	 * CPT keys
	 *
	 * @since       0.1.0
	 * @access      protected
	 * @var         array $tax_keys key / string
	 */
	protected static $cpt_keys = array(
		'OEP_EVENT_KEY'         => 'oep_cpts_event',
		'OEP_PROFILE_KEY'       => 'oep_cpts_profile',
		'OEP_VIDEO_KEY'         => 'oep_cpts_video',
		'OEP_PODCAST_KEY'   	=> 'oep_cpts_podcast',
		'OEP_LANDING_PAGE_KEY'  => 'oep_cpts_lan-page', //cannot exceed 20 characters, ugh
		'OEP_SUCCESS_KEY'       => 'oep_cpts_success',
		'OEP_COMPANY_KEY'       => 'oep_cpts_company',
		'OEP_STATISTIC_KEY'     => 'oep_cpts_statistic',
		'OEP_HOOD_KEY'          => 'oep_cpts_hood',
		'OEP_DOWNLOAD_KEY'      => 'oep_cpts_download',
	);


	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      Loader $loader Maintains and registers all hooks for the plugin.
	 */
	protected $loader;
	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $oep The ID of this plugin.
	 */
	protected $oep;
	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	protected $version;
	/**
	 * CPT classes array for invoking registration
	 *
	 * @since       0.1.0
	 * @access      protected
	 * @var         array $tax_classes key / string
	 */
	protected $cpt_classes = array();

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 *
	 * @param      string $oep     The name of this plugin.
	 * @param      string $version The version of this plugin.
	 *
	 * /**
	 * Initialize CPTs and properties
	 *
	 * @since    0.1.0
	 */
	public function __construct( $oep, $version ) {
		$this->oep     = $oep;
		$this->version = $version;
		$this->loader  = new Common\Loader();
		$this->define_keys();
		$this->load_cpts();
		$this->cpt_hooks();
		$this->run();
	}

	/**
	 * Define keys
	 *
	 * @since 0.1.0
	 */
	private function define_keys() {
		$keys = $this->get_cpt_keys();
		foreach ( $keys as $key => $value ) {
			define( $key, $value );
		}
	}

	/**
	 * Get all CPT keys
	 *
	 * @return array
	 */
	public static function get_cpt_keys() {
		return self::$cpt_keys;
	}

	/**
	 * Load CPT arguments
	 *
	 * @since 0.1.0
	 */
	public function load_cpts() {
		$keys = $this->get_cpt_keys();
		foreach ( $keys as $key => $value ) {
			if ( file_exists( OEP_PLUGIN_DIR . 'cpts/' . $value . '.php' ) ) {
				require_once OEP_PLUGIN_DIR . 'cpts/' . $value . '.php';
			}
		}
	}

	/**
	 * Define CPT hooks
	 *
	 * @since 0.2.0
	 */

	public function cpt_hooks() {
		$this->loader->add_action( 'init'					, $this, 'register_template' );
		$this->loader->add_action( 'related_post'			, $this, 'related_posts' );
		$this->loader->add_action( 'weather'				, $this, 'weather' );
		$this->loader->add_action( 'related_stories'		, $this, 'related_stories' );
		$this->loader->add_action( 'featured_post'			, $this, 'featured_posts' );
		$this->loader->add_action( 'newsletter'			    , $this, 'newsletter' );
		$this->loader->add_action( 'widgets_init'		    , $this, 'register_newsletter_widget' );
		$this->loader->add_action( 'recommended_stories'	, $this, 'render_recommended_stories' );
		$this->loader->add_action( 'media_thumbnail'		, $this, 'set_media_thumb', 99, 1 );
		$this->loader->add_action( 'upcoming_events'		, $this, 'upcoming_events' );
		$this->loader->add_action( 'recent_events'			, $this, 'recent_events' );
		$this->loader->add_action( 'trending_stories'		, $this, 'trending_stories' );
	}

	/**
	 * Run it
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * Run related posts blocks
	 *
	 * @TODO  move this out of the class into another file that doesn't get instantiated right away and throws ridiculous errors
	 * @since 0.2.0
	 */

	public function related_posts() {
		if ( is_admin() ) {
			return;
		}
		Common\oep_fn_template_part( 'related_posts' );
		wp_reset_postdata();
	}

	/**
	 * Run related stories blocks
	 *
	 * @TODO  move this out of the class into another file that doesn't get instantiated right away and throws ridiculous errors
	 * @since 0.2.0
	 */

	public function related_stories() {
		if ( is_admin() ) {
			return;
		}
		Common\oep_fn_template_part( 'related_stories' );
	}

	/**
	 * Run newsletter block
	 *
	 * @TODO  move this out of the class into another file that doesn't get instantiated right away and throws ridiculous errors
	 * @since 0.2.0
	 */

	public function newsletter() {
		if ( is_admin() ) {
			return;
		}
		Common\oep_fn_template_part( 'newsletter' );
	}

	/**
	 * Register newsetter widget
	 *
	 * @since 0.8.0
	 */

	public function register_newsletter_widget() {
		register_widget( new Common\Newsletter_Widget());
		register_sidebar( array(
			'name' => 'Sidebar',
			'id' => 'sidebar',
			'before_widget' => '<div>',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
	}

	/**
	 * Run featured posts blocks
	 *
	 * @TODO  move this out of the class into another file that doesn't get instantiated right away and throws ridiculous errors
	 * @since 0.2.0
	 */

	public function featured_posts() {
		if ( is_admin() ) {
			return;
		}
		Common\oep_fn_template_part( 'featured_posts' );
		wp_reset_postdata();
	}

	/**
	 * Render recommended stories
	 *
	 * @since 0.2.0
	 */

	public function render_recommended_stories(){
		global $post;
		$recommended_post_ids = get_post_meta($post->ID, '_oep_recommended_stories', true);
		if(!$recommended_post_ids || empty( $recommended_post_ids ) || !is_single()){
			return; //nothing to see here, bounce
		}

		$recommended_posts_query_args = array(
			'ignore_sticky_posts' => true,
			'post_type'           => array_merge( [ 'post' ], self::get_cpt_keys()),
			'post__in' => explode( ',', $recommended_post_ids),
		);

		$recommended_posts_query = new \WP_Query(($recommended_posts_query_args));

		Common\oep_fn_template_part( 'recommended-stories', '', array( 'recommended_posts_query' => $recommended_posts_query ) );
	}

	/**
	 * Run featured recent events block for single events
	 *
	 * @TODO  move this out of the class into another file that doesn't get instantiated right away and throws ridiculous errors
	 * @since 0.2.0
	 */

	public function recent_events() {
		if ( is_admin() ) {
			return;
		}
		Common\oep_fn_template_part( 'recent_events' );
		wp_reset_postdata();
	}

	/**
	 * Run featured upcoming events blocks
	 *
	 * @TODO  move this out of the class into another file that doesn't get instantiated right away and throws ridiculous errors
	 * @since 0.2.0
	 */

	public function upcoming_events() {
		if ( is_admin() ) {
			return;
		}
		Common\oep_fn_template_part( 'upcoming_events' );
		wp_reset_postdata();
	}


	/**
	 * Weather widget
	 *
	 * @TODO  move this out of the class into another file that doesn't get instantiated right away and throws ridiculous errors
	 * @since 0.2.0
	 */

	public function weather() {
		if ( is_admin() ) {
			return;
		}
		Common\oep_fn_template_part( 'weather' );
	}

	/**
	 * Run featured upcoming events blocks
	 *
	 * @TODO  move this out of the class into another file that doesn't get instantiated right away and throws ridiculous errors
	 * @since 0.2.0
	 */

	public function trending_stories() {
		if ( is_admin() ) {
			echo 'admin';
			return;
		}
		Common\oep_fn_template_part( 'trending_stories' );
	}

	/**
	 * Set the thumb for media related posts for use in sidebars and content areas
	 * @param $media_post \WP_Post
	 * @return mixed
	 */
	public function set_media_thumb( $media_post ) {
		if($media_post->post_type == OEP_VIDEO_KEY){
			$blocks = parse_blocks( $media_post->post_content);
			foreach ($blocks as $block) {
				if( $block['blockName'] == 'core/video' || $block['blockName'] == 'core-embed/vimeo' || $block['blockName'] == 'core-embed/youtube' ){
//				if($block['attrs']['type'] == 'video'){
					$thumb = render_block( $block );
					echo apply_filters( 'the_content', $thumb);
					return;
				}
			}
		}
	}

	/**
	 * Register CPTs
	 *
	 * @since   0.1.0
	 */
	public function register() {
		//register custom post types
		$cpts = $this->get_cpt_keys();
		foreach ( $cpts as $cpt => $value ) {
			/*
			 * @TODO move this to a helper function since we'll probably need to do this for CPTs as well.
			 */
			$filename_to_array = explode( '_', constant( $cpt ) );
			$class_name        = end( $filename_to_array );
			//$cpt_class         = __NAMESPACE__ . '\\' . ucfirst( $class_name );
			$cpt_class = __NAMESPACE__ . '\\' . str_replace( ' ', '', ucwords( str_replace( [
					'-',
					'_',
				], ' ', $class_name ) ) );
			$a         = new $cpt_class;
			$success   = register_post_type( constant( $cpt ), $a::get_args() );

			//register templates for cpts if any
			if ( method_exists( $a, 'register_template' ) ) {
				$cpt_loader = new Common\Loader();
				$cpt_loader->add_action( 'init', $a, 'register_template' );
				$cpt_loader->run();
			}
		}
	}

	/**
	 * Displays default set of Gutenberg / post_meta block for posts.
	 * Will be reused for other CPTs
	 *
	 * @TODO  move placeholder / lorem generation somewhere else
	 *
	 * @since 0.2.0
	 */
	public function register_template() {
		$post_type_object           = get_post_type_object( 'post' );
		$post_type_object->template = array(
			array(
				'core/paragraph',
				array(
					'placeholder' => 'Excerpt...',
					'className'   => 'detail',
				),
			),
			array( 'core/image' ),
			array(
				'core/heading',
				array(
					'placeholder' => 'Section Title',
//					'level'       => '3',

				),
			),
			array(
				'core/paragraph',
				array(
					'placeholder' => OEP_LOREM,
				),
			),
			array( 'oep/recommendedstories' ),
		);
	}
}
