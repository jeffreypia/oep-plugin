<?php
/**
 * Created by PhpStorm.
 * User: ryansmith
 * Date: 2019-03-21
 * Time: 12:07
 */

namespace OEP\CPT;


/**
 * Class Podcast
 *
 * @package OEP\CPT
 */
class Podcast {

	/**
	 * @var array
	 */
	protected static $template_args = array(
		array(
			'core/audio',
		),
		array(
			'oep/recommendedstories'
		),
	);

	/**
	 * Get args for CPT
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels = [
			'name'                  => _x( 'Podcasts', 'Post Type General Name', 'oep' ),
			'singular_name'         => _x( 'Podcast', 'Post Type Singular Name', 'oep' ),
			'menu_name'             => __( 'Podcasts', 'oep' ),
			'name_admin_bar'        => __( 'Podcast', 'oep' ),
			'archives'              => __( 'Podcast Archives', 'oep' ),
			'attributes'            => __( 'Podcast Attributes', 'oep' ),
			'parent_item_colon'     => __( 'Parent Podcast:', 'oep' ),
			'all_items'             => __( 'All Podcasts', 'oep' ),
			'add_new_item'          => __( 'Add New Podcast', 'oep' ),
			'add_new'               => __( 'Add New', 'oep' ),
			'new_item'              => __( 'New Podcast', 'oep' ),
			'edit_item'             => __( 'Edit Podcast', 'oep' ),
			'update_item'           => __( 'Update Podcast', 'oep' ),
			'view_item'             => __( 'View Podcast', 'oep' ),
			'view_items'            => __( 'View Podcasts', 'oep' ),
			'search_items'          => __( 'Search Podcast', 'oep' ),
			'not_found'             => __( 'Not found', 'oep' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'oep' ),
			'featured_image'        => __( 'Featured Image', 'oep' ),
			'set_featured_image'    => __( 'Set featured image', 'oep' ),
			'remove_featured_image' => __( 'Remove featured image', 'oep' ),
			'use_featured_image'    => __( 'Use as featured image', 'oep' ),
			'insert_into_item'      => __( 'Insert into podcast', 'oep' ),
			'uploaded_to_this_item' => __( 'Uploaded to this podcast', 'oep' ),
			'items_list'            => __( 'Podcasts list', 'oep' ),
			'items_list_navigation' => __( 'Podcasts list navigation', 'oep' ),
			'filter_items_list'     => __( 'Filter podcasts list', 'oep' ),
		];

		$rewrite = [
			'slug'                  => 'podcast',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		];

		$args = [
			'label'                 => __( 'Podcast', 'oep' ),
			'description'           => __( 'Podcast posts.', 'oep' ),
			'labels'                => $labels,
			'supports'              => [ 'title', 'thumbnail', 'editor', 'author' ],
			'taxonomies'            => [ 'post_tag' ],
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-rss',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'podcasts',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
			'template'              => self::$template_args,
		];

		return $args;
	}

	/**
	 * Displays default set of Gutenberg / post_meta block for posts.
	 * Will be reused for other CPTs
	 *
	 * @TODO  move placeholder / lorem generation somewhere else
	 *
	 * @since 0.2.0
	 */
	public function register_template() {
		$lorem                      = "Cras ac tristique purus, a ultrices turpis. Nam consequat convallis interdum. Ut nisl lacus, ornare nec nisi in, tristique porta purus. Fusce vel risus ante. Etiam imperdiet mauris libero, ac placerat odio placerat eget. Pellentesque sagittis tempus tortor et ornare. Pellentesque iaculis tincidunt lobortis.";
		$post_type_object           = get_post_type_object( OEP_PODCAST_KEY );
		$post_type_object->template = array(
			array(
				'core/paragraph',
				array(
					'placeholder' => 'Excerpt...',
				),
			),
		);
	}
}