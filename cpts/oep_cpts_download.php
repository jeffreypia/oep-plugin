<?php
/**
 * Created by PhpStorm.
 * User: leisyvidal
 * Date: 2019-08-27
 * Time: 1:00
 */

namespace OEP\CPT;
use OEP\Common;

/**
 * Class Download
 *
 * @package OEP\CPT
 */
class Download {

	protected $loader;

	/**
	 * @var array
	 */
	protected static $template_args = array(
		array(
			'core/paragraph',
		)
	);

	public function __construct() {
		$this->loader  = new Common\Loader();
		$this->run();
	}

	/**
	 * Get args for CPT
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels = [
			'name'                  => _x( 'Downloadable', 'Post Type General Name', 'oep' ),
			'singular_name'         => _x( 'Downloadable', 'Post Type Singular Name', 'oep' ),
			'menu_name'             => __( 'Downloadables', 'oep' ),
			'name_admin_bar'        => __( 'Downloadable', 'oep' ),
			'archives'              => __( 'Downloadable Archives', 'oep' ),
			'attributes'            => __( 'Downloadable Attributes', 'oep' ),
			'parent_item_colon'     => __( 'Parent Downloadable:', 'oep' ),
			'all_items'             => __( 'All Downloadables', 'oep' ),
			'add_new_item'          => __( 'Add New Downloadable', 'oep' ),
			'add_new'               => __( 'Add New', 'oep' ),
			'new_item'              => __( 'New Downloadable', 'oep' ),
			'edit_item'             => __( 'Edit Downloadable', 'oep' ),
			'update_item'           => __( 'Update Downloadable', 'oep' ),
			'view_item'             => __( 'View Downloadable', 'oep' ),
			'view_items'            => __( 'View Downloadables', 'oep' ),
			'search_items'          => __( 'Search Downloadables', 'oep' ),
			'not_found'             => __( 'Not found', 'oep' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'oep' ),
			'featured_image'        => __( 'Featured Image', 'oep' ),
			'set_featured_image'    => __( 'Set featured image', 'oep' ),
			'remove_featured_image' => __( 'Remove featured image', 'oep' ),
			'use_featured_image'    => __( 'Use as featured image', 'oep' ),
			'insert_into_item'      => __( 'Insert into downloadable', 'oep' ),
			'uploaded_to_this_item' => __( 'Uploaded to this downloadable', 'oep' ),
			'items_list'            => __( 'Downloadables list', 'oep' ),
			'items_list_navigation' => __( 'Downloadables list navigation', 'oep' ),
			'filter_items_list'     => __( 'Filter downloadables list', 'oep' ),
		];

		$rewrite = [
			'slug'                  => 'downloadables',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		];

		$args = [
			'label'                 => __( 'Downloadable', 'oep' ),
			'description'           => __( 'Downloadables relevant to the Orlando region.', 'oep' ),
			'labels'                => $labels,
			'supports'              => [ 'title', 'editor', 'thumbnail', 'revisions', 'author' ],
			'taxonomies'            => array( 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5.1,
			'menu_icon'             => 'dashicons-download',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'downloadables',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
			'template'              => self::$template_args,
		];

		return $args;
	}

	/**
	 * Displays default set of Gutenberg / post_meta block for posts.
	 * Will be reused for other CPTs
	 *
	 * @TODO  move placeholder / lorem generation somewhere else
	 *
	 * @since 0.2.0
	 */
	public function register_template() {
		$lorem                      = "Cras ac tristique purus, a ultrices turpis. Nam consequat convallis interdum. Ut nisl lacus, ornare nec nisi in, tristique porta purus. Fusce vel risus ante. Etiam imperdiet mauris libero, ac placerat odio placerat eget. Pellentesque sagittis tempus tortor et ornare. Pellentesque iaculis tincidunt lobortis.";
		$post_type_object           = get_post_type_object( OEP_DOWNLOAD_KEY );
		$post_type_object->template = self::$template_args;
	}


	public function set_company_title( $title ) {
		//change title placeholder
		$screen = get_current_screen();

		if ( OEP_DOWNLOAD_KEY == $screen->post_type ) {
			$title = 'Downloadable Name';
		}

		return $title;
	}

	/**
	 * Run it
	 */
	public function run() {
		$this->loader->add_filter( 'enter_title_here'	, $this, 'set_company_title', 99, 1 );
		$this->loader->run();
	}

}
