<?php
/**
 * Created by PhpStorm.
 * User: ryansmith
 * Date: 2019-03-21
 * Time: 12:07
 */

namespace OEP\CPT;


/**
 * Class Event
 *
 * @package OEP\CPT
 */
class Video {

	/**
	 * @var array
	 */
	protected static $template_args = array(
		array(
			'core/video',
		),
		array(
			'oep/recommendedstories'
		),
	);

	/**
	 * Get args for CPT
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels = [
			'name'                  => _x( 'Videos', 'Post Type General Name', 'oep' ),
			'singular_name'         => _x( 'Video', 'Post Type Singular Name', 'oep' ),
			'menu_name'             => __( 'Videos', 'oep' ),
			'name_admin_bar'        => __( 'Video', 'oep' ),
			'archives'              => __( 'Video Archives', 'oep' ),
			'attributes'            => __( 'Video Attributes', 'oep' ),
			'parent_item_colon'     => __( 'Parent Video:', 'oep' ),
			'all_items'             => __( 'All Videos', 'oep' ),
			'add_new_item'          => __( 'Add New Video', 'oep' ),
			'add_new'               => __( 'Add New', 'oep' ),
			'new_item'              => __( 'New Video', 'oep' ),
			'edit_item'             => __( 'Edit Video', 'oep' ),
			'update_item'           => __( 'Update Video', 'oep' ),
			'view_item'             => __( 'View Video', 'oep' ),
			'view_items'            => __( 'View Videos', 'oep' ),
			'search_items'          => __( 'Search Video', 'oep' ),
			'not_found'             => __( 'Not found', 'oep' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'oep' ),
			'featured_image'        => __( 'Featured Image', 'oep' ),
			'set_featured_image'    => __( 'Set featured image', 'oep' ),
			'remove_featured_image' => __( 'Remove featured image', 'oep' ),
			'use_featured_image'    => __( 'Use as featured image', 'oep' ),
			'insert_into_item'      => __( 'Insert into video', 'oep' ),
			'uploaded_to_this_item' => __( 'Uploaded to this video', 'oep' ),
			'items_list'            => __( 'Videos list', 'oep' ),
			'items_list_navigation' => __( 'Videos list navigation', 'oep' ),
			'filter_items_list'     => __( 'Filter videos list', 'oep' ),
		];

		$rewrite = [
			'slug'                  => 'video',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		];

		$args = [
			'label'                 => __( 'Video', 'oep' ),
			'description'           => __( 'Video files.', 'oep' ),
			'labels'                => $labels,
			'supports'              => [ 'title', 'editor', 'thumbnail', 'revisions', 'author', 'custom-fields' ],
			'taxonomies'            => array( 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-format-video',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'videos',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
			'template'              => self::$template_args,
		];

		return $args;
	}

	/**
	 * Displays default set of Gutenberg / post_meta block for posts.
	 * Will be reused for other CPTs
	 *
	 * @TODO  move placeholder / lorem generation somewhere else
	 *
	 * @since 0.2.0
	 */
	public function register_template() {
		$lorem                      = "Cras ac tristique purus, a ultrices turpis. Nam consequat convallis interdum. Ut nisl lacus, ornare nec nisi in, tristique porta purus. Fusce vel risus ante. Etiam imperdiet mauris libero, ac placerat odio placerat eget. Pellentesque sagittis tempus tortor et ornare. Pellentesque iaculis tincidunt lobortis.";
		$post_type_object           = get_post_type_object( OEP_VIDEO_KEY );
		$post_type_object->template = self::$template_args;
	}

	/**
	 * @TODO helper function for generating thumbnails for featured images from video urls
	 *       maybe a filter inside The_post_thumbnail
	 */
}