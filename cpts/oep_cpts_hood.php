<?php
/**
 * Created by PhpStorm.
 * User: leisyvidal
 * Date: 2019-08-27
 * Time: 11:25
 */

namespace OEP\CPT;
use OEP\Common;

/**
 * Class Neighborhood
 *
 * @package OEP\CPT
 */
class Hood {

	protected $loader;

	/**
	 * @var array
	 */
	protected static $template_args = array(
		array(
			'core/paragraph',
			array(
				'placeholder' => OEP_LOREM,
			),
		),
	);

	public function __construct() {
		$this->loader  = new Common\Loader();
		$this->run();
	}

	/**
	 * Get args for CPT
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels = [
			'name'                  => _x( 'Neighborhood', 'Post Type General Name', 'oep' ),
			'singular_name'         => _x( 'Neighborhood', 'Post Type Singular Name', 'oep' ),
			'menu_name'             => __( 'Neighborhoods', 'oep' ),
			'name_admin_bar'        => __( 'Neighborhood', 'oep' ),
			'archives'              => __( 'Neighborhood Archives', 'oep' ),
			'attributes'            => __( 'Neighborhood Attributes', 'oep' ),
			'parent_item_colon'     => __( 'Parent Neighborhood:', 'oep' ),
			'all_items'             => __( 'All Neighborhoods', 'oep' ),
			'add_new_item'          => __( 'Add New Neighborhood', 'oep' ),
			'add_new'               => __( 'Add New', 'oep' ),
			'new_item'              => __( 'New Neighborhood', 'oep' ),
			'edit_item'             => __( 'Edit Neighborhood', 'oep' ),
			'update_item'           => __( 'Update Neighborhood', 'oep' ),
			'view_item'             => __( 'View Neighborhood', 'oep' ),
			'view_items'            => __( 'View Neighborhoods', 'oep' ),
			'search_items'          => __( 'Search Neighborhoods', 'oep' ),
			'not_found'             => __( 'Not found', 'oep' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'oep' ),
			'featured_image'        => __( 'Featured Image', 'oep' ),
			'set_featured_image'    => __( 'Set featured image', 'oep' ),
			'remove_featured_image' => __( 'Remove featured image', 'oep' ),
			'use_featured_image'    => __( 'Use as featured image', 'oep' ),
			'insert_into_item'      => __( 'Insert into neighborhood', 'oep' ),
			'uploaded_to_this_item' => __( 'Uploaded to this neighborhood', 'oep' ),
			'items_list'            => __( 'Neighborhoods list', 'oep' ),
			'items_list_navigation' => __( 'Neighborhoods list navigation', 'oep' ),
			'filter_items_list'     => __( 'Filter neighborhoods list', 'oep' ),
		];

		$rewrite = [
			'slug'                  => 'neighborhood',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		];

		$args = [
			'label'                 => __( 'Neighborhood', 'oep' ),
			'description'           => __( 'Neighborhood posts.', 'oep' ),
			'labels'                => $labels,
			'supports'              => [ 'title', 'editor', 'thumbnail', 'revisions' ],
			'taxonomies'            => [ 'post_tag' ],
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-admin-multisite',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'neighborhoods',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
		];

		return $args;
	}

	/**
	 * register custom events gutenberg block
	 */

	public function neighborhood_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-neighborhood',
				'title'				=> __('Neighborhood'),
				'description'		=> __('Embed a neighborhood'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/neighborhood.php',
				'category'			=> 'common', //'oep-neighborhood',
				'icon'				=> 'admin-multisite',
				'keywords'			=> array( 'neighborhood' ),
		));
		}
	}


	/**
	 * Displays default set of Gutenberg / post_meta block for posts.
	 * Will be reused for other CPTs
	 *
	 * @TODO  move placeholder / lorem generation somewhere else
	 *
	 * @since 0.2.0
	 */
	public function register_template() {
		$lorem                      = "Cras ac tristique purus, a ultrices turpis. Nam consequat convallis interdum. Ut nisl lacus, ornare nec nisi in, tristique porta purus. Fusce vel risus ante. Etiam imperdiet mauris libero, ac placerat odio placerat eget. Pellentesque sagittis tempus tortor et ornare. Pellentesque iaculis tincidunt lobortis.";
		$post_type_object           = get_post_type_object( OEP_NEIGHBORHOOD_KEY );
		$post_type_object->template = self::$template_args;
	}

	/**
	 * Run it
	 */
	public function run() {
		$this->loader->add_action( 'wp_loaded'	, $this, 'neighborhood_block'); //apparently acf/init is too early, go figure
		$this->loader->run();
	}
}
