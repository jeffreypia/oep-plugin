<?php
/**
 * Created by PhpStorm.
 * User: ryansmith
 * Date: 2019-03-21
 * Time: 12:07
 */

namespace OEP\CPT;


/**
 * Class Event
 *
 * @package OEP\CPT
 */
class Profile {

	/**
	 * Get args for CPT
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels = [
			'name'                  => _x( 'Profiles', 'Post Type General Name', 'oep' ),
			'singular_name'         => _x( 'Profile', 'Post Type Singular Name', 'oep' ),
			'menu_name'             => __( 'Profiles', 'oep' ),
			'name_admin_bar'        => __( 'Profile', 'oep' ),
			'archives'              => __( 'Profile Archives', 'oep' ),
			'attributes'            => __( 'Profile Attributes', 'oep' ),
			'parent_item_colon'     => __( 'Parent Profile:', 'oep' ),
			'all_items'             => __( 'All Profiles', 'oep' ),
			'add_new_item'          => __( 'Add New Profile', 'oep' ),
			'add_new'               => __( 'Add New', 'oep' ),
			'new_item'              => __( 'New Profile', 'oep' ),
			'edit_item'             => __( 'Edit Profile', 'oep' ),
			'update_item'           => __( 'Update Profile', 'oep' ),
			'view_item'             => __( 'View Profile', 'oep' ),
			'view_items'            => __( 'View Profiles', 'oep' ),
			'search_items'          => __( 'Search Profile', 'oep' ),
			'not_found'             => __( 'Not found', 'oep' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'oep' ),
			'featured_image'        => __( 'Featured Image', 'oep' ),
			'set_featured_image'    => __( 'Set featured image', 'oep' ),
			'remove_featured_image' => __( 'Remove featured image', 'oep' ),
			'use_featured_image'    => __( 'Use as featured image', 'oep' ),
			'insert_into_item'      => __( 'Insert into profile', 'oep' ),
			'uploaded_to_this_item' => __( 'Uploaded to this profile', 'oep' ),
			'items_list'            => __( 'Profiles list', 'oep' ),
			'items_list_navigation' => __( 'Profiles list navigation', 'oep' ),
			'filter_items_list'     => __( 'Filter profiles list', 'oep' ),
		];

		$rewrite = [
			'slug'                  => 'profile',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		];

		$args = [
			'label'                 => __( 'Profile', 'oep' ),
			'description'           => __( 'Augmentation for User profiles.', 'oep' ),
			'labels'                => $labels,
			'supports'              => [ 'title', 'editor', 'thumbnail', 'revisions' ],
			// 'taxonomies'            => [ 'category' ],
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-admin-users',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'profiles',
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
		];

		return $args;
	}
}