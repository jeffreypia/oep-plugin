<?php
/**
 * Created by PhpStorm.
 * User: ryansmith
 * Date: 2019-03-21
 * Time: 12:07
 */

namespace OEP\CPT;
use OEP\Common;

/**
 * Class Event
 *
 * @package OEP\CPT
 */
class Event {

	protected $loader;

	/**
	 * @var array
	 */
	protected static $template_args = array(
		array(
			'core/paragraph',
			array(
				'placeholder' => OEP_LOREM,
			),
		),
	);

	public function __construct() {
		$this->loader  = new Common\Loader();
		$this->run();
	}

	/**
	 * Get args for CPT
	 *
	 * @return array
	 */
	public static function get_args() {

		$labels = [
			'name'                  => _x( 'Events', 'Post Type General Name', 'oep' ),
			'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'oep' ),
			'menu_name'             => __( 'Events', 'oep' ),
			'name_admin_bar'        => __( 'Event', 'oep' ),
			'archives'              => __( 'Event Archives', 'oep' ),
			'attributes'            => __( 'Event Attributes', 'oep' ),
			'parent_item_colon'     => __( 'Parent Event:', 'oep' ),
			'all_items'             => __( 'All Events', 'oep' ),
			'add_new_item'          => __( 'Add New Event', 'oep' ),
			'add_new'               => __( 'Add New', 'oep' ),
			'new_item'              => __( 'New Event', 'oep' ),
			'edit_item'             => __( 'Edit Event', 'oep' ),
			'update_item'           => __( 'Update Event', 'oep' ),
			'view_item'             => __( 'View Event', 'oep' ),
			'view_items'            => __( 'View Events', 'oep' ),
			'search_items'          => __( 'Search Event', 'oep' ),
			'not_found'             => __( 'Not found', 'oep' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'oep' ),
			'featured_image'        => __( 'Featured Image', 'oep' ),
			'set_featured_image'    => __( 'Set featured image', 'oep' ),
			'remove_featured_image' => __( 'Remove featured image', 'oep' ),
			'use_featured_image'    => __( 'Use as featured image', 'oep' ),
			'insert_into_item'      => __( 'Insert into event', 'oep' ),
			'uploaded_to_this_item' => __( 'Uploaded to this event', 'oep' ),
			'items_list'            => __( 'Events list', 'oep' ),
			'items_list_navigation' => __( 'Events list navigation', 'oep' ),
			'filter_items_list'     => __( 'Filter events list', 'oep' ),
		];

		$rewrite = [
			'slug'                  => 'event',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		];

		$args = [
			'label'                 => __( 'Event', 'oep' ),
			'description'           => __( 'Event posts.', 'oep' ),
			'labels'                => $labels,
			'supports'              => [ 'title', 'editor', 'thumbnail', 'revisions' ],
			'taxonomies'            => [ 'post_tag' ],
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-calendar',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'events',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
		];

		return $args;
	}

	/**
	 * register custom events gutenberg block
	 */

	public function events_block() {
		if( function_exists('acf_register_block_type') ) {
			acf_register_block_type(array(
				'name'				=> 'oep-event',
				'title'				=> __('Event'),
				'description'		=> __('Embed an event'),
				'render_template'   => OEP_PLUGIN_DIR . 'public/partials/event.php',
				'category'			=> 'oep-landing-page',
				'icon'				=> 'calendar',
				'keywords'			=> array( 'event' ),
		));
		}
	}


	/**
	 * Displays default set of Gutenberg / post_meta block for posts.
	 * Will be reused for other CPTs
	 *
	 * @TODO  move placeholder / lorem generation somewhere else
	 *
	 * @since 0.2.0
	 */
	public function register_template() {
		$lorem                      = "Cras ac tristique purus, a ultrices turpis. Nam consequat convallis interdum. Ut nisl lacus, ornare nec nisi in, tristique porta purus. Fusce vel risus ante. Etiam imperdiet mauris libero, ac placerat odio placerat eget. Pellentesque sagittis tempus tortor et ornare. Pellentesque iaculis tincidunt lobortis.";
		$post_type_object           = get_post_type_object( OEP_EVENT_KEY );
		$post_type_object->template = self::$template_args;
	}

	/**
	 * Run it
	 */
	public function run() {
		$this->loader->add_action( 'wp_loaded'	, $this, 'events_block'); //apparently acf/init is too early, go figure
		$this->loader->run();
	}
}