<?php

/**
 * Orlando Economic Partnership (OEP) Plugin
 *
 * @link                    http://www.designzillas.com/
 * @since                   0.1.0
 * @package                 OEP
 *
 * @wordpress-plugin
 * Plugin Name:             Orlando Economic Partnership
 * Plugin URI:  Theme URI:  https://orlando.org/
 * Description:             CPTs, taxonomy definitions, third-party integrations and other non-theme functionality for OEP.
 * Version:                 0.1.0
 * Author:                  Designzillas
 * Author URI:              http://www.designzillas.com/
 * License:                 GPL-2.0+
 * License URI:             http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:             oep
 */

namespace OEP\Common;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Main class
 *
 * @since 0.1.0
 */
final class Plugin {

	/**
	 * POST/GET action key
	 *
	 * @since 0.1.0
	 */
	const ACTION_KEY = 'oep_action';
	/**
	 * The one and only instance of OEP
	 *
	 * @var   object
	 * @since 0.1.0
	 */
	protected static $instance = null;
	/**
	 * Plugin version for enqueueing, etc.
	 *
	 * @var   string
	 * @since 0.1.0
	 */
	public $version = '0.1.0';

	/**
	 * Main OEP Plugin instance
	 *
	 * @return \OEP\Common\Plugin
	 * @since  0.1.0
	 */
	public static function instance() {

		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Plugin ) ) {

			self::$instance = new Plugin;
			self::$instance->constants();

			add_action( 'init', [ self::$instance, 'do_post_actions' ] );

			add_action( 'acf/init', [ self::$instance, 'temp_add_success_options_page'] );

			register_activation_hook( OEP_PLUGIN_FILE, [ self::$instance, 'activate_oep' ] );
			register_deactivation_hook( OEP_PLUGIN_FILE, [ self::$instance, 'deactivate_oep' ] );

			do_action( 'oep/loaded' );

			/**
			 * The core plugin class that is used to define internationalization,
			 * admin-specific hooks, and public-facing site hooks.
			 */
			require OEP_PLUGIN_DIR . '/includes/class-oep.php';

			$plugin = new Core();
			$plugin->run();
		}

		return self::$instance;
	}

	/**
	 * Setup plugin constants
	 *
	 * @since 0.1.0
	 */
	protected function constants() {
		define( 'OEP_VERSION', $this->version );
		define( 'OEP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		define( 'OEP_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		define( 'OEP_PLUGIN_FILE', __FILE__ );
	}

	/**
	 * Hooks OEP actions, when present in the $_POST superglobal. Every
	 * oep/action present in $_POST is called using do_action.
	 *
	 * @since 0.1.0
	 */
	public function do_post_actions() {

		$key = ! empty( $_POST[ self::ACTION_KEY ] ) ? sanitize_key( $_POST[ self::ACTION_KEY ] ) : false;

		if ( ! empty( $key ) ) {
			do_action( "oep/{$key}", $_POST );
		}
	}

	/**
	 * Add options page
	 *
	 * @todo move to class-oep-success-cpt file
	 *
	 * @since 1.2.0
	 */
	public function temp_add_success_options_page() {

		$settings = __( 'Settings', 'oep' );

		acf_add_options_sub_page([
			'post_id'     => OEP_SUCCESS_KEY,
			'page_title'  => __('Success') . " $settings",
			'menu_title'  => $settings,
			'parent_slug' => add_query_arg( 'post_type', OEP_SUCCESS_KEY, 'edit.php' ),
			'menu_slug'   => 'success-story-settings',
		]);
	}

	/**
	 * The code that runs during plugin activation.
	 * This action is documented in includes/class-oep-activator.php
	 */
	function activate_oep() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-oep-activator.php';
		OEP_Activator::activate();
	}

	/**
	 * The code that runs during plugin deactivation.
	 * This action is documented in includes/class-oep-deactivator.php
	 */
	function deactivate_oep() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-oep-deactivator.php';
		OEP_Deactivator::deactivate();
	}

}

/**
 * The main function that instantiates the OEP plugin
 *
 * @return object
 * @since  0.1.0
 */
function OEP() {
	return Plugin::instance();
}

// slaughter
OEP();
